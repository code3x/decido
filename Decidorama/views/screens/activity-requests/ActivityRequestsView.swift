// 
import SwiftUI
import Foundation

struct ActivityRequestsView: View {
    @StateObject var viewModel = ActivityRequestsViewModel()
    @EnvironmentObject var profile: ProfileData
    let userFunctions = UserFunctions()
    let activityFunctions = ActivityFunctions()
    
    var body: some View{
        VStack{
            Typography(text: "My Requests",size:17, weight: .semibold).padding(.trailing, 300)
            
            Rectangle().frame(height: 5)
                .padding(.horizontal, 0).foregroundColor(Color.purple)
                .padding(.bottom, 0)
            if(viewModel.loading){
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: .purple)).scaleEffect(x: 2.0, y: 2.0, anchor: .center).padding(20)
                
            }
            else if(viewModel.noRequests){
                Image("no-user-requests.png").resizable()
                    .frame(width: 350, height: 350, alignment: .center)
                    .scaledToFill()
                    .shadow(radius: 2)
                    .padding(.top, 100)
                    
                Typography(text: "You don't have any requests", size: 20).padding(15).padding(.top, 30)
            }
            ScrollView {
                ForEach(viewModel.requests, id: (\.id)) { request in
                    
                    
                    
                    NavigationLink(
                        destination: ActivityDetailView(activity: request, isRequestPage: true)
                    ) {
                        HStack{
                            ApplicationImageStatic(urlString: request.image)
                                .aspectRatio(contentMode: .fill)
                                .clipped()
                                .clipShape(Circle())
                                .frame(width: 75, height: 75)
                                .padding(.leading, 5)
                                .padding(.trailing, 5)
                                .padding(.top, 20)
                            VStack (alignment: .leading, spacing: 10){
                                VStack (alignment: .leading, spacing: 2){
                                    Text(request.name)
                                        .frame(width: 300, alignment: .leading)
                                        .lineLimit(1)
                                        .foregroundColor(.primary)
                                    Text("\(request.description)")
                                        .frame(width: 300, alignment: .leading)
                                        .lineLimit(1)
                                        .foregroundColor(.secondary)
                                }
                            }
                        }
                    }
                    HStack{
                        Button(action: {
                            userFunctions.userAcceptRequest(userPhoneNumber: profile.user.phoneNumber, activityId: request.activityId ?? "")
                            self.delete(at:self.viewModel.requests.firstIndex(where: { $0.id == request.id })!)
                        }) {
                            ZStack {
                                RoundedRectangle(cornerRadius: 5)
                                    .frame(height: 35)
                                    .foregroundColor(.blue)
                                Text("Accept")
                                    .font(.system(size: 13))
                                    .foregroundColor(.white)
                            }
                        }
                        
                        Button(action: {
                            userFunctions.userDeclineRequest(userPhoneNumber: profile.user.phoneNumber, activityId: request.activityId ?? "")
                            self.delete(at:self.viewModel.requests.firstIndex(where: { $0.id == request.id })!)
                        }) {
                            ZStack {
                                RoundedRectangle(cornerRadius: 5)
                                    .frame(height: 35)
                                    .foregroundColor(.gray)
                                Text("Decline")
                                    .font(.system(size: 13))
                                    .foregroundColor(.white)
                            }
                        }
                    }
                    .frame(width: 300, alignment: .leading)
                    .padding(.leading, 110)
                    .padding(.trailing, 10)
                    .padding(.top, -30)
                }
                
                if(viewModel.requests.count == 0){
                    ProgressView()
                        .progressViewStyle(CircularProgressViewStyle(tint: .purple)).padding(.bottom, 250).scaleEffect(x: 2.0, y: 2.0, anchor: .center)
                }
            }
        }
        .onAppear{
            viewModel.requests = []
            viewModel.loading = true
            
            userFunctions.getUserRequests(userPhoneNumber: profile.user.phoneNumber){ result in
                switch result {
                case .success(let requests):
                    viewModel.noRequests = false
                    for request in requests{
                        
                        activityFunctions.getActivityById(activityId: request){ result in
                            switch result{
                            case .success(let activity):
                                viewModel.requests.append(activity)
                            case .failure(_):
                                print("something went wrong retrieving the activity")
                            }
                        }
                    }
                    viewModel.loading = false
                    
                case .failure(_):
                    print("something went wrong or there are no requests")
                    viewModel.noRequests = true
                    viewModel.loading = false
                }
            }
        }
    }
    
    func delete(at index: Int) {
        viewModel.requests.remove(at: index)
    }
}

struct ActivityRequestsView_Previews: PreviewProvider {
    static var previews: some View {
        ActivityRequestsView().environmentObject(ProfileData())
    }
}
