import Foundation

final class ActivityRequestsViewModel: ObservableObject{
    @Published var requests: [Activity] = []
    
    @Published var noRequests = false
    @Published var loading = false
}


