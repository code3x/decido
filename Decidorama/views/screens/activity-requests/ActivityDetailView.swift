import SwiftUI

struct ActivityDetailView: View {
    var activity: Activity
    
    @State var showShareActivity = false
    @State var sharedPhoneNumber = ""
    @State var showingAlert = false
    let isRequestPage: Bool
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack(alignment: .center, spacing: 20) {
                
                ZStack(alignment: .bottomTrailing){
                    ApplicationImageStatic(urlString: activity.image)
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 450, height: 450)
                        .clipped()
                }
                
                VStack(alignment: .leading, spacing: 20) {
                    // Title
                    Text(activity.name)
                        .font(.largeTitle)
                        .fontWeight(.heavy)
                        .foregroundColor(.black)
                    
                    // Headline
                    Text("Location: \(activity.location)")
                        .font(.headline)
                        .multilineTextAlignment(.leading)
                    
                    // Subheadline
                    Text("To know more about \(activity.name)".uppercased())
                        .fontWeight(.bold)
                    
                    // Description
                    Text(activity.description)
                        .multilineTextAlignment(.leading)
                    
                }
                .padding(.horizontal, 20)
                .frame(maxWidth: 440, alignment: .leading)
            }
            .navigationBarTitle( activity.deleted ? "Activity Deleted" : activity.name, displayMode: .inline)
        }
        .edgesIgnoringSafeArea(.top)
        .customBottomSheet(isPresented: $showShareActivity){
            withAnimation(.easeInOut) {
                shareActivityView(phoneNumber: $sharedPhoneNumber, activityID: activity.activityId ?? "Error", showShareActivity: $showShareActivity, showAlert: $showingAlert)
            }
        }
        .alert(isPresented: $showingAlert) {
            Alert(
                title: Text("Request sent"),
                message: Text("You have successfully sent a request to \(sharedPhoneNumber).")
            )
        }
        .toolbar {
            ToolbarItem(placement: .primaryAction) {
                if(!isRequestPage && !activity.deleted){
                    Button("Share") {
                        showShareActivity.toggle()
                    }
                }
            }
        }
    }
}

struct ActivityDetailView_Previews: PreviewProvider {
    static var previews: some View {
        ActivityDetailView(activity: ActivityMockData.sampleActivity, isRequestPage: false)
    }
}

struct shareActivityView: View {
    
    @Binding var phoneNumber: String
    var activityID: String
    @Binding var showShareActivity: Bool
    @Binding var showAlert: Bool
    
    var body: some View{
        
        ZStack{
            BackgroundColor(topColor: Color("Purple_1"), bottomColor: Color("Purple_2"))
                .frame(width: UIScreen.main.bounds.size.width*0.95, height: 250)
                .cornerRadius(10.0)
                .opacity(0.95)
            
            VStack{
                Typography(text: "Students Phone Number", color: .white)
                
                HStack {
                    Image("canada_flag").resizable().frame(width: 40, height: 20).padding(.leading, 5)
                    Text("+1 | ")
                    TextField("999-999-9999", text: $phoneNumber).foregroundColor(.white)
                }.customTextField().padding()
                
                Button("Send Request") {
                    ActivityFunctions().sendGroupInvite(userPhoneNumber: phoneNumber, activityId: activityID)
                    showShareActivity = false
                    showAlert = true
                }
                .foregroundColor(.white)
                .font(.system(size: 22))
                .padding()
                .border(Color.white, width: 2)
                .cornerRadius(3.0)
            }
        }
        
        Text("")
            .frame(height: UIScreen.main.bounds.size.height*0.5 - 125)
    }
}
