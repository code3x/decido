import SwiftUI

struct ActivityRequestsRow: View {
    
    var request: Activity
    var userPhoneNumber: String
    
    let userFunctions = UserFunctions()
    
    var body: some View {
        NavigationLink(
            destination: ActivityDetailView(activity: request)
        ) {
            HStack{
                ApplicationImageStatic(urlString: request.image)
                    .aspectRatio(contentMode: .fill)
                    .clipped()
                    .clipShape(Circle())
                    .frame(width: 75, height: 75)
                    .padding(.leading, 20)
                    .padding(.trailing, 5)
                VStack (alignment: .leading, spacing: 10){
                    VStack (alignment: .leading, spacing: 2){
                        Text(request.name)
                            .frame(width: 300, alignment: .leading)
                            .lineLimit(1)
                            .foregroundColor(.primary)
                        Text("\(request.description)")
                            .frame(width: 300, alignment: .leading)
                            .lineLimit(1)
                            .foregroundColor(.secondary)
                    }
                    HStack{
                        Button(action: {
                            userFunctions.userAcceptRequest(userPhoneNumber: userPhoneNumber, activityId: request.activityId ?? "")
                        }) {
                            ZStack {
                                RoundedRectangle(cornerRadius: 5)
                                    .frame(height: 35)
                                    .foregroundColor(.blue)
                                Text("Accept")
                                    .font(.system(size: 13))
                                    .foregroundColor(.white)
                            }
                        }
                        Button(action: {
                            userFunctions.userDeclineRequest(userPhoneNumber: userPhoneNumber, activityId: request.activityId ?? "")
                        }) {
                            ZStack {
                                RoundedRectangle(cornerRadius: 5)
                                    .frame(height: 35)
                                    .foregroundColor(.gray)
                                Text("Decline")
                                    .font(.system(size: 13))
                                    .foregroundColor(.white)
                            }
                        }
                        
                    }
                }
            }
        }
    }
}

struct ActivityRequestsRow_Previews: PreviewProvider {
    static var previews: some View {
        ActivityRequestsRow(request: ActivityMockData.sampleActivity, userPhoneNumber: "")
            .previewLayout(.sizeThatFits)
    }
}
