import SwiftUI

struct GroupPageWrapper: View {
    var activity: Activity
    @EnvironmentObject var profile: ProfileData
    @StateObject var viewModel = ActivityTabViewModel()
    @State private var selectedTab = 0
    
    var body: some View {
        
        ZStack{
            
            VStack{
                if(!activity.deleted){
                    Picker(selection: $selectedTab, label: Text("Group Page")) {
                        Text("Activities").tag(0)
                        Text("Chat").tag(1)
                        Text("Settings").tag(2)
                        Text("Voting").tag(3)
                    }.pickerStyle(SegmentedPickerStyle())
                }
                
                if (selectedTab == 0) {
                    ActivityDetailView(activity: activity, isRequestPage: false)
                }
                
                if(!activity.deleted){
                    if (selectedTab == 1) {
                        MessagingView(activity: activity)
                    }
                    
                    if (selectedTab == 2) {
                        ActivitySettings(activity: activity, members: $viewModel.members)
                    }
                    
                    if (selectedTab == 3) {
                        VotingView(activity: activity)
                            .navigationBarTitle(activity.name, displayMode: .inline)
                    }
                }
            }
            
            if(activity.deleted){
                Typography(text: "Deleted", size: 120, weight: .bold, color: .red)
                    .rotationEffect(Angle(degrees: -60))
            }
            
            
        }
        
        .onAppear(){
            viewModel.onLoad(activityId: activity.activityId ?? "")
        }
    }
}


struct NavigationBar_Previews: PreviewProvider {
    static var previews: some View {
        GroupPageWrapper(activity: ActivityMockData.sampleActivity)
    }
}
