//
//  ActivityTabViewModel.swift
//  Decidorama
//
//  Created by Zakim Javer on 2021-11-10.
//

import Foundation
import Firebase

final class ActivityTabViewModel: ObservableObject{
    @Published var members = [User]()
    var ref = DatabaseReference()
    
    func onLoad(activityId: String){
        print("XX")
        loadActivityMembers(activityId: activityId)
    }
    
    func loadActivityMembers(activityId: String){
        ActivityFunctions().getActivityMembers(activityId: activityId) { result in
            switch result{
            case .success(let members):
                print("Total members received", members.count)
                self.members = members
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}



