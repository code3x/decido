import SwiftUI

struct ActivitySettings: View {
    @State var activity: Activity
    @Binding var members: [User]
    @EnvironmentObject var profile: ProfileData
    @StateObject var viewModel = ActivitySettingViewModel()
    @Environment(\.presentationMode) var presentationMode
    @State var showShareActivity = false
    @State var sharedPhoneNumber = ""
    @State var showingAlert = false
    
    var body: some View {
        VStack{
            NavigationLink(destination: GroupRequestsView(activity: activity), isActive: $viewModel.showRequestPage){
                EmptyView()
            }
            List{
                Section {
                    ZStack {
                        HStack {
                            
                            ZStack(alignment: .bottomTrailing) {
                                
                                ApplicationImageStatic(urlString: activity.image)
                                    .aspectRatio(contentMode: .fill)
                                    .clipped()
                                    .clipShape(Circle())
                                    .frame(width: 75, height: 75)
                                
                            }
                            
                            VStack(alignment: .leading) {
                                Text(activity.name)
                                    .foregroundColor(.primary)
                                    .font(.system(size: 16, weight: .semibold)).padding(.bottom, 4)
                                Text("Edit")
                                    .foregroundColor(.secondary)
                                    .font(.system(size: 14, weight: .regular))
                            }.padding(.leading, 9)
                            
                            Spacer()
                            
                            HStack {
                                
                                Button(action: {
                                    viewModel.goToActivityEdit = true
                                }, label: {
                                    Image(systemName: "pencil.circle")
                                        .resizable()
                                        .frame(width: 25, height: 25)
                                        .foregroundColor(.blue)
                                        .padding()
                                    
                                }).buttonStyle(PlainButtonStyle()).padding(.trailing, 5)
                                
                            }
                            
                        }
                        
                        NavigationLink(destination: ActivityEdit(activity: activity)
                                       , isActive: $viewModel.goToActivityEdit) { EmptyView()}
                        
                    }.padding(.vertical, 9)
                }
                
                Section{
                    Button(action: {
                        viewModel.showAnalytics.toggle()
                    }, label: {
                        HStack{
                            Text("📈📉 Activity Analytics")
                                .foregroundColor(.black)
                        }
                        
                    })
                    
                    if(activity.owner == profile.user.phoneNumber){
                        Button(action: {
                            viewModel.showRequestPage.toggle()
                        }, label: {
                            HStack{
                                Text("🔔 Requests")
                                    .foregroundColor(.black)
                            }
                            
                        })
                    }
                }
                
                HStack{
                    
                    Text("Members")
                    Spacer()
                    Button("Add Member") {
                        showShareActivity.toggle()
                    }
                }
                
                if members.count > 0 {
                    ForEach(members, id: \.phoneNumber) { member in
                        memberInfo(activity: activity, user: profile.user, member: member)
                    }
                }else{
                    Text("Loading Members...")
                }
                
                Section{
                    HStack{
                        Spacer()
                        Button(action: {
                            viewModel.showLeaveGroupAlert.toggle()
                        }, label: {
                            Text("Leave Group")
                        }).foregroundColor(.red)
                        Spacer()
                    }
                    .actionSheet(isPresented: $viewModel.showLeaveGroupAlert) {
                        ActionSheet(
                            title: Text("Leave Activity"),
                            message: Text("Are you sure you want to leave \(activity.name)"),
                            buttons:[
                                .destructive(Text("Leave"),
                                             action: {
                                                ActivityFunctions().adminRemovesMember(userPhoneNumber: profile.user.phoneNumber, activityId: activity.activityId ?? "")
                                                
                                                self.presentationMode.wrappedValue.dismiss()
                                             }),
                                .cancel()
                            ]
                        )
                    }
                    
                    if(activity.owner == profile.user.phoneNumber){
                        HStack{
                            Spacer()
                            Button(action: {
                                viewModel.deleteGroupModalOpen.toggle()
                            }, label: {
                                Text("Delete / Complete Activity")
                            }).foregroundColor(.red)
                            Spacer()
                        }.actionSheet(isPresented: $viewModel.deleteGroupModalOpen) {
                            ActionSheet(
                                title: Text("Delete Activity"),
                                message: Text("Are you sure you want to delete \(activity.name)"),
                                buttons:[
                                    .destructive(Text("Delete"),
                                                 action: {
                                                    if ((activity.activityId) != nil){
                                                        ActivityFunctions().deleteActivity(activityId: activity.activityId ?? "")
                                                    }
                                                    activity.deleted = true
                                                    self.presentationMode.wrappedValue.dismiss()
                                                    
                                                 }),
                                    .cancel()
                                ]
                            )
                        }
                    }
                    
                    
                }
            }.listStyle(GroupedListStyle())
            .customBottomSheet(isPresented: $showShareActivity){
                withAnimation(.easeInOut) {
                    shareActivityView(phoneNumber: $sharedPhoneNumber, activityID: activity.activityId ?? "Error", showShareActivity: $showShareActivity, showAlert: $showingAlert)
                }
            }
            .alert(isPresented: $showingAlert) {
                Alert(
                    title: Text("Request sent"),
                    message: Text("You have successfully sent a request to \(sharedPhoneNumber).")
                )
            }
        }
        .sheet(isPresented: $viewModel.showAnalytics, content: {
            AnalyticsView(analyticsOpen: $viewModel.showAnalytics, activity: $activity)
        })
        .navigationBarTitle(activity.name, displayMode: .inline)
    }
}

struct GroupSettings_Previews: PreviewProvider {
    static var previews: some View {
        ActivitySettings(activity: ActivityMockData.sampleActivity, members: .constant([UserMockData.sampleUser]))
    }
}

struct memberInfo: View {
    @State private var showBlockOptions = false
    @State private var showProfile = false
    var activity: Activity
    var user: User
    var member: User
    
    var body: some View{
        VStack(){
            HStack(){
                ApplicationImageStatic(urlString: member.avatar)
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 40, height: 40, alignment: .center)
                    .clipped()
                    .cornerRadius(150)
                
                Text(member.name)
                    .fontWeight(.semibold)
                    .font(.system(size: 13))
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.leading, -2)
                
                if activity.owner == member.phoneNumber{
                    Text("Admin")
                        .fontWeight(.semibold)
                        .font(.system(size: 12))
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.leading, 80)
                }else{
                    Text("Member")
                        .fontWeight(.semibold)
                        .font(.system(size: 12))
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.leading, 80)
                }
                
                Image(systemName: "ellipsis").rotationEffect(.init(degrees: 90))
                    .foregroundColor(.red)
                    .frame(width: 20, height: 20)
                    .onTapGesture {
                        showBlockOptions.toggle()
                    }
                Button {
                    showProfile.toggle()
                } label: {
                    Text("")
                }
                
            }
            .actionSheet(isPresented: $showBlockOptions) {
                
                if activity.owner == user.phoneNumber{
                    return ActionSheet(
                        title: Text("Remove Member"),
                        message: Text("You can't undo this action. \(member.phoneNumber)"),
                        buttons:[
                            .destructive(Text("Remove"),
                                         action: {
                                            ActivityFunctions().adminRemovesMember(userPhoneNumber: member.phoneNumber, activityId: activity.activityId ?? "")
                                         }),
                            .cancel()
                        ]
                    )
                }else{
                    return ActionSheet(
                        title: Text("Admin"),
                        message: Text("Only admins can remove members"),
                        buttons:[
                            .cancel()
                        ]
                    )
                }
            }
            
            .sheet(isPresented: $showProfile) {
                membersProfile(membersProfile: member, showProfile: $showProfile)
            }
        }
    }
    
}

