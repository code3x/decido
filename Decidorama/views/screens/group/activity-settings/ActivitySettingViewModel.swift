//
//  ActivitySettingViewModel.swift
//  Decidorama
//
//  Created by Ali Muhammad on 2021-11-04.
//

import Foundation

final class ActivitySettingViewModel: ObservableObject{
    @Published var activityData = ActivityData()
    @Published var goToActivityEdit = false
    @Published var showLeaveGroupAlert = false
    @Published var showRequestPage = false
    @Published var showAnalytics = false
    @Published var deleteGroupModalOpen = false
    
}
