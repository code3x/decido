import SwiftUI

struct AddGroup: View {
    
    @State private var groupName: String = ""
    @State private var groupDescription: String = ""
    @State private var groupType: String = "Public"
    
    @Binding var isCreateGroupOpen: Bool
    
    var body: some View {
        
        VStack() {
            
            List{
                LabelTextField(label: "GROUP NAME", mainText: $groupName, placeHolder: "Enter group name")
                LabelTextField(label: "DESCRIPTION", mainText: $groupDescription, placeHolder: "Enter a short description")
            }.padding(.top, 20)
            
            RoundedButton {
                
                let s = serverRequest()
                let today = Date()
                let formatter3 = DateFormatter()
                
                formatter3.dateFormat = "HH:mm E, d MMM y"
                
                s.saveGroup(group: Group(groupID: UUID().uuidString, groupName: groupName, groupCreateDate: formatter3.string(from: today), groupDescription: groupDescription, groupPictureURL: "p0", groupType: "Public", activities: [""], preferences: [""]))
                
                isCreateGroupOpen = false
                
            }
        }
        .navigationBarTitle(Text("Creating Group"))
    }
}

struct AddGroup_Previews: PreviewProvider {
    static var previews: some View {
        AddGroup(isCreateGroupOpen: .constant(true))
    }
}

struct RoundedButton : View {
    var btnTitle: String = "Create group"
    var btnColor: Color = Color.red
    let action: () -> Void
    
    var body: some View {
        Button(action: action) {
            HStack {
                Spacer()
                Text(btnTitle)
                    .font(.headline)
                    .foregroundColor(.white)
                
                Spacer()
            }
        }
        .padding(.vertical, 10.0)
        .background(btnColor)
        .cornerRadius(8.0)
        .padding(.horizontal, 50)
    }
}

struct LabelTextField : View {
    var label: String
    @Binding var mainText: String
    var placeHolder: String
    
    var body: some View {
        
        VStack(alignment: .leading) {
            Text(label)
                .font(.headline)
            TextField(placeHolder, text: $mainText)//(.constant(""), text: placeHolder, placeholder: Text(placeHolder))
                .padding(.all)
                .background(Color(red: 239.0/255.0, green: 243.0/255.0, blue: 244.0/255.0, opacity: 1.0))
                .cornerRadius(5.0)
        }
        .padding(.horizontal, 15)
    }
}
