import SwiftUI

struct ActivityRow: View {
    
    var activity: Activity
    
    var body: some View {
        NavigationLink(
            destination: GroupPageWrapper(activity: activity).navigationBarTitle("", displayMode: .inline)
        ) {
            HStack{
                ApplicationImageStatic(urlString: activity.image)
                    .aspectRatio(contentMode: .fill)
                    .clipped()
                    .clipShape(Circle())
                    .frame(width: 75, height: 75)
                    .padding(.leading, 20)
                    .padding(.trailing, 5)
                VStack (alignment: .leading, spacing: 10){
                    VStack (alignment: .leading, spacing: 2){
                        Text(activity.name)
                            .frame(width: 300, alignment: .leading)
                            .lineLimit(1)
                            .foregroundColor(.primary)
                        Text("\(activity.description)")
                            .frame(width: 300, alignment: .leading)
                            .lineLimit(1)
                            .foregroundColor(.secondary)
                        Text("📍 \(activity.location)")
                            .frame(alignment: .leading)
                            .lineLimit(1)
                            .foregroundColor(.secondary)
                    }
                    
                }
            }.opacity(activity.deleted ? 0.6 : 1)
        }
    }
}

struct ActivityRow_Previews: PreviewProvider {
    static var previews: some View {
        ActivityRow(activity: ActivityMockData.sampleActivity)
            .previewLayout(.sizeThatFits)
    }
}
