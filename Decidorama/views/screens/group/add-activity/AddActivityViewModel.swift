import SwiftUI
import Foundation
import SwiftyJSON

//if didLoadEvents == false {
//    Api().loadData() {(events) in self.events = events}
//    didLoadEvents = true
//}

final class AddActivityViewModel: ObservableObject{
    @Published var activity: Activity = ActivityMockData.emptyActivity
    @Published var showLoadingScreen: Bool = false
    @Published var limitStringValue: String = ""
    @Published var showImagePicker = false
    @Published var didLoadEvents : Bool = false
    @Published var preference: UserPreference = UserPreference(preferenceId: "", preferenceName: "Select a preference", preferenceColor: .blue)
    
    @Published var image: Bool = false
    
    func updateMemberLimit() {
        activity.memberLimit = Int(limitStringValue) ?? 0
    }
    
    func onSubmitCreateActivity(userPhoneNumber: String, image: UIImage, complete: @escaping () -> Void) {
        showLoadingScreen = true
        updateMemberLimit()
        
        //saving image to server and getting url
        NetworkManager.shared.uploadImg(imag: image, activityID: "\(activity.id)") {
                    result in
                    
                    switch result {
                        case .success(let url):
                            //images saved successfully, url retrieved
                            var newActivity = self.activity
                            newActivity.image = url
                            newActivity.preference = self.preference.preferenceName
                            ActivityFunctions().createActivity(activity: newActivity, userPhoneNumber: userPhoneNumber)
                            self.showLoadingScreen = false
                            print("Saved activity")
                            complete()
                            
                        case .failure(let error):
                            print("Unable to upload image when updating: ", error)
                            self.showLoadingScreen = false
                            complete()
                        }
                }
    }
}


class Api : ObservableObject{
    
    @Published var events = [Activity]()
    @Published var activity: Activity = ActivityMockData.emptyActivity
    @Published var searchCity : String = ""
    
    
    
    func loadData(completion:@escaping ([Activity]) -> ()) {

        let dispatchGroup = DispatchGroup()
        
        let queryString = "https://api.seatgeek.com/2/events?client_secret=e43c2d81465188ca2e26eb29931f25491aaa4545dfa73408011c72faf7440981&client_id=MjQ1MzI5MzV8MTYzNzM0Njc2OS43NDMwNTky"
            
//            "https://app.ticketmaster.com/discovery/v2/events.json?apikey=L8A5GxHUO2bbsAAJWawelhbsFEb1MtEA&city=" + "Burlington" // +  searchCity
        
        let urlString = URL(string: queryString)
        
        
        
        if let url = urlString {
            
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                
                guard let data = data, error == nil else {
                    return
                }
                do {
                    
                    dispatchGroup.enter()
                    let jsonData = try  JSON(data: data)
                    
                    print(jsonData["events"][0])
                    
                    for i in 0..<jsonData["events"].count{
                        
                        
                        var apiActivity : Activity = Activity(owner: "", image: "", campus: "", name: "", description: "", rating: "0", location: "", memberLimit: 5, date: "", price : "", preference: "External", deleted: false)
                        
                        apiActivity.name = "\(jsonData["events"][i]["title"])"

                        apiActivity.image = "\(jsonData["events"][i]["performers"][0]["images"]["huge"])"

                        apiActivity.date = "\(jsonData["events"][i]["datetime_utc"])"
                        
                        apiActivity.location = "\(jsonData["events"][i]["venue"]["display_location"])"
                        
                        apiActivity.description = "\(jsonData["events"][i]["description"])"
                        
                        ExternalActivityFunctions().createExternalActivity(activity: apiActivity)
                        
                        self.events.append(apiActivity)
                    }
                    
                    dispatchGroup.leave()
                    dispatchGroup.notify(queue: DispatchQueue.main) {
                
                        completion(self.events)
                    }
            
                } catch _ {
                    
                    //completionHandler(nil)
                    
                }
                
            }
            
            task.resume()
        }
    }
}
