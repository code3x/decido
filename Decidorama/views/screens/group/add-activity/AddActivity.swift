import SwiftUI
import PopupView

struct AddActivity: View {
    
    @EnvironmentObject var profile: ProfileData
    @StateObject var viewModel = AddActivityViewModel()
    @State var imageViolationSnackbar = false
    //used to pop navigation controller
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    
    @State private var tempImage: UIImage?
    @State private var image: UIImage = UIImage(named: "ImagePlaceholder")!//
    @ObservedObject var classifier: ImageClassifier = ImageClassifier()
    var disableForm: Bool {
        viewModel.activity.name == "" || viewModel.activity.location == "" || viewModel.activity.description == "" 
    }
    
    var body: some View {
        VStack{
            ZStack{
                
                VStack{
                    roundImageAction(image: $image){
                        viewModel.showImagePicker = true
                    }
                    
                    Form {
                        Section(header: Text("Name of activity")) {
                            TextField("e.g. Study in learning commons", text: $viewModel.activity.name)
                        }
                        Section(header: Text("Location")) {
                            TextField("e.g. Oakville", text: $viewModel.activity.location)
                        }
                        Section(header: Text("Member Limit")) {
                            TextField("e.g. 10", text: $viewModel.limitStringValue).keyboardType(.numberPad)
                        }
                        Section(header: Text("Description")) {
                            TextField("e.g. study with a group of people", text: $viewModel.activity.description)
                        }
                        
                        Section(header: Text("Preferences")) {
                            preferenceDropDown(buttonText: $viewModel.preference.preferenceName, allPreferences: $profile.allPreferences)
                        }
                    }
                    
                    Button {
                        
                        //save activity click
                        viewModel.onSubmitCreateActivity(userPhoneNumber: profile.user.phoneNumber, image: image){
                            self.mode.wrappedValue.dismiss()
                        }
                        
                    } label: {
                        ApplicationButton(title: "Add Activity", backgroundColor: .purple, textColor: .white, disabled: disableForm)
                    }
                    .padding()
                    .disabled(disableForm)
                }
                .navigationTitle(" 🧊 Add Activity")
                .sheet(isPresented: $viewModel.showImagePicker, onDismiss: setImage) {
                    ImagePicker(image: self.$tempImage)
                }
            }
        }
        .popup(isPresented: $imageViolationSnackbar, type: .toast, autohideIn: 4.0, view: {
            ApplicationSnackbar(text: "Our system has detectcect violence in your image.", color: .red)
        })
    }
    
    
    
    
    func setImage(){
        guard let img = tempImage else {
            print("Error: invalid image selected")
            return
        }
        if tempImage != nil {
            classifier.detect(uiImage: img)
        }
        if doesImageHaveViolence(description: classifier.imageClass ?? ""){
            imageViolationSnackbar = true
        }else{
            image = img
        }
        
    }
    
    func doesImageHaveViolence(description: String) -> Bool{
        var imageHasViolence = false
        let criterias = ["gun", "revolver", "guillotine", "rifle", "six-gun", "six-shooter", "missile", "scabbard", "meat cleaver"]
        for criteria in criterias {
            if(description.contains(criteria)){
                imageHasViolence = true
            }
        }
        return imageHasViolence
    }
}

struct AddActivity_Previews: PreviewProvider {
    static var previews: some View {
        AddActivity().environmentObject(ProfileData(user: UserMockData.sampleUser))
    }
}

struct roundImageAction: View {
    @Binding var image : UIImage
    
    var bottomColor: Color = .white
    let action: () -> Void
    
    var body: some View {
        
        ZStack(alignment: .center){
            
            Image(uiImage: image)
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: UIScreen.main.bounds.size.width*0.9, height: UIScreen.main.bounds.size.width*0.6, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                .clipped()
                .cornerRadius(10)
                .padding()
                .onTapGesture {
                    self.action()
                }
            
            //show + icon only if image is not yet added
            if image == UIImage(named: "ImagePlaceholder")!{
                Button {
                    print("Add Button Clicked")
                } label: {
                    Typography(text: "+", size: 35, weight: .bold, color: .purple)
                        .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        .overlay(
                            RoundedRectangle(cornerRadius: 80)
                                .stroke(Color.purple, lineWidth: 4)
                        )
                        .onTapGesture {
                            self.action()
                        }
                    
                }
            }
        }
        
        
        
    }
}



struct preferenceDropDown: View {
    @Binding var buttonText: String
    @Binding var allPreferences: [UserPreference]
    
    
    var body: some View{
        HStack{
            Label("", systemImage: "newspaper")
                .frame(maxWidth: 45, alignment: .trailing)
                .font(.system(size: 30))
                .foregroundColor(Color.purple)
            
            Menu(buttonText) {
                ForEach(allPreferences, id: \.id){ preference in
                    Button(preference.preferenceName, action: {
                        buttonText = "\(preference.preferenceName)"
                    })
                }
            }
        }.padding(.vertical)
    }
}
