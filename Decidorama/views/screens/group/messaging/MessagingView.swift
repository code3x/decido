//
//  MessagingHome.swift
//  Decidorama
//
//  Created by Zakim Javer on 2021-10-30.
//

import SwiftUI

struct MessagingView: View {
    
    @EnvironmentObject var profile: ProfileData
    @StateObject var viewModel = MessagingViewModel()
    var activity: Activity
    
    var body: some View {
        VStack(spacing: 0){
            
            ScrollViewReader{ reader in
                ScrollView{
                    VStack(spacing: 15){
                        ForEach(viewModel.messages){msg in
                            ChatRow(chat: msg)
                                .onAppear(){
                                    reader.scrollTo(viewModel.messages.last?.id, anchor: .bottom)
                                }
                        }
                        .onChange(of: viewModel.messages) { value in
                            reader.scrollTo(viewModel.messages.last?.id, anchor: .bottom)
                        }
                    }
                    .padding(.vertical)
                }
            }
            
            HStack(spacing: 15){
                TextField("Enter Message", text: $viewModel.text)
                    .padding(.horizontal)
                    .frame(height: 45)
                    .background(Color.primary.opacity(0.06))
                    .clipShape(Capsule())
                
                if viewModel.text != ""{
                    Button(action: {viewModel.sendMessage(sendersPhoneNumber: profile.user.phoneNumber, activityId: activity.activityId ?? "")}
                           , label: {
                        Image(systemName: "paperplane.fill")
                            .font(.system(size: 22))
                            .foregroundColor(.white)
                            .frame(width: 45, height: 45, alignment: .center)
                            .background(Color.blue)
                            .clipShape(Circle())
                    })
                }
            }
            .animation(.default)
            .padding()
        }
        .navigationBarTitle(activity.name, displayMode: .inline)
        .onAppear(){
            
            viewModel.onLoad(activityId: activity.activityId!)
            
            
        }
        .onDisappear(){
            viewModel.ref.removeAllObservers()
        }
    }
}

struct MessagingHome_Previews: PreviewProvider {
    static var previews: some View {
        MessagingView(activity: ActivityMockData.sampleActivityZJ)
    }
}
