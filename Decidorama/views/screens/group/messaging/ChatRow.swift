//
//  ChatRow.swift
//  Decidorama
//
//  Created by Zakim Javer on 2021-10-30.
//

import SwiftUI

struct ChatRow: View {
    
    @EnvironmentObject var profile: ProfileData
    @StateObject var viewModel = MessagingViewModel()
    var chat: Message
    
    var body: some View {
        HStack(spacing: 15){
            //Nickname view
            if chat.sendersPhoneNumber != profile.user.phoneNumber{
                ProfileBubble(message: chat, currentUser: profile.user)
            }
            
            if chat.sendersPhoneNumber == profile.user.phoneNumber{
                Spacer(minLength: 0)
            }
            
            VStack(alignment: chat.sendersPhoneNumber == profile.user.phoneNumber ? .trailing : .leading, spacing: 5){
                
                Text(chat.messageText)
                    .fontWeight(.semibold)
                    .foregroundColor(.white)
                    .padding()
                    .background(Color.blue)
                    //.padding(15)
                //Custom chat bubble
                    .clipShape(ChatBubble(myMessage: chat.sendersPhoneNumber == profile.user.phoneNumber))
                
                HStack{
                Text(chat.timeStamp, style: .time)
                    .font(.caption2)
                    .foregroundColor(.gray)
                    .padding(chat.sendersPhoneNumber == profile.user.phoneNumber ? .leading : .trailing, 10)
                if chat.sendersPhoneNumber == profile.user.phoneNumber{
                    Text("Sent by me")
                        .font(.caption2)
                        .foregroundColor(.gray)
                        .padding(chat.sendersPhoneNumber == profile.user.phoneNumber ? .leading : .trailing, 10)
                }
                if chat.sendersPhoneNumber != profile.user.phoneNumber{
                    Text("Sent by:  " + chat.sendersPhoneNumber)
                        .font(.caption2)
                        .foregroundColor(.gray)
                        .padding(chat.sendersPhoneNumber == profile.user.phoneNumber ? .leading : .trailing, 10)
                }
                }
            }
            
            if chat.sendersPhoneNumber == profile.user.phoneNumber{
                ProfileBubble(message: chat, currentUser: profile.user)
            }
            
            if chat.sendersPhoneNumber != profile.user.phoneNumber{
                Spacer(minLength: 0)
            }
        }
        .padding(.horizontal)
        .id(chat.id) //for scroll reader
    }
}

struct ProfileBubble : View {
    var message : Message
    var currentUser: User
    
    var body: some View {
        //sets the profile bubble to the users first inital
        
        ApplicationImageViaPhoneNumber(phoneNumber: message.sendersPhoneNumber)
            .aspectRatio(contentMode: .fill)
            .frame(width: 70, height: 70)
            .cornerRadius(150)
    }
}


struct ChatRow_Previews: PreviewProvider {
    static var previews: some View {
        ChatRow(chat: MessageMockData.sampleMessage1).environmentObject(ProfileData(user: UserMockData.sampleUser))
    }
}
