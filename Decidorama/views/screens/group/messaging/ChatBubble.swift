//
//  ChatBubble.swift
//  Decidorama
//
//  Created by Zakim Javer on 2021-10-30.
//

import SwiftUI

struct ChatBubble: Shape {
    
    let myMessage : Bool
    
    func path(in rect: CGRect) -> Path {
        
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: [.topLeft, .topRight, myMessage ? .bottomLeft : .bottomRight], cornerRadii: CGSize(width: 15, height: 15))
        
        return Path(path.cgPath)
    }
}

