//
//  message.swift
//  Decidorama
//
//  Created by Zakim Javer on 2021-10-29.
//

import SwiftUI

struct messageCell: View {
    
    @State var name = "Zakim Javer"
    
    var body: some View {
        ZStack{
            
            VStack{
                Image(systemName: "person.circle.fill")
                    .resizable()
                    .frame(width: 60, height: 60)
                    .padding(.top, 12)
                
                TextField("Name", text: $name)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .padding()
                
                NavigationLink(destination: MessagePage(name: name)){
                    HStack{
                        Text("Join")
                        Image(systemName: "arrow.right.circle.fill")
                            .resizable()
                            .frame(width: 20, height: 20, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    }
                }.frame(width: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, height: 54, alignment: .center)
                .background(Color.orange)
                .foregroundColor(.white)
                .cornerRadius(27)
                .padding(.bottom, 15)
                
            }
            .background(Color.white)
            .cornerRadius(20)
            .padding()
        }
    }
}

struct message_Previews: PreviewProvider {
    static var previews: some View {
        messageCell()
    }
}

struct MessagePage : View {
    var name = ""
    
    var body: some View{
        Text(name)
    }
}
