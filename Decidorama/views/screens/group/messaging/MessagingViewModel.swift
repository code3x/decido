//
//  MessagingViewModel.swift
//  Decidorama
//
//  Created by Zakim Javer on 2021-10-30.
//

import Foundation
import Firebase

final class MessagingViewModel: ObservableObject{
    @Published var messages = [Message]()
    @Published var text : String = ""
    @Published var members = [User]()
    var ref = DatabaseReference()
    
    func onLoad(activityId: String){
        receiveMessages(activityId: activityId) {
            //message loaded successfully
        }
    }
    
    
    func sendMessage(sendersPhoneNumber: String, activityId: String){
        ActivityFunctions().sendMessage(sendersPhoneNumber: sendersPhoneNumber, activityId: activityId , message: text)
        text = ""
    }
    
    func receiveMessages(activityId: String, complete: @escaping () -> ()){
            ref = Database.database().reference().child("Activity").child(activityId).child("messages")
                ref.observe(.childAdded) { snapshot in
                //print("TEST123x", snapshot)
                if let data = snapshot.value as? Dictionary<String, Any>{
                    let timeStamp = "\(data["time"] ?? 0)"
                    let timeInSec = (Double(timeStamp) ?? 0)/1000
                    
                    let time = Date(timeIntervalSince1970: timeInSec)
                    let id = snapshot.key
                    let messageText = data["message"] as? String ?? ""
                    let sendersPhoneNumber = data["sender"] as? String ?? ""
                    
                    let message = Message(id: id, messageText: messageText, sendersPhoneNumber: sendersPhoneNumber, timeStamp: time)
                    
                    DispatchQueue.main.async {
                        self.messages.append(message)
                    }
                    complete()
                }else{
                    print("Error in receiving messages ")
                    complete()
                }
            } withCancel: { error in
                print(error)
            }

        }
}



