
import Foundation

final class ActivityListViewModel: ObservableObject{
    @Published var searchText = ""
    @Published var searching = false
    @Published var activities: [Activity] = []
    
    @Published var noActivities = false
    @Published var loading = false
}


