import SwiftUI
import Firebase

struct ActivityListView: View {
    @StateObject var viewModel = ActivityListViewModel()
    @EnvironmentObject var profile: ProfileData
    let activityFunctions = ActivityFunctions()
    
    var body: some View {
        
        VStack{
            Typography(text: "My Activities",size:17, weight: .semibold).padding(.trailing, 300)
            
            Rectangle().frame(height: 5)
                .padding(.horizontal, 0).foregroundColor(Color.purple)
                .padding(.bottom, 0)
            SearchBar(searchText: $viewModel.searchText, searching: $viewModel.searching)
            
            if(viewModel.loading){
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: .purple)).scaleEffect(x: 2.0, y: 2.0, anchor: .center).padding(20)
                
            }
            else if(viewModel.noActivities){
                Image("no-activities.png").resizable()
                    .scaledToFill()
                    .shadow(radius: 2)
                Typography(text: "There are no activities keep swiping.", size: 20).padding(15)
            }
            
            ScrollView {
                ForEach(viewModel.activities.filter({ (activity) -> Bool in
                    return activity.name.hasPrefix(viewModel.searchText) || viewModel.searchText == ""
                }), id: (\.id)) { activity in
                    ActivityRow(activity: activity).padding(.trailing, 40)
                }
            }
            
        }
        .navigationBarHidden(true)
        .onAppear{
            
            viewModel.loading = true
            activityFunctions.getUserGroups(userPhoneNumber: profile.user.phoneNumber){ result in
                viewModel.activities = []
                switch result {
                case .success(let activitiesIds):
                    viewModel.noActivities = false
                    for activityId in activitiesIds {
                        activityFunctions.getActivityById(activityId: activityId){ result in
                            switch result{
                            case .success(let activity):
                                viewModel.activities.append(activity)
                            case .failure(_):
                                print("something went wrong retrieving the activity")
                            }
                        }
                    }
                    viewModel.loading = false
                case .failure(_):
                    print("something went wrong or there are no groups")
                    viewModel.noActivities = true
                    viewModel.loading = false
                }
            }
            }
        .redacted(reason: viewModel.loading ? .placeholder : RedactionReasons())
        }
        
        
    }


struct ActivityListView_Previews: PreviewProvider {
    static var previews: some View {
        ActivityListView().environmentObject(ProfileData(user: UserMockData.sampleUser))
    }
}

struct SearchBar: View {
    
    @Binding var searchText: String
    @Binding var searching: Bool
    
    var body: some View {
        ZStack {
            Rectangle()
                .foregroundColor(Color("lightGray"))
            HStack {
                Image(systemName: "magnifyingglass")
                TextField("Search ..", text: $searchText) { startedEditing in
                    if startedEditing {
                        withAnimation {
                            searching = true
                        }
                    }
                } onCommit: {
                    withAnimation {
                        searching = false
                    }
                }
            }
            .foregroundColor(.gray)
            .padding(.leading, 13)
        }
        .frame(height: 40)
        .cornerRadius(13)
        .padding()
    }
}

extension UIApplication {
    func dismissKeyboard() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
