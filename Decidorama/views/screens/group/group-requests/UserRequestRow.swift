import SwiftUI

struct UserRequestRow: View {
    
    var request: User
    
    var body: some View {
        NavigationLink(
            destination: Text(request.name)
        ) {
            HStack{
                ApplicationImageStatic(urlString: request.avatar)
                    .aspectRatio(contentMode: .fill)
                    .clipped()
                    .clipShape(Circle())
                    .frame(width: 75, height: 75)
                    .padding(.leading, 20)
                    .padding(.trailing, 5)
                VStack (alignment: .leading, spacing: 10){
                    VStack (alignment: .leading, spacing: 2){
                        Text(request.name)
                            .foregroundColor(.primary)
                        Text("\(request.bio)")
                            .foregroundColor(.secondary)
                    }
                    HStack{
                        Button(action: {
                            print("Accept")
                        }) {
                            ZStack {
                                RoundedRectangle(cornerRadius: 5)
                                    .frame(height: 35)
                                    .foregroundColor(.blue)
                                Text("Accept")
                                    .font(.system(size: 13))
                                    .foregroundColor(.white)
                            }
                        }
                        Button(action: {
                            print("Decline")
                        }) {
                            ZStack {
                                RoundedRectangle(cornerRadius: 5)
                                    .frame(height: 35)
                                    .foregroundColor(.gray)
                                Text("Decline")
                                    .font(.system(size: 13))
                                    .foregroundColor(.white)
                            }
                        }
                        
                    }
                }
            }
        }
    }
}

struct UserRequestRow_Previews: PreviewProvider {
    static var previews: some View {
        UserRequestRow(request: UserMockData.sampleUser)
            .previewLayout(.sizeThatFits)
    }
}
