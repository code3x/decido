import Foundation

final class GroupRequestsViewModel: ObservableObject{
    @Published var requests: [User] = []
    
    @Published var noRequests = false
    @Published var loading = false
}


