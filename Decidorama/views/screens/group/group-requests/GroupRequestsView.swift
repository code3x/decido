import SwiftUI
import Foundation

struct GroupRequestsView: View {
    @StateObject var viewModel = GroupRequestsViewModel()
    @EnvironmentObject var profile: ProfileData
    
    let activityFunctions = ActivityFunctions()
    let userFunctions = UserFunctions()
    var activity: Activity
    @State var showProfile = false
    
    var body: some View{
        VStack{
            if(viewModel.loading){
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: .purple)).scaleEffect(x: 2.0, y: 2.0, anchor: .center).padding(20)
                
            }
            else if(viewModel.noRequests){
                Image("no-group-requests.png").resizable()
                    .frame(width: 350, height: 350, alignment: .center)
                    .scaledToFill()
                    .padding(.top, 100)
                
                Typography(text: "You don't have any requests", size: 20).padding(15).padding(.top, 30)
            }
            ScrollView {
                ForEach(viewModel.requests, id: (\.phoneNumber)) { request in
                    HStack{
                        ApplicationImageStatic(urlString: request.avatar)
                            .aspectRatio(contentMode: .fill)
                            .clipped()
                            .clipShape(Circle())
                            .frame(width: 75, height: 75)
                            .padding(.leading, 20)
                            .padding(.trailing, 5)
                            .onTapGesture {
                                showProfile.toggle()
                            }
                        
                        VStack (alignment: .leading, spacing: 10){
                            
                            VStack (alignment: .leading, spacing: 2){
                                Text(request.name)
                                    .frame(width: 300, alignment: .leading)
                                    .lineLimit(1)
                                    .foregroundColor(.primary)
                                    .onTapGesture {
                                        showProfile.toggle()
                                    }
                                
                                Text("\(request.program)")
                                    .frame(width: 300, alignment: .leading)
                                    .lineLimit(1)
                                    .foregroundColor(.secondary)
                                    .onTapGesture {
                                        showProfile.toggle()
                                    }
                            }
                            HStack{
                                Button(action: {
                                    activityFunctions.adminAcceptRequest(userPhoneNumber: request.phoneNumber, activityId: activity.activityId! )
                                    self.delete(at:self.viewModel.requests.firstIndex(where: { $0.phoneNumber == request.phoneNumber })!)
                                }) {
                                    ZStack {
                                        RoundedRectangle(cornerRadius: 5)
                                            .frame(height: 35)
                                            .foregroundColor(.blue)
                                        Text("Accept")
                                            .font(.system(size: 13))
                                            .foregroundColor(.white)
                                    }
                                }
                                Button(action: {
                                    activityFunctions.adminDeclineRequest(userPhoneNumber: request.phoneNumber, activityId: activity.activityId! )
                                    self.delete(at:self.viewModel.requests.firstIndex(where: { $0.phoneNumber == request.phoneNumber })!)
                                }) {
                                    ZStack {
                                        RoundedRectangle(cornerRadius: 5)
                                            .frame(height: 35)
                                            .foregroundColor(.gray)
                                        Text("Decline")
                                            .font(.system(size: 13))
                                            .foregroundColor(.white)
                                    }
                                }
                                
                            }
                        }
                    }
                    .sheet(isPresented: $showProfile) {
                        membersProfile(membersProfile: request, showProfile: $showProfile)
                    }
                    
                    
                }
                
                if(viewModel.requests.count == 0){
                    ProgressView()
                        .progressViewStyle(CircularProgressViewStyle(tint: .purple)).padding(.bottom, 250).scaleEffect(x: 2.0, y: 2.0, anchor: .center)
                }
            }
        }
        .onAppear{
            viewModel.requests = []
            viewModel.loading = true
            
            activityFunctions.getGroupRequests(activityId: activity.activityId!){ result in
                switch result {
                case .success(let requests):
                    viewModel.noRequests = false
                    for request in requests{
                        userFunctions.getUserByPhoneNumber(phoneNumber: request){ result in
                            switch result{
                            case .success(let member):
                                viewModel.requests.append(member)
                            case .failure(_):
                                print("something went wrong retrieving the activity")
                            }
                        }
                    }
                    viewModel.loading = false
                    
                case .failure(_):
                    print("something went wrong or there are no requests")
                    viewModel.noRequests = true
                    viewModel.loading = false
                }
            }
        }
    }
    
    func delete(at index: Int) {
        viewModel.requests.remove(at: index)
    }
}

struct GroupRequestsView_Previews: PreviewProvider {
    static var previews: some View {
        GroupRequestsView(activity: ActivityMockData.sampleActivity)
    }
}

