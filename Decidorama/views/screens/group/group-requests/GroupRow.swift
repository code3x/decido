import SwiftUI

struct UserRequestRow: View {
    
    var request: UserRequest
    
    var body: some View {
        NavigationLink(
            destination: Text(request.name)
        ) {
            HStack{
                Image(request.avatar)
                    .resizable()
                    .renderingMode(.original)
                    .aspectRatio(contentMode: .fill)
                    .clipped()
                    .clipShape(Circle())
                    .frame(width: 80, height: 80)
                VStack (alignment: .leading, spacing: 10){
                    VStack (alignment: .leading, spacing: 2){
                        Text(request.name)
                            .foregroundColor(.primary)
                        Text("\(request.bio)")
                            .foregroundColor(.secondary)
                    }
                    HStack{
                        Button(action: {
                            print("Accept")
                        }) {
                            ZStack {
                                RoundedRectangle(cornerRadius: 5)
                                    .frame(height: 35)
                                    .foregroundColor(.blue)
                                Text("Accept")
                                    .font(.system(size: 13))
                                    .foregroundColor(.white)
                            }
                        }
                        Button(action: {
                            print("Decline")
                        }) {
                            ZStack {
                                RoundedRectangle(cornerRadius: 5)
                                    .frame(height: 35)
                                    .foregroundColor(.gray)
                                Text("Decline")
                                    .font(.system(size: 13))
                                    .foregroundColor(.white)
                            }
                        }
                        
                    }
                }
            }
        }
    }
}

struct GroupRow_Previews: PreviewProvider {
    static var previews: some View {
        UserRequestRow(request: MockUserRequestData.sampleUserRequest)
            .previewLayout(.sizeThatFits)
    }
}
