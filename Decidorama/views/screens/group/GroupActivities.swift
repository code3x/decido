import Foundation
import SwiftUI

struct GroupActivities: View {
    
    @State private var itemList: [GroupActivitiesText] = [
        GroupActivitiesText(title: "Today", titleUnderline: "1", ActivityImage:"p0", ActivityName: "Bowling", ActivityMembers:"5"),
        GroupActivitiesText(title: "Upcoming", titleUnderline: "1", ActivityImage:"white", ActivityName: "No Activities", ActivityMembers:"0"),
        GroupActivitiesText(title: "Passed", titleUnderline: "1",ActivityImage:"p1", ActivityName: "Hiking", ActivityMembers:"3"),
        GroupActivitiesText(title: "Passed", titleUnderline: "2",ActivityImage:"p2", ActivityName: "Resteraunt", ActivityMembers:"5"),
        GroupActivitiesText(title: "Passed", titleUnderline: "2",ActivityImage:"camping", ActivityName: "Camping", ActivityMembers:"5"),
        GroupActivitiesText(title: "Passed", titleUnderline: "3",ActivityImage:"movies", ActivityName: "Movies", ActivityMembers:"4"),
        GroupActivitiesText(title: "Passed", titleUnderline: "4",ActivityImage:"cycling", ActivityName: "Bicyling", ActivityMembers:"2"),
        GroupActivitiesText(title: "Passed", titleUnderline: "4",ActivityImage:"paintball", ActivityName: "Paint Ball", ActivityMembers:"7"),
        GroupActivitiesText(title: "Passed", titleUnderline: "4",ActivityImage:"siteseeing", ActivityName: "Site Seeing", ActivityMembers:"4")
    ]
    
    @Binding var groupValue: String
    @Binding var groupName: String
    
    var body: some View {
        VStack(){
            List{
                Section {
                    
                    if (groupValue == "Empty"){
                        Text("Today")
                            .fontWeight(.semibold)
                        Rectangle().frame(height: 5)
                            .padding(.horizontal, 5).foregroundColor(Color.purple)
                            .padding(.bottom, 10)
                        Text("Upcoming")
                            .fontWeight(.semibold)
                        Rectangle().frame(height: 5)
                            .padding(.horizontal, 5).foregroundColor(Color.purple)
                            .padding(.bottom, 10)
                        Text("Passed")
                            .fontWeight(.semibold)
                        Rectangle().frame(height: 5)
                            .padding(.horizontal, 5).foregroundColor(Color.purple)
                            .padding(.bottom, 0)
                        
                    }
                    else {
                        
                        ForEach(0 ..< itemList.count) { number in
                            
                            if (itemList[number].title != "Passed"){
                                
                                Text(itemList[number].title)
                                    .fontWeight(.semibold)
                                Rectangle().frame(height: 5)
                                    .padding(.horizontal, 5).foregroundColor(Color.purple)
                                    .padding(.bottom, 0)
                                
                            }
                            
                            if (itemList[number].title == "Passed"){
                                if (itemList[number].titleUnderline == "1"){
                                    Text(itemList[number].title)
                                        .fontWeight(.semibold)
                                    Rectangle().frame(height: 5)
                                        .padding(.horizontal, 5).foregroundColor(Color.purple)
                                        .padding(.bottom, 0)
                                }
                            }
                            
                            Button(action: {
                                
                            }, label: {
                                itemList[number]
                            })
                            
                        }
                        
                    }
                }.id(UUID())
            }
        }
        .frame(
            minWidth: 0,
            maxWidth: .infinity,
            minHeight: 0,
            maxHeight: .infinity,
            alignment: .topLeading
        )
        .background(Color(.systemGray6))
        .edgesIgnoringSafeArea(.all)
        .navigationBarTitle(groupName, displayMode: .inline)
        
    }
}

struct GroupActivities_Previews: PreviewProvider {
    static var previews: some View {
        GroupActivities(groupValue: .constant("Test"), groupName: .constant("Test Name"))
    }
}


struct GroupActivitiesText: View {
    
    var title: String
    var titleUnderline: String
    var ActivityImage: String
    var ActivityName: String
    var ActivityMembers: String
    var body: some View{
        
        HStack(alignment: .top) {
            
            Image(ActivityImage).resizable()
                .frame(width: 40.0, height: 40.0)
                .padding(.leading, 3)
                .padding(.top, 2)
            
            VStack(alignment: .leading, spacing: 4) {
                
                Text(ActivityName)
                AcitivityTotalMembers(totalnumberofMembers: ActivityMembers)
            }
            .padding(.leading, 8)
            
        }.padding(.vertical, 10)
    }
}

struct AcitivityTotalMembers: View {
    
    var totalnumberofMembers: String
    var body: some View{
        VStack(alignment: .leading){
            if (totalnumberofMembers == "0"){
                Text("")
            }
            else{
                Text(totalnumberofMembers + " Members have voted on this activity")
                    .font(.system(size: 12))
            }}
    }
}
