import Foundation
import SwiftUI

struct addVotingCardView: View {
    
    @Binding var phoneNumber: String
    var activity: Activity
    @Binding var showAddVotingPage: Bool
    @StateObject var viewModel: VotingViewModel
    
    @State var title = ""
    @State var description = ""
    @State var duration = Date()
    @State var cardType = "Swiping"
    @State var option = ""
    @State var options = [String]()
    
    var cardTypes = ["Swiping", "Multiple Choice"]
    
    var body: some View{
        
        VStack{
            
            Form {
                Section(header: HeaderView(showAddVotingPage: $showAddVotingPage) ) {

                }//.padding(.top, 20)
                
                Section(header: Text("Choose a Voting Title")) {
                    TextField("e.g. Voting on a fast food place", text: $title)
                }.padding(.top, -10)
                
                Section(header: Text("Choose a Description")) {
                    //TextField("e.g. What is the vote", text: $description)
                    TextEditor(text: $description)
                        .frame(height: 100, alignment: .leading)
                        .cornerRadius(3)
                }
                
                Section(header: Text("Choose a card type")) {
                    Menu(cardType) {
                        ForEach(cardTypes, id: \.self) { type in
                            Button(type, action: {
                                cardType = "\(type)"
                            })
                            
                            
                        }
                    }
                    .padding(5)
                    .font(.system(size: 20, weight: .bold, design: .default))
                    .border(Color.white, width: 2)
                    .cornerRadius(8)
                }
                
                Section(header: Text("Options")) {
                    HStack{
                        TextField("Type your options here", text: $option)
                        
                        Button("Add Option") {
                            //add to options array
                            if !option.isEmpty{
                                options = options.filter { $0 != option }
                                options.append(option)
                                option = ""
                            }
                        }
                    }
                    
                    ScrollView{
                        ForEach(options, id: \.self) { opt in
                            Text(opt)
                                .foregroundColor(.black)
                                .onTapGesture {
                                    options = options.filter { $0 != opt }
                                }
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .frame(width: 100, height: 50)
                            
                        }
                    }
                }
                
                Section(header: Text("Choose a date")) {
                    DatePicker("Event duration: ", selection: $duration, displayedComponents: [.date, .hourAndMinute])
                        .foregroundColor(.purple)
                }
            }
            Button {
                //add card
                
                let card = VotingCard(id: "\(UUID())", cardType: cardType == "Swiping" ? .multiSelect : .multipleChoice, title: title, description: description, duration: duration, options: createOptions(options: options))
                
                viewModel.votingCards.append(card)
                VotingFunctions().createVotingCard(votingCard: card, activityId: activity.activityId ?? "")
                
                showAddVotingPage = false
                
            } label: {
                ApplicationButton(title: "Start Voting", backgroundColor: .purple, textColor: .white)
            }
        }
        .padding(.bottom, 30)
    }
}

struct HeaderView: View {
    @Binding var showAddVotingPage: Bool
    var body: some View{
        HStack{
            Text("Voting Card").fontWeight(.bold).font(.system(size: 30))
            Spacer()
            Button(action: {
                showAddVotingPage = false
            }) {
                
                Image("close").renderingMode(.original).resizable().frame(width: 20, height: 20)
            }.padding()
        }
    }
    
}

