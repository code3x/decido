import SwiftUI
import SwiftUICharts

struct VotingAnalyticsComponent: View {
    
    let chartColors: [ColorGradient] = [ColorGradient(.blue, .purple),
                                        ColorGradient(.orange,.red),
                                        ColorGradient(.green, .purple),
                                        ColorGradient(.pink, .purple),
                                        ColorGradient(.yellow, .blue)]
    @State var votingResults: [String]
    
    var votingCardId: String
    var activityId: String
    
    
    var body: some View {
        HStack{
            VStack{
                Text("Voting results")
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .font(Font.title2)
                    .padding()
                
                VStack{
                    BarChart()
                        .data(getResultRatio(results: votingResults))
                        .chartStyle(ChartStyle(backgroundColor: .white,foregroundColor: chartColors))
                        .frame(width: 120, height: 120)
                    
                }.padding(.bottom, 10)
                
                VStack{
                    
                    ForEach(Array(zip(getUniqueResults(results: votingResults).indices, getUniqueResults(results: votingResults))), id: \.0){index, result in
                        HStack{
                            Circle()
                                .fill(LinearGradient(gradient: chartColors[index].gradient,startPoint: .top,endPoint: .bottom))
                                .frame(width: 20, height: 20)
                                .padding(.leading, 10)
                            
                            Text("\(result) (\(getOccurrences(results: votingResults, result: result)))")
                                .font(Font.title3)
                               
                            Spacer()
                            
                        }
                        .padding(.bottom, 10)
                    }
                    
                }.padding([.top, .bottom], 5)
            }.frame(width: 340)
            .background(Color.white)
            .cornerRadius(15)
            
        }
        .onAppear {
            VotingFunctions().getActivityVotingCardResult(activityId: activityId, votingCardId: votingCardId) { result in
                switch result{
                case .failure(let error):
                    print(error.localizedDescription)
                case .success(let votingRes):
                    votingResults = votingRes
                }
            }
        }
    }
}

struct VotingAnalyticsComponent_Previews: PreviewProvider {
    static var previews: some View {
        VotingAnalyticsComponent(votingResults: ["pizza", "burger"], votingCardId: "A", activityId: "B")
    }
}

func getUniqueResults(results: [String]) -> [String]{
    return Array(Set(results))
    
}

func getResultRatio(results: [String]) -> [Double]{
    
    let uniqueResults: [String] = Array(Set(results))
    var ratios: [Double] = []
    
    
    for uniqueResult in uniqueResults {
        var occurrence: Double = 0
        for result in results {
            if uniqueResult == result{
                occurrence += 1
            }
        }
        ratios.append(occurrence)
    }
    return ratios
}

func getOccurrences(results: [String], result: String) -> Int{
    
    var occurrence: Int = 0
    for value in results {
        if value == result{
            occurrence += 1
        }
    }
    
    return occurrence
}

