//
//  VotingViewModel.swift
//  Decidorama
//
//  Created by Zakim Javer on 2021-11-18.
//
import Foundation
import Firebase

final class VotingViewModel: ObservableObject{
    
    @Published var votingCards = [VotingCard]()
    @Published var votingDecisions = [String]()
    
    @Published var loading: Bool = false
    
    @Published var showAddVotingPage = false
    
    
    func getCards(activityId: String){
        loading = true
        VotingFunctions().getActivityVotingCards(activityId: activityId) { result in
            switch result{
            case .failure(let error):
                print(error.localizedDescription)
                // no Voting cards
            case .success(let votingCards):
                self.votingCards = votingCards
            }
        }
        loading = false
    }
    
    func submitVote(activityId: String, usersPhoneNumber: String, votingCardId: String){
        //print("sub voting id: ", currentVotingCardId)
        VotingFunctions().registerUserVote(votingCardId: votingCardId, activityId: activityId, usersPhoneNumber: usersPhoneNumber, userVotes: votingDecisions)
        
        
        
        self.votingDecisions = []
//        votingCards = []
//        let seconds = 4.0
//        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
//            self.getCards(activityId: activityId)
//        }

    }
}



