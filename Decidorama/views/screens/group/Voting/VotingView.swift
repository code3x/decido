import SwiftUI
import CardStack

struct VotingView: View {
    
    @EnvironmentObject var profile: ProfileData
    @StateObject var viewModel = VotingViewModel()
    var activity: Activity
    
    var body: some View {
        ZStack{
            
            if viewModel.loading {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: .purple)).scaleEffect(x: 2.0, y: 2.0, anchor: .center).padding(20)
            }
            else if (viewModel.votingCards.count == 0){
                VStack{
                    Image("Voting.png")
                        .resizable()
                        .frame(width: 350, height: 350, alignment: .center)
                        .scaledToFill()
                    Typography(text: "There are no voting sessions 😲", size: 20).padding(.top, 15)
                    Typography(text: "Ask an admin to start one.", size: 20)
                }
            }
            
            ScrollView{
                ForEach(viewModel.votingCards) { votingCard in
                    VotingCardView(votingCard: votingCard, viewmodel: viewModel, phoneNumber: profile.user.phoneNumber, activity: activity)
                        .shadow(radius: 10)
                        .padding()
                }
            }
            
        }
        .toolbar {
            ToolbarItem(placement: .primaryAction) {
                if activity.owner == profile.user.phoneNumber{
                    Button("Create") {
                        viewModel.showAddVotingPage.toggle()
                    }
                }
                
            }
        }
        .sheet(isPresented: $viewModel.showAddVotingPage) { 
            withAnimation(.easeInOut) {
                addVotingCardView(phoneNumber: $profile.user.phoneNumber, activity: activity, showAddVotingPage: $viewModel.showAddVotingPage, viewModel: viewModel)
            }
        }
        .onAppear {
            viewModel.getCards(activityId: activity.activityId ?? "")
            
        }
    }
}

struct VotingView_Previews: PreviewProvider {
    static var previews: some View {
        VotingView(activity: ActivityMockData.sampleActivityZJ).environmentObject(ProfileData(user: UserMockData.sampleUser))
    }
}

struct TimerView : View {
    
    @State var nowDate: Date = Date()
    
    let setDate: Date
    let activityID: String
    @StateObject var viewModel = VotingViewModel()
    
    var timer: Timer {
        Timer.scheduledTimer (withTimeInterval: 1, repeats: true) {_ in
            self.nowDate = Date()
        }
    }
    
    var body: some View {
        Text (TimerFunction(from: setDate))
            .onAppear (perform: { self.timer})
            .font(.system(size: 16, weight: .semibold, design: .default))
            .foregroundColor(.white)
        
    }
    
    func TimerFunction(from date: Date) -> String {
        let calendar = Calendar(identifier: .gregorian)
        let timeValue = calendar
            .dateComponents([.day, .hour, .minute, .second], from: nowDate, to: setDate)
        
        if timeValue.day ?? 0 > 0{
            return String(format: "Time left: %02d days, %02d hours",
                          timeValue.day!,
                          timeValue.hour!)
        }else if timeValue.hour ?? 0 > 0{
            return String(format: "Time left: %02d hours,  %02d minutes",
                          timeValue.hour!,
                          timeValue.minute!)
        }else if timeValue.second ?? 0 > 0 || timeValue.minute ?? 0 > 0{
            return String(format: "Time left: %02d minutes,  %02d seconds",
                          timeValue.minute!,
                          timeValue.second!)
        }else{
            return "Completed"
        }
    }
}


struct VotingCardView: View {
    
    @State var votingCard: VotingCard
    @State var startVoting: Bool = false
    @ObservedObject var viewmodel: VotingViewModel
    
    var phoneNumber: String
    var activity: Activity
    
    var cardWidth   = UIScreen.main.bounds.size.width*0.9
    var increasedHeight = UIScreen.main.bounds.size.width*0.9 + 80
    
    //updating date & voted
    @State var nowDate: Date = Date()
    @State var userVoted: Bool = false
    
    var timer: Timer {
        Timer.scheduledTimer (withTimeInterval: 1, repeats: true) {_ in
            self.nowDate = Date()
        }
    }
    
    var body: some View{
        VStack(alignment: .leading, spacing: 15){
            Typography(text: votingCard.title, size: 22, weight: .bold, color: Color.white)
                .padding(.leading)
                .padding(.top)
            
            Typography(text: votingCard.description, size: 18, weight: .regular, color: .white)
                .padding(.leading)
                .padding(.trailing)
            
            if startVoting{
                if votingCard.duration < nowDate{
                    HStack{
                        Spacer()
                        VotingAnalyticsComponent(votingResults: votingCard.results, votingCardId: votingCard.id, activityId: activity.activityId ?? "")
                        Spacer()
                    }
                }else if userVoted{
                    HStack{
                        Spacer()
                        VStack(alignment: .center){
                            Image(systemName: "checkmark.seal.fill")
                                .resizable()
                                .foregroundColor(.green)
                                .frame(width: 100, height: 100)
                            
                            Typography(text: "I Voted", size: 24, weight: .bold, color: .black)
                        }
                        .padding()
                        
                        Spacer()
                    }
                }else{
                    switch votingCard.cardType{
                    case .multiSelect:
                        HStack{
                            VotingCardSwipingSystem(data: $votingCard.options, viewmodel: viewmodel, userPhoneNumber: phoneNumber, activityID: activity.activityId ?? "", votingCard: votingCard, userVoted: $userVoted)
                        }
                        .frame(height: 120)
                    case .multipleChoice:
                        VotingCardMultipleChoiceSystem(data: $votingCard.options, viewmodel: viewmodel, userPhoneNumber: phoneNumber, activityID: activity.activityId ?? "", votingCard: votingCard, userVoted: $userVoted)
                    }
                }
            }
            
            HStack{
                Spacer()
                TimerView(setDate: votingCard.duration, activityID: activity.activityId ?? "", viewModel: viewmodel)
                    .padding()
            }
        }
        .frame(width: cardWidth, alignment: .leading)
        .shadow(color: .black, radius: 20, x: 0.8, y: 0.8)
        .foregroundColor(.black)
        .background(Color("Purple_1"))
        .cornerRadius(20)
        .onTapGesture {
            startVoting.toggle()
        }
        .onAppear (perform: {
            self.timer
            userVoted = votingCard.usersVoted.contains(phoneNumber)
        })
        
    }
}

struct VotingCardMultipleChoiceSystem: View {
    @Binding var data: [VotingCardOptions]
    @State var option = ""
    @ObservedObject var viewmodel: VotingViewModel
    
    var userPhoneNumber: String
    var activityID: String
    @State var votingCard: VotingCard
    @Binding var userVoted: Bool
    
    var body: some View {
        ForEach(data, id: \.id) { votingCard in
            VStack{
                Text(votingCard.text)
                    .foregroundColor(option == votingCard.text ? Color.yellow : Color.white)
                    .frame(width: UIScreen.main.bounds.size.width*0.8, height: 30, alignment: .center)
                    .border(option == votingCard.text ? Color.yellow : Color.black, width: 2)
                    .cornerRadius(10)
                    .background(option == votingCard.text ? Color.yellow.opacity(0.3) : Color.gray.opacity(0.3))
                    .padding(.leading, 20)
            }
            .onTapGesture {
                option = votingCard.text
            }
        }
        
        Button {
            viewmodel.votingDecisions.append(option)
            votingCard.usersVoted.append(userPhoneNumber)
            userVoted = true
            viewmodel.submitVote(activityId: activityID, usersPhoneNumber: userPhoneNumber, votingCardId: votingCard.id)
            
        } label: {
            HStack{
                Spacer()
                Text("Vote")
                    .font(.title2)
                    .foregroundColor(Color.yellow)
                    .frame(width: 120, height: 35, alignment: .center)
                    .border(Color.black, width: 2)
                    .cornerRadius(10)
                    .background(Color("Purple_1").opacity(0.8))
                    .padding(.leading, 20)
                    .padding(.top)
                Spacer()
            }
            
        }
    }
}

struct VotingCardSwipingSystem: View {
    @Binding var data: [VotingCardOptions]
    @ObservedObject var viewmodel: VotingViewModel
    
    var userPhoneNumber: String
    var activityID: String
    @State var votingCard: VotingCard
    @Binding var userVoted: Bool
    
    @State var counter = 0
    @State var tempVotingDecisions = [String]()
    
    var body: some View {
        VStack(alignment: .center){
            CardStack(
                direction: LeftRight.direction,
                data: data,
                onSwipe: { card, direction in
                    
                    if "\(direction)" == "right"{
                        tempVotingDecisions.append("\(card.text)")
                    }
                    
                    counter  = counter+1
                    
                    if counter == data.count{
                        viewmodel.votingDecisions = tempVotingDecisions
                        viewmodel.submitVote(activityId: activityID, usersPhoneNumber: userPhoneNumber, votingCardId: votingCard.id)
                        votingCard.usersVoted.append(userPhoneNumber)
                        userVoted = true
                    }
                    
                },
                content: { option, direction, _ in
                    VotingCardWithYesNope(cardOption: option, direction: direction)
                }
            )
        }
    }
}

struct VotingCardWithYesNope: View {
    let cardOption: VotingCardOptions
    let direction: LeftRight?
    
    
    var body: some View {
        ZStack(alignment: .top){
            SwipingVotingCardView(cardOption: cardOption.text)
            
            Image("yes")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .opacity(direction == .right ? 1 : 0)
                .frame(width: 100, height: 100)
                .padding()
            
            Image("nope")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .opacity(direction == .left ? 1 : 0)
                .frame(width: 100, height: 100)
                .padding()
        }
        .animation(.default)
        
    }
}

struct SwipingVotingCardView: View {
    let cardOption: String
    
    var body: some View {
        HStack(alignment: .top){
            GeometryReader { geo in
                Typography(text: cardOption, size: 22, weight: .bold, color: .white)
                    .padding()
                    .background(Color("Purple_1"))
                    .cornerRadius(12)
                    .shadow(radius: 4)
                    .frame(width: geo.size.width, height: geo.size.height)
            }
            .frame(height: 100)
        }
    }
}
