import SwiftUI

struct ActivitySettings: View {
    var activity: Activity
    
    var body: some View {
        
        VStack{
            
            List{
                
                Section {
                    ZStack {
                        HStack {
                            
                            ZStack(alignment: .bottomTrailing) {
                                
                                Image(activity.image)
                                    .resizable()
                                    .frame(width: 58, height: 58)
                                    .cornerRadius(58/2)
                                
                            }
                            
                            VStack(alignment: .leading) {
                                Text(activity.name)
                                    .foregroundColor(.primary)
                                    .font(.system(size: 16, weight: .semibold)).padding(.bottom, 4)
                                Text("Edit")
                                    .foregroundColor(.secondary)
                                    .font(.system(size: 14, weight: .regular))
                            }.padding(.leading, 9)
                            
                            Spacer()
                            
                            HStack {
                                
                                Button(action: {
                                    print("btn click")
                                }, label: {
                                    Image(systemName: "qrcode")
                                        .resizable()
                                        .frame(width: 18, height: 15)
                                        .foregroundColor(.blue)
                                        .padding()
                                        .frame(width: 36, height: 36)
                                        .cornerRadius(36/2)
                                    
                                }).buttonStyle(PlainButtonStyle()).padding(.trailing, 5)
                                
                            }
                            
                        }
                        
                        NavigationLink(
                            destination: Text("Destination"),
                            label: {
                                EmptyView()
                            })
                        
                    }.padding(.vertical, 9)
                }
                
                Section{
                    NavigationLink(
                        destination: Text("Destination"),
                        label: {
                            HStack {
                                Text("Group 1").fontWeight(.regular)
                                    .padding(.leading, 8)
                            }.padding(.vertical, 4)
                        }
                    )
                    
                    NavigationLink(
                        destination: Text("Destination"),
                        label: {
                            HStack {
                                Text("Add group description").fontWeight(.regular)
                                    .padding(.leading, 8)
                            }.padding(.vertical, 4)
                        }
                    )
                    
                }
                
                Section{
                    
                    NavigationLink(
                        destination: StartSwipingView(),
                        label: {
                            HStack {
                                Text("Start Swiping").fontWeight(.regular)
                                    .padding(.leading, 8)
                            }.padding(.vertical, 4)
                        }
                    )
                    
                    //                    NavigationLink(
                    //                        destination: CustomSwipingView(groupValue: self.$groupValue,groupName: self.$groupName),
                    //                        label: {
                    //                            HStack {
                    //                                Text("Custom Swiping").fontWeight(.regular)
                    //                                    .padding(.leading, 8)
                    //                            }.padding(.vertical, 4)
                    //                        }
                    //                    )
                    
                }
                
                HStack{
                    
                    Text("Members")
                    Spacer()
                    Button("Add Member") {
                        print("btn click")
                    }
                }
                
                //                if (groupValue == "Empty"){}
                //                else {
                //                    ForEach(0 ..< itemList.count) { number in
                //
                //                        Button(action: {
                //
                //                        }, label: {
                //                            itemList[number]
                //                        })
                //
                //                    }.id(UUID())
                //                }
                
                Section{
                    
                    HStack{
                        Spacer()
                        Button(action: {print("btn click")}, label: {
                            Text("Leave Group")
                        }).foregroundColor(.red)
                        Spacer()
                    }
                    
                    HStack{
                        Spacer()
                        Button(action: {print("btn click")}, label: {
                            Text("Report Group")
                        }).foregroundColor(.red)
                        Spacer()
                    }
                }
            }.listStyle(GroupedListStyle())
        }
    }
}

struct GroupSettings_Previews: PreviewProvider {
    static var previews: some View {
        ActivitySettings(activity: ActivityMockData.sampleActivity)
    }
}

struct memberInfo: View {
    
    var memberName: String
    var memberCredentials: String
    var womenImage: String
    var body: some View{
        VStack(alignment: .leading){
            
            HStack(){
                Image(womenImage)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 40, height: 40)
                    .cornerRadius(150)
                
                Text(memberName)
                    .fontWeight(.semibold)
                    .font(.system(size: 13))
                    .frame(width: 100, height: 20)
                    .padding(.leading,-10)
                
                
                Text(memberCredentials)
                    .fontWeight(.semibold)
                    .font(.system(size: 12))
                    .frame(width: 380, height: 20)
            }
        }
        .padding(.leading, 150)
        .frame(
            alignment: .topLeading
        )
        .frame(minWidth: 200, maxWidth: 400)
        .padding(3)
        .background(Color(.white))
        .foregroundColor(.black)
        
    }
}

