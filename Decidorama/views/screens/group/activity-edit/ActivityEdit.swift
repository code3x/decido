import SwiftUI

struct ActivityEdit: View {
    
    
    @StateObject var viewModel = ActivityEditViewModel()
    //used to pop navigation controller
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    @EnvironmentObject var Activity: ActivityData
    
    @State private var tempImage: UIImage?
    @State private var image1: UIImage = UIImage(named: "ImagePlaceholder")!
    var activity: Activity
    
    
    var body: some View {
        
        ZStack(alignment: .top){
        
            VStack(){
                //activity: $viewModel.activity
                ActivityEditText(activity: activity, image: $image1, bottomColor: Color("lightGray")){
                    //change image function call
                    viewModel.showImagePicker = true
                }
                
                Form {
                    Section(header: Text("Edit Activity Info")) {
                        
                        activityItem(itemImage: "wand.and.stars", itemText: activity.name)
                            .frame(height: UIScreen.main.bounds.size.height*0.1).padding(.top, 20)
                        
                        activityItem(itemImage: "newspaper", itemText: activity.description)
                            .frame(width: 370, height: 140, alignment: .leading).multilineTextAlignment(.leading)
                        
                        activityItem(itemImage: "location", itemText: activity.location)
                            .frame(height: UIScreen.main.bounds.size.height*0.1)
                        
                    
                    }
                }
                HStack{
                    Button {
                        print("Save clicked")
                        //upload img and save user
                        //zj - needs to be updated once update user has been create on the backend
                        
                        viewModel.updateUser(image: image1) { user in
                            //Activity.ac = user
                            self.mode.wrappedValue.dismiss()
                        }
                        
                        
                    } label: {
                        ApplicationButton(title: "Save", backgroundColor: .purple, textColor: .white, width: UIScreen.main.bounds.size.width*0.4)
                    }.padding()
                    
                    Button {
                        print("Cancel clicked")//go back
                        self.mode.wrappedValue.dismiss()
                    } label: {
                        ApplicationButton(title: "Cancel", backgroundColor: .red, textColor: .white, width: UIScreen.main.bounds.size.width*0.4)
                    }.padding()
                }
                
            }
            backNavigationButton(){
                //
                self.mode.wrappedValue.dismiss()
            }
        }
        .navigationBarHidden(true)
        .sheet(isPresented: $viewModel.showImagePicker, onDismiss: setImage) {
            ImagePicker(image: self.$tempImage)
        }
        .onAppear(){
            //going back to Activity view (popping navigation)
            if viewModel.goToActivityView == true{
                self.mode.wrappedValue.dismiss()
            }
            
            //setting the image -> get image from cache
//            NetworkManager.shared.downloadImage(fromURLString: Activity.user.avatar) { uiImage in
//                guard let uiImage = uiImage else { return }
//                DispatchQueue.main.async {
//                    self.image1 = uiImage
//                }
//            }
            
            //setting up temp user in viewmodel
            //viewModel.user = Activity.user
        }
        
    }
    
    func setImage(){
        guard let img = tempImage else {
            print("Error: invalid image selected")
            return
        }
        image1 = img
    }
}

struct ActivityEdit_Previews: PreviewProvider {
    static var previews: some View {
        ActivityEdit(activity: ActivityMockData.sampleActivity).environmentObject(ActivityData(activity: ActivityMockData.sampleActivity))
    }
}


struct ActivityEditText: View {
   var activity : Activity
    @Binding var image : UIImage
    
    
    var bottomColor: Color = .white
    let action: () -> Void
    
    var body: some View {
        ZStack (alignment: .top){
            
            VStack{
                HStack{
                    //activityEditView(activity: activity).padding(.bottom, 40)
                    Spacer()
                    
                    ApplicationImageStatic(urlString: activity.image)
                        .aspectRatio(contentMode: .fill)
                        .frame(width: UIScreen.main.bounds.size.width*0.9, height: UIScreen.main.bounds.size.width*0.6, alignment: .center)
                        .clipped()
                        .cornerRadius(10)
                        .padding()
                        .onTapGesture {
                            self.action()
                        }
                }
                .padding(.top, 30)
                .padding(.bottom,30)
                
                Spacer()
            }.navigationTitle("Edit Activity")
            
        }
        .frame(height: 280)
        
        .background(
            LinearGradient(gradient: Gradient(colors: [bottomColor, Color("Purple_1"), .purple]), startPoint: .bottom, endPoint: .top).ignoresSafeArea()
        )
        
    }
}


struct activityEditView: View {
    
    var activity : Activity
    
    var body: some View {
        VStack(alignment: .leading){
            Typography(text: activity.name, size: 30, weight: .heavy, color: .white)
            Typography(text: activity.location, size: 20, color: .white)
            
        }
        
    }
}


struct activityItem: View {
    @State var itemImage: String
    @State var itemText: String
    
    var body: some View{
        HStack{
            Label("", systemImage: itemImage)
                .frame(maxWidth: 45, alignment: .trailing)
                .font(.system(size: 30))
                .foregroundColor(Color.purple)
            TextEditor(text: $itemText).multilineTextAlignment(.leading).padding(.top, 10)
        }.padding(.vertical)
    }
}
