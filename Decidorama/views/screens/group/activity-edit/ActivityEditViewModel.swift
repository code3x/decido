//
//  ActivityEditViewModel.swift
//  Decidorama
//
//  Created by Ali Muhammad on 2021-11-10.
//

import SwiftUI

final class ActivityEditViewModel: ObservableObject{
    
    @Published var goToActivityView: Bool = false
    @Published var goToPreferenceView: Bool = false
    @Published var user: User = UserMockData.emptyUser
    @Published var activity: Activity = ActivityMockData.emptyActivity
    
    @Published var showImagePicker = false
    
    func updateUser(image: UIImage, complete: @escaping (User) -> Void){
        
        print("Running NetworkManger Uploadimage")
        NetworkManager.shared.uploadImg(imag: image, phoneNumber: user.phoneNumber) {
            result in
            
            switch result {
                case .success(let url):
                    //save user
                    //zj need to update when update user is created
                    var newUser = self.user
                    newUser.avatar = url
                    UserFunctions().createUser(user: newUser)
                    
                    complete(newUser)
                case .failure(let error):
                    print("Unable to upload image when updating: ", error)
                }
        }
    }
}
