//
//  PreferenceEditView.swift
//  Decidorama
//
//  Created by Ali Muhammad on 2021-11-29.
//

import SwiftUI

struct PreferenceEditView: View {
    
    @EnvironmentObject var profile: ProfileData
    //@State var selectedPreferences: [UserPreference] = []
    @State var showingAlert: Bool = false
    @Binding var showPreferencesView: Bool
    @State var loading: Bool = true
    
    //var allPreferences = [UserPreference]() //= PreferenceMockData.samplePreferences
    
    var body: some View {
        ZStack{
            if !loading{
                VStack {
                    HStack{
                        Spacer()
                        
                        Button(action: {
                            showPreferencesView = false
                        }) {
                            Typography(text: "Preference page", size: 33, weight: .bold)
                            Spacer()
                            Image("close").renderingMode(.original).resizable().frame(width: 20, height: 20)
                        }.padding()
                    }
                
                
                Typography(text: "Select 5 interests that you'd like to share", size: 20, color: .gray).multilineTextAlignment(.center).padding(.vertical)
                
                Divider()
                
                Spacer()
                
                GeometryReader { geometry in
                    self.generateContent(in: geometry)
                }.padding([.leading, .top], 5)
                Spacer()
                Button(action: {
                    print("Total selections: ", profile.user.selectedPreferences.count)
                    if profile.user.selectedPreferences.count != 5{
                        showingAlert.toggle()
                    }else{
                        //save preferences
                        print("Changes saved. backend will be called here")
                                                
                        PreferenceFunctions()
                            .setUserPreference(userPhoneNumber: profile.user.phoneNumber,
                                               preferences: profile.user.selectedPreferences)
                        
                        showPreferencesView.toggle()
                    }
                }, label: {
                    ApplicationButton(title: "Save", backgroundColor: Color("Purple_2"), textColor: .white)
                })
                .padding(.bottom)
                .alert(isPresented: $showingAlert) {
                    Alert(
                        title: Text("Please select 5 interests"),
                        message: Text("You have selected \(profile.user.selectedPreferences.count)/5 interests")
                    )
                }
                
                Spacer()
            }
            }else{
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: .purple)).padding(.bottom, 250).scaleEffect(x: 2.0, y: 2.0, anchor: .center)
            }
        }
        .onAppear(){
            
            if profile.allPreferences.count > 0{
                loading = false
            }
        }
        //.navigationBarBackButtonHidden(true)
    }
    
    private func generateContent(in g: GeometryProxy) -> some View {
        var width = CGFloat.zero
        var height = CGFloat.zero
        
        return ZStack(alignment: .topLeading) {
            ForEach(profile.allPreferences, id: \.id) { preference in
                PreferenceShape(preference: preference, listOfPreferences: self.$profile.user.selectedPreferences)
                    .padding(.vertical, 5)
                    .padding(.horizontal, 4)
                    .alignmentGuide(.leading, computeValue: { d in
                        if (abs(width - d.width) > g.size.width)
                        {
                            width = 0
                            height -= d.height
                        }
                        let result = width
                        if preference.preferenceName == profile.allPreferences.last!.preferenceName {
                            width = 0 //last item
                        } else {
                            width -= d.width
                        }
                        return result
                    })
                    .alignmentGuide(.top, computeValue: {d in
                        let result = height
                        if preference.preferenceName == profile.allPreferences.last!.preferenceName {
                            height = 0 // last item
                        }
                        return result
                    })
            }
        }
    }
}

struct PreferenceEditView_Previews: PreviewProvider {
    static var previews: some View {
        PreferenceEditView(showPreferencesView: .constant(true)).environmentObject(ProfileData(user: UserMockData.sampleUser))
    }
}
