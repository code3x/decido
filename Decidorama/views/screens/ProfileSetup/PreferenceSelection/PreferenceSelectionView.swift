import SwiftUI

struct PreferenceSelectionView: View {
    
    @EnvironmentObject var profile: ProfileData
    //@State var selectedPreferences: [UserPreference] = []
    @State var showingAlert: Bool = false
    @State var goToHomePage: Bool = false
    @State var loading: Bool = true
    
    //var allPreferences = [UserPreference]() //= PreferenceMockData.samplePreferences
    
    var body: some View {
        ZStack{
            NavigationLink(destination:ApplicationTabView(),
                           isActive: $goToHomePage) { EmptyView()}
            
            if !loading{
                VStack {
                Typography(text: "Preference page", size: 33, weight: .bold)
                
                Typography(text: "Select 5 interests that you'd like to share. You must select at least 3 interests", size: 20, color: .gray).multilineTextAlignment(.center).padding(.vertical)
                
                Divider()
                
                Spacer()
                
                GeometryReader { geometry in
                    self.generateContent(in: geometry)
                }.padding([.leading, .top], 5)
                Spacer()
                Button(action: {
                    print("Total selections: ", profile.user.selectedPreferences.count)
                    if profile.user.selectedPreferences.count != 5{
                        showingAlert.toggle()
                    }else{
                        //save preferences
                        print("Changes saved. backend will be called here")
                                                
                        PreferenceFunctions()
                            .setUserPreference(userPhoneNumber: profile.user.phoneNumber,
                                               preferences: profile.user.selectedPreferences)
                        
                        goToHomePage.toggle()
                    }
                }, label: {
                    ApplicationButton(title: "Save", backgroundColor: Color("Purple_2"), textColor: .white)
                })
                .padding(.bottom)
                .alert(isPresented: $showingAlert) {
                    Alert(
                        title: Text("Please select 5 interests"),
                        message: Text("You have selected \(profile.user.selectedPreferences.count)/5 interests")
                    )
                }
                
                Spacer()
            }
            }else{
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: .purple)).padding(.bottom, 250).scaleEffect(x: 2.0, y: 2.0, anchor: .center)
            }
        }
        .onAppear(){
            
            if profile.allPreferences.count > 0{
                loading = false
            }
        }
        //.navigationBarBackButtonHidden(true)
    }
    
    private func generateContent(in g: GeometryProxy) -> some View {
        var width = CGFloat.zero
        var height = CGFloat.zero
        
        return ZStack(alignment: .topLeading) {
            ForEach(profile.allPreferences, id: \.id) { preference in
                PreferenceShape(preference: preference, listOfPreferences: self.$profile.user.selectedPreferences)
                    .padding(.vertical, 5)
                    .padding(.horizontal, 4)
                    .alignmentGuide(.leading, computeValue: { d in
                        if (abs(width - d.width) > g.size.width)
                        {
                            width = 0
                            height -= d.height
                        }
                        let result = width
                        if preference.preferenceName == profile.allPreferences.last!.preferenceName {
                            width = 0 //last item
                        } else {
                            width -= d.width
                        }
                        return result
                    })
                    .alignmentGuide(.top, computeValue: {d in
                        let result = height
                        if preference.preferenceName == profile.allPreferences.last!.preferenceName {
                            height = 0 // last item
                        }
                        return result
                    })
            }
        }
    }
}

struct PreferenceSelectionView_Previews: PreviewProvider {
    static var previews: some View {
        PreferenceSelectionView()
    }
}

struct PreferenceShape: View{
    
    var preference: UserPreference
    @State var selected: Bool = false
    @Binding var listOfPreferences: [UserPreference]
    
    
    var body: some View {
        Button(action: {
            selected = selected ? false : true
            
            if(selected){
                listOfPreferences.append(preference)
            }else{
                listOfPreferences.removeAll { value in
                    return value.id == preference.id
                }
            }
            
        }, label: {Text(preference.preferenceName)
            .padding(.horizontal, 20)
            .padding(.vertical, 11)
            .background( selected ? preference.preferenceColor : Color.clear)
            .border(preference.preferenceColor, width: 1)
            .font(.system(size: 20, weight: .bold,design: .default))
            .foregroundColor(selected ? .white : .black)})
        .onAppear(){
            if listOfPreferences.contains(where: { pref in
                preference.preferenceId == pref.preferenceId
            }){
                selected = true
            }
        }
    }
}
