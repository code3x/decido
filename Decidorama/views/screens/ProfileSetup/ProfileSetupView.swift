/*
 task:
    add program selection to profileSettupView
 
    error messages, first name validator, dob age (16-80?), gender selection, program selection (profileSettupView)
    connect error messages to button via progress (profileSettupView)
    UI for error message under button (profileSettupView)
 
     navigate to preference selection afer profile setup
     navigate to home page after selecting preferences
 
    update user for editing profile backend
 */


import SwiftUI

struct ProfileSetupView: View {
    
    @EnvironmentObject var profile: ProfileData
    @StateObject var viewModel = ProfileSetupViewModel()
    
    var body: some View {
        ZStack{
            NavigationLink(destination: PreferenceSelectionView(),
                           isActive: $viewModel.goToPreferenceSelectionView) { EmptyView()}
            
            VStack{
                ProgressBar(progression: $viewModel.progression)
                
                ZStack(alignment: .top){
                    switch viewModel.progression {
                    case 1:
                        InputFormText(tittle: "My full name is ", placeHolder: "Full Name", input: $profile.user.name)
                    case 2:
                        InputFormDate(tittle: "I was born on ", placeHolder: "Date of birth", input: $profile.user.dob)
                    case 3:
                        InputFormGender(tittle: "I am a ", gender: $profile.user.gender)
                    case 4:
                        //InputFormText(tittle: "I am studying", placeHolder: "Program", input: $profile.user.program)
                        
                        InputFormProgramSelect(tittle: "I am studying", placeHolder: "Program", program: $profile.user.program, programs: viewModel.programs)
                    case 5:
                        InputFormImageSelect(avatar: $profile.user.avatar){
                            viewModel.showImagePicker = true
                        }
                    case 6:
                        
                        VStack{
                            Text("Going to preference page \(String(viewModel.goToPreferenceSelectionView))")
                            
                        }.onAppear {
                            UserFunctions().createUser(user: User(phoneNumber: profile.user.phoneNumber, avatar: profile.user.avatar, bio: "bio", age: 0, gender: profile.user.gender, program: profile.user.program, name: profile.user.name, userStatus: "Available"))
                        }
                        
                    default:
                        Text("Something went wrong, Please restart the App")
                    }
                    
                    HStack{
                        Typography(text: "<", weight: .bold, color: .purple)
                            .onTapGesture {
                                viewModel.progression -= 1
                            }
                            .frame(width: 50, height: 50, alignment: .center)
                        
                        Spacer()
                    }
                }
                
                Button(action: {viewModel.nextAction(user: profile.user)}, label: {
                    ApplicationButton(title: "Next", backgroundColor: .purple, textColor: .white)
                })
                
                Typography(text: viewModel.errorMessage, size: 24, weight: .semibold, color: .red)
                    .padding()
                
               
                Spacer()
            }
        }
        .navigationTitle("Profile Setup")
        .navigationBarBackButtonHidden(true)
        .sheet(isPresented: $viewModel.showImagePicker, onDismiss: setImage) {
            ImagePicker(image: self.$viewModel.image)
        }
        .onAppear(){
            profile.user.program = viewModel.programs[0]
        }
    }
    
    func setImage(){
        viewModel.updateUserImage(phoneNumber: profile.user.phoneNumber, complete: { url in
            profile.user.avatar = url
        })
    }
}

struct ProfileSetupView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileSetupView().environmentObject(ProfileData(user: UserMockData.sampleUser))
    }
}

struct ProgressBar: View {
    @Binding var progression: Double
    
    var body: some View{
        ZStack(alignment: .leading){
            RoundedRectangle(cornerRadius: 0, style: .continuous)
                .frame(width: UIScreen.main.bounds.size.width, height: 10, alignment: .center)
                .foregroundColor(.black)
            
            RoundedRectangle(cornerRadius: 0, style: .continuous)
                .frame(width: UIScreen.main.bounds.size.width*CGFloat(progression/7), height: 10, alignment: .center)
                .background(LinearGradient(gradient: Gradient(colors: [.purple, Color("Purple_1"), .purple]), startPoint: .bottom, endPoint: .top))
                .foregroundColor(.clear)
        }
    }
}

struct InputFormProgramSelect: View {
    
    var tittle: String
    var placeHolder: String
    @Binding var program: String
    var programs: [String]
    
    var body: some View{
        VStack(spacing: 20){
            Text(tittle)
                .font(Font.system(size:40, design: .rounded))
                .fontWeight(.semibold)
                .lineLimit(2)
                .frame(width: 220, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: .leading)
            
            userItemDropdownReusable(itemImage: "text.book.closed", buttonText: $program, programs: programs)
                                    .frame(height: UIScreen.main.bounds.size.height*0.1)
                                    
            
        }
        .padding(.top, 80)
    }
}


struct InputFormText: View {
    
    var tittle: String
    var placeHolder: String
    @Binding var input: String
    
    var body: some View{
        VStack(spacing: 20){
            Text(tittle)
                .font(Font.system(size:40, design: .rounded))
                .fontWeight(.semibold)
                .lineLimit(2)
                .frame(width: 220, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: .leading)
            
            
            VStack {
                TextField(placeHolder, text: $input){ isEditing in
                    //self.isEditing = isEditing
                    print("Started Editing")
                } onCommit: {
                    print("Changes committed: ", input)
                }
                .foregroundColor(.black)
                .frame(width: 200, alignment: .leading)
                .autocapitalization(.words)
                .disableAutocorrection(true)
            }.customTextField(lineColor: .black).padding()
            
        }
        .padding(.top, 80)
    }
}

struct InputFormDate: View {
    var tittle: String
    var placeHolder: String
    @Binding var input: Date
    
    //convert date to string
    
    var body: some View{
        VStack(spacing: 20){
            Text(tittle)
                .font(Font.system(size:40, design: .rounded))
                .fontWeight(.semibold)
                .lineLimit(2)
                .frame(width: 220, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: .leading)
            
            DatePicker(
                placeHolder,
                selection: $input,
                displayedComponents: [.date]
            )
            .frame(width: 280, height: 250, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            .datePickerStyle(GraphicalDatePickerStyle())
            .padding(.top, 40)
            
        }
        .padding(.top, 80)
    }
}

struct InputFormGender: View {
    var tittle: String
    @Binding var gender: String
    
    var body: some View{
        VStack(spacing: 20){
            Text(tittle)
                .font(Font.system(size:40, design: .rounded))
                .fontWeight(.semibold)
                .lineLimit(2)
                .frame(width: 220, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: .leading)
                
            VStack {
                
                Button(action: {
                    gender = "Male"
                }, label: {
                    Text("Male")
                        .foregroundColor(gender == "Male" ? .purple : .black)
                        .fontWeight(.semibold)
                    
                })
                .frame(width: 220, height: 30, alignment: .center)
                .padding()
                .overlay(
                    RoundedRectangle(cornerRadius: 16)
                        .stroke(gender == "Male" ? Color.purple : Color.gray, lineWidth: 4)
                )
                .padding(.top, 20)
                
                Button(action: {
                    gender = "Female"
                }, label: {
                    Text("Female")
                        .foregroundColor(gender == "Female" ? .purple : .black)
                        .fontWeight(.semibold)
                    
                })
                .frame(width: 220, height: 30, alignment: .center)
                .padding()
                .overlay(
                    RoundedRectangle(cornerRadius: 16)
                        .stroke(gender == "Female" ? Color.purple : Color.gray, lineWidth: 4)
                )
                .padding(.top, 20)
                
                Button(action: {
                    gender = "Other"
                }, label: {
                    Text("Other")
                        .foregroundColor(gender == "Other" ? .purple : .black)
                        .fontWeight(.semibold)
                    
                })
                .frame(width: 220, height: 30, alignment: .center)
                .padding()
                .overlay(
                    RoundedRectangle(cornerRadius: 16)
                        .stroke(gender == "Other" ? Color.purple : Color.gray, lineWidth: 4)
                )
                .padding(.top, 20)
                .padding(.bottom, 54)
                
            }
        }
        .padding(.top, 80)
        
    }
}

private struct InputFormImageSelect: View{
    @Binding var avatar: String
    
    let action: () -> Void
    
    var body: some View{
        HStack(alignment: .center){
            VStack(spacing: 20){
                Text("Please select an avatar")
                    .font(Font.system(size:36, design: .rounded))
                    .fontWeight(.semibold)
                    .lineLimit(2)
                    .frame(width: 220, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: .leading)
                    
                
                ApplicationImage(urlString: $avatar)
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 300, height: 300, alignment: .center)
                    .clipped()
                    .cornerRadius(150)
                    .onTapGesture {
                        self.action()
                    }
            }
            .padding(.top, 80)
            .padding(.bottom, 50)
        }
    }
}
