//
//  PreferenceSelectionViewModel.swift
//  Decidorama
//
//  Created by Zakim Javer on 2021-10-07.
//
import SwiftUI
import Firebase


final class ProfileSetupViewModel: ObservableObject{
    @Published var progression: Double = 1
    @Published var dob: Date = Date()
    @Published var image: UIImage?
    @Published var showImagePicker = false
    @Published var errorMessage: String = ""
    @Published var goToPreferenceSelectionView: Bool = false
    
    
    
    
    private var errorMessages: [Double: String] = [1: "Your name should be more than 3 characters long.",
                                                   2: "You must be older then 16",
                                                   3: "Select a gender",
                                                   4: "select a program",
                                                   5: "select an image"]
    
    //https://www.ontariocolleges.ca/en/programs
    @Published var programs = ["Agriculture, Animal & Related Practices", "Arts & Culture", "Business, Finance & Administration", "Career & Preparation", "Computers & Telecommunications", "Culinary, Hospitality, Recreation & Tourism", "Education, Community & Social Services", "Energy, Environmental & Natural Resources", "Engineering & Technology", "Fire, Justice & Security", "Health, Food & Medical", "Media", "Professions & Trades", "Transportation & Logistics"]
    
    
    func nextAction(user: User){
        errorMessage = ""
        
        if validate(user: user){
            //errors were found
            errorMessage = errorMessages[progression] ?? "Unknown Error"
        }else{
            progression += 1
        }
    }
    
    //true = errors were found
    func validate(user: User) -> Bool{
        // error messages, first name validator, dob age (16-80?), gender selection, program selection
        switch progression {
        case 1:
            //Name Check
            if user.name == "" || user.name.count < 4{
                return true
            }else{
                return false
            }
            
        case 2:
            //DOB
            let now = Date()
            let birthday: Date = user.dob
            let calendar = Calendar.current

            let ageComponents = calendar.dateComponents([.year], from: birthday, to: now)
            let age = ageComponents.year!
            
            //check age
            if age < 16{
                return true
            }else{
                return false
            }
            
        case 3:
            //Gender
            if user.gender == ""{
                return true
            }else{
                return false
            }
        case 4:
            //I am studying", placeHolder: "Program", input: $profile.user.program)
            if user.program == ""{
                return true
            }else{
                return false
            }
        case 5:
            //img
            if user.avatar == ""{
                return true
            }else{
                goToPreferenceSelectionView = true
                return false
            }
        default:
            return true
        }
    }
    
    
    func updateUserImage(phoneNumber: String, complete: @escaping (String) -> Void){
        guard let img = image else {
            print("Error: invalid image selected")
            return
        }
        
        print("Running NetworkManger Uploadimage")
        NetworkManager.shared.uploadImg(imag: img, phoneNumber: phoneNumber) {
            result in
            
            switch result {
                case .success(let url):
                    complete(url)
                case .failure(let error):
                    self.errorMessage = error.localizedDescription
                }
        }
    }

}
