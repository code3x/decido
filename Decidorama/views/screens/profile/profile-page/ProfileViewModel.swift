import Foundation

final class ProfileViewModel: ObservableObject{
    @Published var user: User = UserMockData.sampleUser
    
    @Published var goToEditing: Bool = false
    
    @Published var goToLogin: Bool = false
    
    func logOut() {
        print("Logging out")
        goToLogin = true
        //removes previous
        UserDefaults.standard.set("", forKey: "phoneNumber")
    }
}
