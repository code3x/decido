import SwiftUI

struct ProfileView: View {
    @EnvironmentObject var profile: ProfileData
    @StateObject var viewModel = ProfileViewModel()
    
    var body: some View {
    
        VStack{
            ZStack{
                NavigationLink(destination: ProfileEdit(),
                               isActive: $viewModel.goToEditing) { EmptyView()}
                
                NavigationLink(destination: PhoneNumberEntryView().navigationBarBackButtonHidden(true)
                               , isActive: $viewModel.goToLogin) { EmptyView()}
                
                VStack{
                    VStack(alignment: .leading){
                        ProfileText(user: $profile.user).padding(.bottom,-30)
                        infoText(title: "Program", image: "text.book.closed", subText: profile.user.program)
                        infoText(title: "Biography", image: "newspaper", subText: profile.user.bio)
                        infoText(title: "Status", image: "person.crop.circle.badge.questionmark", subText: profile.user.userStatus)
                    }
                    
                    Spacer()
                    
                    logOut(){
                        print("Logging out")
                        profile.user = UserMockData.emptyUser
                        viewModel.logOut()
                    }
                    
                    Button(action: {
                        viewModel.goToEditing = true
                    }, label: {
                        ApplicationButton(title: "Edit", backgroundColor: Color("Purple_2"), textColor: .white)
                    }).padding(.bottom)
                    
                }
            }
            .navigationBarHidden(true)
            .onAppear{
                print("Avatar url: ", profile.user.avatar)
            }
            
        }
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView().environmentObject(ProfileData(user: UserMockData.sampleUser))
    }
}

struct ProfileText: View {
    @Binding var user : User
    var bottomColor: Color = .white
    var body: some View {
        VStack{
            HStack{
                VStack(alignment: .leading){
                    Typography(text: user.name, size: 30, weight: .heavy, color: .white)
                    Typography(text: user.phoneNumber, size: 20, color: .white)
                    
                }.padding(.all, 25)
                .padding(.top, 35)
                
                Spacer()
                ApplicationImage(urlString: $user.avatar)
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 150, height: 150, alignment: .center)
                    .clipped()
                    .cornerRadius(150)
                    .padding(.top, 38)
                    .padding(.trailing, 25)
            }
            .padding(.top,10)
            .padding(.bottom,30)
        }
        .frame(height: 280)
        .background(
            LinearGradient(gradient: Gradient(colors: [bottomColor, Color("Purple_1"), .purple]), startPoint: .bottom, endPoint: .top)
        )
        .ignoresSafeArea()
        
    }
}

struct infoText: View {
    
    var title: String
    var image: String
    var subText: String
    
    var body: some View{
        HStack{
            VStack(spacing: 0){
                Image(systemName: image)
                    .resizable()
                    .frame(width: 40, height: 40)
                    .foregroundColor(Color.purple)
            }
            VStack(alignment: .leading){
                Typography(text: title, size: 23, color: Color.gray)
                Typography(text: subText, size: 23, color: .black)
            }
            .padding(.leading,15)
            
        }.padding()
        
        Rectangle().frame(height: 1)
            .padding(.horizontal, 20).foregroundColor(Color.gray)
    }
}

struct logOut: View {
    let action: () -> Void
    
    var body: some View{
        Button(action: {
            self.action()
        }, label: {
            ApplicationButton(title: "Log Out", backgroundColor: .red, textColor: .white)
        }).padding(.bottom)
    }
}
