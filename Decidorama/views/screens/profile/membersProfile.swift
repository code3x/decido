//
//  membersProfile.swift
//  Decidorama
//
//  Created by Zakim Javer on 2021-11-14.
//

import SwiftUI

struct membersProfile: View {
    @State var membersProfile: User
    @Binding var showProfile: Bool

    
    var body : some View{
        
        ZStack{
            VStack{
            ApplicationImage(urlString: $membersProfile.avatar)
                .aspectRatio(contentMode: .fill)
                .frame(width: 400, height: 380)
                .clipped()
                .cornerRadius(50)
                .scaledToFill()
                .padding(.bottom, 20)

            }.padding(.bottom, 300)

            VStack{
    
                HStack{
                    Spacer()
                    
                    Button(action: {
                        showProfile = false
                    }) {
                        
                        Image("close").renderingMode(.original).resizable().frame(width: 20, height: 20)
                    }.padding()
                }
                
                Spacer()
                
                ZStack(alignment: .top) {
                    
                    VStack{
                        
                        HStack{
                            
                            VStack(alignment: .leading, spacing: 10) {
                                
                                Text(membersProfile.name).font(.title).bold()
                                Text(membersProfile.program)
                            }
                            
                            Spacer()
                            
                            HStack(spacing: 8){
                                Text(membersProfile.userStatus).padding(.trailing).padding(.leading, 10)
                                
                            }.padding(20)
                            .background(Color.black.opacity(0.1))
                            .cornerRadius(10)
                        }.padding(.top,25)
                        .padding()
                        
                        Text(membersProfile.bio).frame(width: 350, height: 140, alignment: .leading).padding(.leading, 15)
                        
                    }
                    .padding(.bottom, 20)
                    .padding(.leading, 20)
                    .background(Blurview())
                    .cornerRadius(25)
                }
                
            }.padding(.bottom, 40)
            .padding()
        }
    }
}

struct membersProfile_Previews: PreviewProvider {
    static var previews: some View {
        membersProfile(membersProfile: UserMockData.sampleUser, showProfile: .constant(true))
    }
}

struct Blurview : UIViewRepresentable {
    
    func makeUIView(context: UIViewRepresentableContext<Blurview>) -> UIVisualEffectView {
        
        
        let view = UIVisualEffectView(effect: UIBlurEffect(style: .systemUltraThinMaterialLight))
        
        return view
    }
    
    func updateUIView(_ uiView: UIVisualEffectView, context: UIViewRepresentableContext<Blurview>) {
        
        
    }
}
