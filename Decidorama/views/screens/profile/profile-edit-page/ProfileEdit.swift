import SwiftUI

struct ProfileEdit: View {
    
    init() {
        //used to hide the white background for the name (TextEditor)
        UITextView.appearance().backgroundColor = .clear
    }
    
    @StateObject var viewModel = ProfileEditViewModel()
    //used to pop navigation controller
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    @EnvironmentObject var profile: ProfileData
    
    @State private var tempImage: UIImage?
    @State private var image1: UIImage = UIImage(named: "ImagePlaceholder")!
    @State private var showPreferencesView = false
    
    var body: some View {
        
        ZStack(alignment: .top){
            VStack(){
                ProfileEditText(user: $viewModel.user, image: $image1, bottomColor: Color("lightGray")){
                    //change image function call
                    viewModel.showImagePicker = true
                }
                
                Form {
                    Section(header: Text("Edit User Info")) {
                        
                        userItemDropdownReusable(itemImage: "text.book.closed", buttonText: $viewModel.user.program, programs: viewModel.programs)
                            .frame(height: UIScreen.main.bounds.size.height*0.1)
                        
                        userItemDropdownReusable(itemImage: "newspaper", buttonText: $viewModel.user.userStatus, programs: viewModel.status)
                        
                        userItem(itemImage: "person.crop.circle.badge.questionmark", itemText: $viewModel.user.bio)
                            .frame(width: 370, height: 180, alignment: .leading).multilineTextAlignment(.leading)
                        
                        Button {
                            viewModel.goToPreferenceView = true
                            showPreferencesView = true
                        } label: {
                            HStack{
                                Label("", systemImage: "heart.text.square")
                                    .frame(maxWidth: 45, alignment: .trailing)
                                    .font(.system(size: 30))
                                    .foregroundColor(Color.purple)
                                Typography(text: "Edit Preference", size: 18)
                            }
                            
                        }
                    }
                }
                HStack{
                    Button {
                        print("Save clicked")
                        //upload img and save user
                        //zj - needs to be updated once update user has been create on the backend
                        
                        viewModel.updateUser(image: image1) { user in
                            profile.user = user
                            self.mode.wrappedValue.dismiss()
                        }
                        
                        
                    } label: {
                        ApplicationButton(title: "Save", backgroundColor: .purple, textColor: .white, width: UIScreen.main.bounds.size.width*0.4)
                    }.padding()
                    
                    Button {
                        print("Cancel clicked")//go back
                        self.mode.wrappedValue.dismiss()
                    } label: {
                        ApplicationButton(title: "Cancel", backgroundColor: .red, textColor: .white, width: UIScreen.main.bounds.size.width*0.4)
                    }.padding()
                }
                
            }
            backNavigationButton(){
                //
                self.mode.wrappedValue.dismiss()
            }
        }
        .navigationBarHidden(true)
        .sheet(isPresented: $viewModel.showImagePicker, onDismiss: setImage) {
            ImagePicker(image: self.$tempImage)
        }
        .sheet(isPresented: $showPreferencesView) {
            PreferenceEditView(showPreferencesView: $showPreferencesView)
        }
        .onAppear(){
            //going back to profile view (popping navigation)
            if viewModel.goToProfileView == true{
                self.mode.wrappedValue.dismiss()
            }
            
            //setting the image -> get image from cache
            NetworkManager.shared.downloadImage(fromURLString: profile.user.avatar) { uiImage in
                guard let uiImage = uiImage else { return }
                DispatchQueue.main.async {
                    self.image1 = uiImage
                }
            }
            
            //setting up temp user in viewmodel
            viewModel.user = profile.user
        }
        
    }
    
    func setImage(){
        guard let img = tempImage else {
            print("Error: invalid image selected")
            return
        }
        image1 = img
    }
}

struct ProfileEdit_Previews: PreviewProvider {
    static var previews: some View {
        ProfileEdit().environmentObject(ProfileData(user: UserMockData.sampleUser))
    }
}

struct userItem: View {
    @State var itemImage: String
    @Binding var itemText: String
    
    var body: some View{
        HStack{
            Label("", systemImage: itemImage)
                .frame(maxWidth: 45, alignment: .trailing)
                .font(.system(size: 30))
                .foregroundColor(Color.purple)
            TextEditor(text: $itemText)
        }.padding(.vertical)
    }
}



struct ProfileEditText: View {
    @Binding var user : User
    @Binding var image : UIImage
    
    
    var bottomColor: Color = .white
    let action: () -> Void
    
    var body: some View {
        ZStack (alignment: .top){
            
            VStack{
                HStack{
                    nameEditView(user: $user)
                        .padding(25)
                    
                    Spacer()
                    
                    Image(uiImage: image)
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 150, height: 150, alignment: .center)
                        .clipped()
                        .cornerRadius(150)
                        .padding(.top, 38)
                        .padding(.trailing, 25)
                        .onTapGesture {
                            self.action()
                        }
                }
                .padding(.top,10)
                .padding(.bottom,30)
                
                Spacer()
            }
            
        }
        .frame(height: 280)
        
        .background(
            LinearGradient(gradient: Gradient(colors: [bottomColor, Color("Purple_1"), .purple]), startPoint: .bottom, endPoint: .top).ignoresSafeArea()
        )
        
    }
}

struct nameEditView: View {
    
    @Binding var user : User
    
    var body: some View {
        VStack(alignment: .leading){
            TextEditor(text: $user.name)
                .font(.system(size: 30, weight: .heavy))
                .foregroundColor(.white)
                .multilineTextAlignment(.leading)
                .padding(.leading, -5)
            
            Typography(text: user.phoneNumber, size: 20, color: .white)
            
        }.frame(height: 120, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
        
    }
}

struct userItemDropdownReusable: View {
    
    @State var itemImage: String
    @Binding var buttonText: String
    
    var programs: [String]
    
    var body: some View{
        HStack{
    
            Image(systemName: itemImage)
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 40, height: 40)
                .foregroundColor(Color.purple)
                .clipped()
            
            Menu(buttonText) {
                ForEach(programs, id: \.self) { program in
                    Button(program, action: {
                        buttonText = "\(program)"
                    })
                }
            }
            .foregroundColor(.black)
        }.padding(.vertical)
    }
}

struct backNavigationButton: View {
    
    let action: () -> Void
    
    var body: some View{
        HStack{
            Button {
                self.action()
            } label: {
                Label("", systemImage: "chevron.backward")
                    .padding(.leading, 10)
                    .frame(width: 60, height: 50, alignment: .leading)
                    .font(.system(size: 30))
                    .foregroundColor(Color.blue)
            }
            .frame(width: 60, height: 50, alignment: .leading)
            Spacer()
        }
    }
}
