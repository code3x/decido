import SwiftUI

final class ProfileEditViewModel: ObservableObject{
    
    @Published var goToProfileView: Bool = false
    @Published var goToPreferenceView: Bool = false
    @Published var user: User = UserMockData.emptyUser
    
    @Published var showImagePicker = false
    
    //https://www.ontariocolleges.ca/en/programs
    @Published var programs = ["Agriculture, Animal & Related Practices", "Arts & Culture", "Business, Finance & Administration", "Career & Preparation", "Computers & Telecommunications", "Culinary, Hospitality, Recreation & Tourism", "Education, Community & Social Services", "Energy, Environmental & Natural Resources", "Engineering & Technology", "Fire, Justice & Security", "Health, Food & Medical", "Media", "Professions & Trades", "Transportation & Logistics"]
    
    @Published var status = ["Available", "Away", "Busy"]
    
    
    func updateUser(image: UIImage, complete: @escaping (User) -> Void){
        
        print("Running NetworkManger Uploadimage")
        NetworkManager.shared.uploadImg(imag: image, phoneNumber: user.phoneNumber) {
            result in
            
            switch result {
                case .success(let url):
                    //save user
                    //zj need to update when update user is created
                    var newUser = self.user
                    newUser.avatar = url
                    UserFunctions().createUser(user: newUser)
                    
                    complete(newUser)
                case .failure(let error):
                    print("Unable to upload image when updating: ", error)
                }
        }
    }
}
