import SwiftUI
import Foundation
import WaterfallGrid

struct RecommendedActivitiesView: View {
    
    @ObservedObject var viewModel = RecommendedActivitiesViewModel()
    @EnvironmentObject var profile: ProfileData

    var body: some View {
        
        let scrollDirection: Axis.Set = .vertical
        
        VStack{
            
            Typography(text: "These are some activities we think you would like.", size: 12).padding(.top, -30)
            
            
            if(viewModel.TokenTimer <= Date() ){
                Button(action: {
                    UserFunctions().setTokenTimer(userPhoneNumber: profile.user.phoneNumber)
                }, label: {
                    ApplicationButton(title: "Redeem Token", backgroundColor: .purple, textColor: .white)
                }).padding([.top, .bottom], 5)
            }
            
            TokenTimer(setDate: viewModel.TokenTimer)
            
            
            ScrollView(scrollDirection, showsIndicators: true) {
                WaterfallGrid((0..<viewModel.activities.count), id: \.self) { index in
                    RectangleView(activity: viewModel.activities[index], userPhoneNumber: profile.user.phoneNumber, scrollDirection: scrollDirection)
                }
                .gridStyle(
                    columnsInPortrait: 2,
                    spacing: CGFloat(10),
                    animation: .default
                )
                .scrollOptions(direction: scrollDirection)
                .padding(10)
            }
        }
        .onAppear(perform: {
            viewModel.getTokenTimer(userPhoneNumber: profile.user.phoneNumber)
            viewModel.getSwipedActivities(userPhoneNumber: profile.user.phoneNumber)
            for preference in profile.user.selectedPreferences{
                viewModel.getPreferenceActivities(preference: preference.preferenceName)
            }
            
        })
    }
}

struct RecommendedActivitiesView_Previews: PreviewProvider {
    static var previews: some View {
        RecommendedActivitiesView()
    }
}

struct RectangleView: View {
    let activity: Activity
    let userPhoneNumber: String
    let scrollDirection: Axis.Set
    @State var blurred: Bool = true
    
    var body: some View {
        Button {
            UserFunctions().useUserToken(userPhoneNumber: userPhoneNumber, activityId: activity.activityId!)
            blurred = false
        } label: {
            VStack() {
                
                ApplicationImageStatic(urlString: activity.image)
                    .frame(width: 200,height: CGFloat.random(in: 240...450), alignment: .center)
                    .blur(radius: blurred ? 12 : 0)
                
                
                HStack() {
                    VStack(alignment: .leading) {
                        Text(activity.name)
                            .font(.headline)
                            .foregroundColor(.primary)
                            .padding(.bottom, 8)
                            .fixedSize(horizontal: false, vertical: true)
                            .layoutPriority(98)
                            .redacted(reason: blurred ? .placeholder : RedactionReasons())
                        Text(activity.description)
                            .font(.caption)
                            .foregroundColor(.secondary)
                            .fixedSize(horizontal: false, vertical: true)
                            .layoutPriority(99)
                            .redacted(reason: blurred ? .placeholder : RedactionReasons())
                    }
                    Spacer()
                }
                .padding([.leading, .trailing, .bottom], 8)
            }
            .cornerRadius(8)
            .background(
                RoundedRectangle(cornerRadius: 8)
                    .stroke(Color.secondary.opacity(0.5))
            )
        }
        
        
    }
}

struct TokenTimer : View {
    
    @State var nowDate: Date = Date()
    
    let setDate: Date
    
    var timer: Timer {
        Timer.scheduledTimer (withTimeInterval: 1, repeats: true) {_ in
            self.nowDate = Date()
        }
    }
    
    var body: some View {
        Text (TimerFunction(from: setDate))
            .onAppear (perform: { self.timer})
            .font(.system(size: 16, weight: .semibold, design: .default))
        
    }
    
    func TimerFunction(from date: Date) -> String {
        let calendar = Calendar(identifier: .gregorian)
        let timeValue = calendar
            .dateComponents([.day, .hour, .minute, .second], from: nowDate, to: setDate)
        
        if timeValue.day ?? 0 > 0{
            return String(format: "Time left: %02d days, %02d hours",
                          timeValue.day!,
                          timeValue.hour!)
        }else if timeValue.hour ?? 0 > 0{
            return String(format: "Time left: %02d hours,  %02d minutes",
                          timeValue.hour!,
                          timeValue.minute!)
        }else if timeValue.second ?? 0 > 0 || timeValue.minute ?? 0 > 0{
            return String(format: "Time left: %02d minutes,  %02d seconds",
                          timeValue.minute!,
                          timeValue.second!)
        }else{
            return "Completed"
        }
    }
}
