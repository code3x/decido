import Foundation


final class RecommendedActivitiesViewModel: ObservableObject{
    
    @Published var activities: [Activity] = []
    @Published var swipedActivities: [String] = []
    @Published var loading = false
    @Published var TokenTimer: Date = Date()
   
    
    let activityFunctions = ActivityFunctions()
    
    
    func getSwipedActivities(userPhoneNumber: String){
        self.activities = []
        activityFunctions.getUserSwipedActivities(userPhoneNumber: userPhoneNumber){ result in
            switch result {
            case .success(let activityIds):
                self.swipedActivities = activityIds
            case .failure(_):
                print("There are no swiped activities for this user")
            }
        }
    }
    
    func getTokenTimer(userPhoneNumber: String){
        self.activities = []
        UserFunctions().getTokenTimer(userPhoneNumber: userPhoneNumber){ result in
            switch result {
            case .success(let tokenTimer):
                print(tokenTimer, "---------------------------- xx")
                self.TokenTimer = tokenTimer
            case .failure(_):
                print("there is no Date")
            }
        }
    }
    
    func getPreferenceActivities(preference: String){
        activityFunctions.getActivitiesByPreference(preference: preference){ result in
            switch result {
            case .success(let activityIds):
                
                for activityId in activityIds {
                    
                    if(!(self.swipedActivities.contains(activityId))){
                        self.activityFunctions.getActivityById(activityId: activityId){ result in
                            switch result {
                            case .success(let activity):
                                self.activities.append(activity)
                            case .failure(_):
                                print("There are no activities")
                            }
                        }
                        self.swipedActivities.append(activityId)
                    }
                }
            case .failure(_):
                print("There are no swiped activities for this user")
                
            }
        }
    }
    
}
