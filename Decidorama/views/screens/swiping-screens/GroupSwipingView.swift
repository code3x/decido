import SwiftUI
import CardStack

struct GroupSwipingView: View {
    @State var activities: [Activity] = []
    
    @EnvironmentObject var profile: ProfileData
    @ObservedObject var viewModel = GroupSwipingViewModel()
    
    var body: some View {
        VStack{
            
            NavigationLink(destination: AddActivity(), isActive: $viewModel.goToAddActivity){
                EmptyView()
            }
            
            NavigationLink(destination: ExternalActivitiesView(), isActive: $viewModel.showExternalActivities){
                EmptyView()
            }
            
            NavigationLink(destination: RecommendedActivitiesView(), isActive: $viewModel.goToRecommendedPage){
                EmptyView()
            }
            
            HStack{
                Button(action: {viewModel.showExternalActivities = true}, label: {
                    Text("External activities")
                })
                Spacer()
                
                Button(action: {viewModel.goToRecommendedPage = true}, label: {
                    Image(systemName: "flame.fill")
                        .resizable()
                        .scaledToFill()
                        .frame(width: 30, height: 30, alignment: .center)
                }).disabled(true)
                
                Spacer()
                
                Button(action: {
                    viewModel.goToAddActivity = true
//                    Api().loadData() {(events) in self.events = events}
                }, label: {
                    Text("+ Add activity")
                })
            }.padding([.leading, .trailing], 30)
            .padding(.top, 10)
            
            
            ActivitySwipingSystem(data: $viewModel.activities, analyticsOpen: $viewModel.analyticsOpen, activityForAnalytics: $viewModel.activityForAnalytics, history: $profile.preferenceHistory, userPhoneNumber: profile.user.phoneNumber)
            
            if(viewModel.loading){
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: .purple)).padding(.top, 30).scaleEffect(x: 2.0, y: 2.0, anchor: .center)
            }else if(viewModel.activities.count == 0){
                Image("Questions.png").resizable()
                    .frame(width: 350, height: 350, alignment: .center)
                    .scaledToFill()
                    .padding(.top, 100)
                Typography(text: "You have swiped on all the activities 😱", size: 16).padding().padding(.top, 25)
            }
            
            Spacer()
            
            Typography(text: "These activities are created by other Decidorama users.", size: 16, color: .gray).padding().padding(.top, 25)
            
        }.sheet(isPresented: $viewModel.analyticsOpen, content: {
            AnalyticsView(analyticsOpen: $viewModel.analyticsOpen, activity: $viewModel.activityForAnalytics)
        }).navigationBarHidden(true)
        .onAppear{
            
            var recommendedPreference = ""
            var count = 0
            
            for preference in profile.preferenceHistory{
                if preference.value > count{
                    count = preference.value
                    recommendedPreference = preference.key
                }
                
            }
            
            viewModel.loading = true
            viewModel.getSwipedActivities(userPhoneNumber: profile.user.phoneNumber)
            viewModel.getPreferenceActivities(preference: recommendedPreference)
            viewModel.loadSwipingCards()
            viewModel.loading = false
        }
    }
}

struct GroupSwipingView_Previews: PreviewProvider {
    static var previews: some View {
        GroupSwipingView(activities: ActivityMockData.mockActivities).environmentObject(ProfileData(user: UserMockData.sampleUser))
    }
}

struct ActivitySwipingSystem: View {
    @Binding var data: [Activity]
    @Binding var analyticsOpen: Bool
    @Binding var activityForAnalytics: Activity
    @Binding var history: [String: Int]
    var userPhoneNumber: String
    var body: some View {
        CardStack(
            direction: LeftRight.direction,
            data: data,
            onSwipe: { card, direction in
                print("Swiped \(card.name) to \(direction)")
                UserFunctions().registerUserSwipeV2(userPhoneNumber: userPhoneNumber, activityId: card.activityId!, directionSwiped: "\(direction)", activityPreference: card.preference, preferenceCount: (history[card.preference] != nil ? (history[card.preference]! + 1) : 1))
                
//                if (history[card.preference] != nil){
//                    history[card.preference] = (history[card.preference]! + 1)
//                }else{
//                    history[card.preference] = 0
//                }
            },
            content: { activity, direction, _ in
                ActivityCardWithYesNope(activity: activity, direction: direction, analyticsOpen: $analyticsOpen, activityForAnalytics: $activityForAnalytics)
            }
        )
        .padding(20)
        .padding(.top, 15)
    }
}


struct SwipingActivity: View {
    let activity: Activity
    
    var body: some View {
        
        GeometryReader { geo in
            VStack {
                ApplicationImageStatic(urlString: activity.image)
                    .aspectRatio(contentMode: .fill)
                    .frame(width: UIScreen.main.bounds.size.width*0.90, height: 570, alignment: .center)
                    .clipped()
                
                
                HStack {
                    Typography(text: self.activity.name, size: 20)
                    Spacer()
                    Text("\(self.activity.description)")
                        .font(.footnote)
                        .foregroundColor(.secondary)
                }
                .padding()
            }
            .background(Color.white)
            .cornerRadius(12)
            .shadow(radius: 4)
        }
    }
}

struct ActivityCardWithYesNope: View {
    let activity: Activity
    let direction: LeftRight?
    @Binding var analyticsOpen: Bool
    @Binding var activityForAnalytics: Activity
    
    var body: some View {
        ZStack(alignment: .topTrailing) {
            Button(action: {
                    activityForAnalytics = activity
                    analyticsOpen = true
            }, label: {
                Image(systemName: "chart.bar.xaxis")
                    .resizable()
                    .scaledToFill()
                    .frame(width: 40, height: 40)
            })
            .padding().zIndex(/*@START_MENU_TOKEN@*/1.0/*@END_MENU_TOKEN@*/)
            .background(Color(red: 1.0, green: 1.0, blue: 1.0, opacity: 0.4))
            
            
            ZStack(alignment: .topLeading) {
                SwipingActivity(activity: activity)
                Image("yes")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .opacity(direction == .right ? 1 : 0)
                    .frame(width: 100, height: 100)
                    .padding()
            }
            
            Image("nope")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .opacity(direction == .left ? 1 : 0)
                .frame(width: 100, height: 100)
                .padding()
        }
        .animation(.default)
    }
}


