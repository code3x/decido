import SwiftUI
import CardStack

struct ExternalActivitiesView: View {
    @State var activities: [Activity] = [ActivityMockData.sampleActivity]
    @EnvironmentObject var profile: ProfileData
    @Environment(\.presentationMode) var presentationMode
    var body: some View {
        VStack{
            HStack{
                Button(action: {self.presentationMode.wrappedValue.dismiss()}, label: {
                    Text("Back")
                })
                
                Spacer()
                Typography(text: "External Activities",size:22, weight: .semibold)
                    .padding(.leading, -15)
                
                Spacer()
                
            }.padding([.leading, .trailing], 30)
            
            Rectangle().frame(height: 5)
                .padding(.horizontal, 0).foregroundColor(Color.purple)
                .padding(.bottom, 0)
        
            ExternalActivitySwipingSystem(data: $activities, userPhoneNumber: profile.user.phoneNumber)
            
            Spacer()
            
            Typography(text: "These are activities from our external sources", size: 20, color: .gray)
                .padding(20)
            
        }.navigationBarHidden(true).navigationBarBackButtonHidden(false)
        .onAppear{
            ActivityFunctions().getUserSwipedActivities(userPhoneNumber: profile.user.phoneNumber){ result in
                switch result {
                case .success(let activityIds):
                    if activities.count <= 1 {
                        activities = []
                        ExternalActivityFunctions().getAllExternalActivities(filterOut: activityIds){ result in
                            switch result {
                            case .success(let activities):
                                self.activities = activities
                            case .failure(_):
                                print("There are no activities")
                            }
                        }
                    }
                case .failure(_):
                    print("There are no swiped activities for this user")
                    if activities.count <= 1 {
                        activities = []
                        ActivityFunctions().getAllActivities(){ result in
                            switch result {
                            case .success(let activities):
                                self.activities = activities
                            case .failure(_):
                                print("There are no activities")
                            }
                        }
                    }
                }
            }
        }
        
    }
}

struct ExternalActivitiesView_Previews: PreviewProvider {
    static var previews: some View {
        ExternalActivitiesView().environmentObject(ProfileData(user: UserMockData.sampleUser))
    }
}

struct ExternalActivitySwipingSystem: View {
    @Binding var data: [Activity]
    var userPhoneNumber: String
    var body: some View {
        CardStack(
            direction: LeftRight.direction,
            data: data,
            onSwipe: { card, direction in
                print("Swiped \(card.name) to \(direction)")
                ExternalActivityFunctions().registerUserExternalSwipe(userPhoneNumber: userPhoneNumber, activityId: card.activityId!, directionSwiped: "\(direction)")
            },
            content: { activity, direction, _ in
                ExternalActivityCardWithYesNope(activity: activity, direction: direction)
            }
        )
        .padding(20)
        .padding(.top, 40)
    }
}

struct ExternalActivityCardWithYesNope: View {
    let activity: Activity
    let direction: LeftRight?
    
    var body: some View {
        ZStack(alignment: .topTrailing) {

            ZStack(alignment: .topLeading) {
                SwipingActivity(activity: activity)
                Image("yes")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .opacity(direction == .right ? 1 : 0)
                    .frame(width: 100, height: 100)
                    .padding()
            }
            
            Image("nope")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .opacity(direction == .left ? 1 : 0)
                .frame(width: 100, height: 100)
                .padding()
        }
        .animation(.default)
    }
}
