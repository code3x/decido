import SwiftUI
import SwiftUICharts

struct AnalyticsView: View {
    @Binding var analyticsOpen: Bool
    @Binding var activity: Activity
    @State var users: [User] = []
    
    let chartColors: [ColorGradient] = [ColorGradient(.blue, .purple),
                                        ColorGradient(.orange,.red),
                                        ColorGradient(.green, .purple),
                                        ColorGradient(.pink, .purple),
                                        ColorGradient(.yellow, .blue)]
    
    @State var uniquePrograms: [String] = []
    
    var body: some View {
        ZStack{
            BackgroundColor(topColor: Color("Purple_1"), bottomColor: Color("Purple_2"))
            VStack(spacing: 10){
                HStack {
                    Typography(text: activity.name, size: 28, weight: .bold, color: .white).padding(.leading, 15)
                    
                    Spacer()
                    
                    Image(systemName: "person")
                        .resizable()
                        .foregroundColor(.white)
                        .imageScale(.large)
                        .frame(width: 30, height: 30)
                    Typography(text: "\(users.count)",size: 28, weight: .bold, color: .white)
                    
                    Button {
                        analyticsOpen = false
                    } label:{
                        Image(systemName: "xmark")
                            .resizable()
                            .foregroundColor(.white)
                            .imageScale(.large)
                            .frame(width: 30, height: 30)
                            .padding(25)
                    }
                }.padding(.top, 20)
                
                HStack(){
                    Image(systemName: "pencil.circle.fill")
                        .resizable()
                        .foregroundColor(.white)
                        .imageScale(.large)
                        .rotation3DEffect(
                            .degrees(180),
                            axis: (x: 1.0, y: 0.0, z: 0.0)
                        )
                        .frame(width: 30, height: 30)
                        .padding(.leading, 15)
                    
                    Typography(text: "Created by:", size: 20, color: .white)
                    Typography(text: "\(getActivityOwner(ownerId: activity.owner, users: users).name)", size: 20, weight: .bold, color: .white)

                }.frame(maxWidth: .infinity, alignment: .leading)
                
                
                Typography(text: activity.description, size: 20, color: .white)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding([.leading], 15)
                
                ScrollView{
                    HStack{
                        VStack(alignment: .leading){
                            Text("Space left in the activity")
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .padding(.leading)
                                .font(Font.title2)
                            
                            HStack{
                                
                                HStack(){
                                    Image(systemName: "leaf")
                                        .resizable()
                                        .imageScale(.large)
                                        .frame(width: 30, height: 30)
                                        .padding(.leading, 15)
                                    
                                    Typography(text: "\(users.count) / \(activity.memberLimit)",size: 30, weight: .bold)
                                    
                                    Image(systemName: "leaf")
                                        .resizable()
                                        
                                        .imageScale(.large)
                                        .rotation3DEffect(
                                            .degrees(180),
                                            axis: (x: 0.0, y: 1.0, z: 0.0)
                                        )
                                        .frame(width: 30, height: 30)
                                    
                                }.padding(.leading, 10)
                                
                                ZStack(alignment: Alignment(horizontal: .center, vertical: .center)){
                                    RingsChart()
                                        .data(getGroupSpace(memberCount: Double(users.count), groupLimit: Double(activity.memberLimit)))
                                        .chartStyle(ChartStyle(backgroundColor: .white,foregroundColor: [ColorGradient(.blue, .purple),ColorGradient(.orange,.red), ColorGradient(.green, .purple)]))
                                        .frame(width: 130, height: 130)
                                    
                                    Typography(text: "\(activity.memberLimit - users.count) left",size: 17, color: .gray)
                                }.padding(.leading, 25)
                                
                                
                                
                                
                                
                                
                                
                            }
                            
                        }.frame(width: 380, height: 200)
                        .background(Color.white)
                        .cornerRadius(15)
                        .shadow(radius: 5)
                    }
                    
                    HStack{
                        VStack{
                            Text("Gender ratio")
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .padding(.leading)
                                .font(Font.title2)
                            
                            HStack{
                                PieChart()
                                    .data(getGenderRatio(users: users))
                                    .chartStyle(ChartStyle(backgroundColor: .white,foregroundColor: [ColorGradient(.blue, .purple),ColorGradient(.orange,.red), ColorGradient(.green, .purple)]))
                                    .frame(width: 120, height: 120)
                                    .padding(.trailing, 35)
                                
                                VStack(alignment: .leading){
                                    HStack{
                                        Circle()
                                            .fill(LinearGradient(gradient: Gradient(colors: [.blue, .purple]),startPoint: .top,endPoint: .bottom))
                                            .frame(width: 20, height: 20)
                                        
                                        Text("Male")
                                            .font(Font.title3)
                                        
                                    }
                                    
                                    HStack{
                                        Circle()
                                            .fill(LinearGradient(gradient: Gradient(colors: [.orange, .red]),startPoint: .top,endPoint: .bottom))
                                            .frame(width: 20, height: 20)
                                        
                                        Text("Female")
                                            .font(Font.title3)
                                    }
                                    
                                    HStack{
                                        Circle()
                                            .fill(LinearGradient(gradient: Gradient(colors: [.green, .purple]),startPoint: .top,endPoint: .bottom))
                                            .frame(width: 20, height: 20)
                                        
                                        Text("Other")
                                            .font(Font.title3)
                                    }
                                }
                                
                            }
                            
                        }.frame(width: 380, height: 190)
                        .background(Color.white)
                        .cornerRadius(15)
                        .shadow(radius: 5)
                    }
                    
                    HStack{
                        VStack{
                            Text("Group programs")
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .font(Font.title2)
                                .padding()
                            
                            VStack{
                                BarChart()
                                    .data(getProgramRatio(users: users))
                                    .chartStyle(ChartStyle(backgroundColor: .white,foregroundColor: chartColors))
                                    .frame(width: 120, height: 120)
                                
                            }.padding(.bottom, 10)
                            
                            VStack(alignment: .leading){
                                
                                ForEach(Array(zip(getUniquePrograms(users: users).indices, getUniquePrograms(users: users))), id: \.0){index, program in
                                    HStack{
                                        Circle()
                                            .fill(LinearGradient(gradient: chartColors[index].gradient,startPoint: .top,endPoint: .bottom))
                                            .frame(width: 20, height: 20)
                                        
                                        Text(program)
                                            .font(Font.title3)
                                        
                                    }
                                    .padding(.bottom, 10)
                                }
                            }
                        }.frame(width: 380)
                        .background(Color.white)
                        .cornerRadius(15)
                        .shadow(radius: 5)
                    }
                }
                

                
                
            }
        }.onAppear{
            ActivityFunctions().getGroupMembersID(activityId: activity.activityId ?? ""){ result in
                switch result {
                case .success(let memberNumbers):
                    for phoneNumber in memberNumbers {
                        UserFunctions().getUserByPhoneNumber(phoneNumber: phoneNumber){ result in
                            switch result {
                            case .success(let user):
                                self.users.append(user)
                            case .failure(_): break
                            }
                        }
                        
                        
                    }
                case .failure(_): break
                }
                
            }
            uniquePrograms = getUniquePrograms(users: users)
        }
    }
}
struct AnalyticsView_Previews: PreviewProvider {
    static var previews: some View {
        AnalyticsView(analyticsOpen: .constant(true), activity: .constant(ActivityMockData.sampleActivity))
    }
}

func getGenderRatio(users: [User]) -> [(String, Double)]{
    
    var males: Double = 0.0
    var females: Double = 0.0
    var other: Double = 0.0
    
    for user in users{
        if user.gender == "Male"{
            males += 1
        }else if user.gender == "Female" {
            females += 1
        }else {
            other += 1
        }
    }
    
    return [("Male", males), ("Female", females), ("Other", other)]
}

func getProgramRatio(users: [User]) -> [Double]{
    
    let programs: [String] = users.map { $0.program }
    let uniquePrograms: [String] = Array(Set(programs))
    var ratios: [Double] = []
    
    
    for uniqueProgram in uniquePrograms {
        var occurrence: Double = 0
        for program in programs {
            if uniqueProgram == program{
                occurrence += 1
            }
        }
        ratios.append(occurrence)
    }
    return ratios
}

func getUniquePrograms(users: [User]) -> [String]{
    
    let programs: [String] = users.map { $0.program }
    return Array(Set(programs))
    
}

func getGroupSpace(memberCount: Double, groupLimit: Double) -> [Double]{
    
    let percentage: Double = (memberCount / groupLimit) * 100
    return [percentage]
    
}

func getActivityOwner(ownerId: String, users: [User]) -> User{
    
    for user in users{
        if user.phoneNumber == ownerId{
            return user
        }
    }
    
    return UserMockData.emptyUser
    
}


