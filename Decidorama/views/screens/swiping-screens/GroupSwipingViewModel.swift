import Foundation

final class GroupSwipingViewModel: ObservableObject{
    
    @Published var activities: [Activity] = []
    @Published var swipedActivities: [String] = []
    @Published var goToAddActivity: Bool = false
    @Published var goToRecommendedPage : Bool = false
    @Published var analyticsOpen = false
    @Published var loading = false
    @Published var activityForAnalytics: Activity = ActivityMockData.emptyActivity
    @Published var showExternalActivities = false
    
    let activityFunctions = ActivityFunctions()
    
    
    func getSwipedActivities(userPhoneNumber: String){
        self.activities = []
        self.swipedActivities = []
        activityFunctions.getUserSwipedActivities(userPhoneNumber: userPhoneNumber){ result in
            switch result {
            case .success(let activityIds):
                self.swipedActivities = activityIds
            case .failure(_):
                print("There are no swiped activities for this user")
            }
        }
    }
    
    func getPreferenceActivities(preference: String){
        activityFunctions.getActivitiesByPreference(preference: preference){ result in
            switch result {
            case .success(let activityIds):
                
                for activityId in activityIds {
                    
                    if(!(self.swipedActivities.contains(activityId))){
                        self.activityFunctions.getActivityById(activityId: activityId){ result in
                            switch result {
                            case .success(let activity):
                                self.activities.append(activity)
                            case .failure(_):
                                print("There are no activities")
                            }
                        }
                        self.swipedActivities.append(activityId)
                    }
                }
            case .failure(_):
                print("There are no swiped activities for this user")
                
            }
        }
    }
    
    func loadSwipingCards(){
        activityFunctions.getAllActivities(filterOut: swipedActivities){ result in
            switch result {
            case .success(let activities):
                self.activities += activities
            case .failure(_):
                print("There are no activities")
            }
        }
        
        activities = activities.reversed()
    }    
    
}
