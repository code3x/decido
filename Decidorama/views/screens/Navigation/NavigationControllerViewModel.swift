//
//  NavigationControllerViewModel.swift
//  Decidorama
//
//  Created by Zakim Javer on 2021-09-28.
//

import Foundation

final class NavigationControllerViewModel: ObservableObject{
    @Published var profileData = ProfileData()
    
    //this is temp. It will be used once we add remember me
    @Published var goToLogin = false
    @Published var goToHomeView = false
    
    func initalLoad(){
        let phoneNumber = UserDefaults.standard.string(forKey: "phoneNumber") ?? ""
        if phoneNumber != ""{
            loadUser(phoneNumber: phoneNumber)
        }else{
            goToLogin = true
            loadOtherData(phoneNumber: "")
        }
    }
    
    //remember me
    func loadUser(phoneNumber: String){
        UserFunctions().getUserByPhoneNumber(phoneNumber: phoneNumber) { [self] result in
            switch result {
            case .success(let user):
                self.profileData.user = user
                
                if user.name == ""{
                    goToLogin = true
                }else{
                    goToHomeView = true
                    //laod other data
                    loadOtherData(phoneNumber: phoneNumber)
                }
                
            case .failure(_):
                goToLogin = true
            }
        }
        
        UserFunctions().getUserSwipeHistory(userPhoneNumber: phoneNumber){ result in
            switch result {
            case .success(let history):
                self.profileData.preferenceHistory = history
                
            case .failure(_):
                print("empty history")
            }
        }
    }
    
    
    func loadOtherData(phoneNumber: String){
        loadAllPreferences(phoneNumber: phoneNumber)
    }
    
    func loadAllPreferences(phoneNumber: String){
        let prefFunctions = PreferenceFunctions()
        
        prefFunctions.getAllPreferences { [self] result in
            switch result {
            case .success(let usersPreferences):
                //save all preferences
                profileData.allPreferences = usersPreferences
                //get selected preferences
                if phoneNumber != "" {
                    loadUsersPreferences(phoneNumber: phoneNumber)
                }
                
                
            case .failure(_):
                print("Error, Cant get all preferences")
            }
        }
        
    }
    
    func loadUsersPreferences(phoneNumber: String){
        let prefFunctions = PreferenceFunctions()
        //get users requests
        prefFunctions.getUserPreferences(userPhoneNumber: phoneNumber) { [self] result in
            switch result {
            case .success(let usersPreferences):
                for pref in profileData.allPreferences{
                    if usersPreferences.contains(pref.preferenceId ?? ""){
                        //save selected preference to user
                        profileData.user.selectedPreferences.append(pref)
                    }
                }
            case .failure(_):
                print("Error")
            }
        }
        
    }
}
