import SwiftUI

struct NavigationController: View {
    @StateObject var viewModel = NavigationControllerViewModel()
    
    var body: some View {
        NavigationView {
            VStack{
                //initial loading page
                Text("Welcome to Decidorama")
                /*NavigationLink(destination: ApplicationTabView()
                               , isActive: $viewModel.goToHomeView) { EmptyView()}*/
                NavigationLink(destination: ApplicationTabView().navigationBarBackButtonHidden(true)
                               , isActive: $viewModel.goToHomeView) { EmptyView()}
                
                
                NavigationLink(destination: PhoneNumberEntryView().navigationBarBackButtonHidden(true)
                               , isActive: $viewModel.goToLogin) { EmptyView()}
                
            }
        }
        .navigationViewStyle(StackNavigationViewStyle())
        .environmentObject(viewModel.profileData)
        .onAppear(){
            viewModel.initalLoad()
        }
    }
}

struct NavigationController_Previews: PreviewProvider {
    static var previews: some View {
        NavigationController()
    }
}
