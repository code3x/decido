//
//  StartSwipingView.swift
//  Decidorama
//
//  Created by George on 2021-04-01.
//

import SwiftUI

struct StartSwipingView: View {
    @State private var checked = false
    @State private var checked2 = false
    @State private var checked3 = false
    var body: some View {
        VStack{
            HStack{
                Button(action:{
                    print("btn click")
                }, label:{
                    ApplicationButton(title: "Start Swiping", backgroundColor: .purple, textColor: .white, width: 160)
                })
                
                Button(action:{
                    checked = true
                    checked2 = true
                    checked3 = true
                }, label:{
                    ApplicationButton(title: "Select All", backgroundColor: .purple, textColor: .white, width: 140)
                })
                
                
            }.padding(.top, 20)
            
            List{
                Section{
                    
                    ZStack {
                        HStack {
                            Toggle(isOn: $checked){}.frame(width: 60, height: 60).padding(.trailing, 5)
                            ZStack(alignment: .bottomTrailing) {
                                Image("p0")
                                    .resizable()
                                    .frame(width: 80, height: 100)
                                    .cornerRadius(10)
                            }
                            
                            VStack(alignment: .leading) {
                                Text("Bowling")
                                    .foregroundColor(.primary)
                                    .font(.system(size: 16, weight: .semibold)).padding(.bottom, 4)
                                Text("Bowling is a really fun activity")
                                    .foregroundColor(.secondary)
                                    .font(.system(size: 14, weight: .regular))
                            }.padding(.leading, 9)
                            
                            Spacer()
                            
                        }
                        
                    }.padding(.top, 20)
                    
                    ZStack {
                        HStack {
                            Toggle(isOn: $checked2){}.frame(width: 60, height: 60).padding(.trailing, 5)
                            ZStack(alignment: .bottomTrailing) {
                                Image("p1")
                                    .resizable()
                                    .frame(width: 80, height: 100)
                                    .cornerRadius(10)
                            }
                            
                            VStack(alignment: .leading) {
                                Text("Hiking")
                                    .foregroundColor(.primary)
                                    .font(.system(size: 16, weight: .semibold)).padding(.bottom, 4)
                                Text("Walking with friends and family on a trail in a natural setting")
                                    .foregroundColor(.secondary)
                                    .font(.system(size: 14, weight: .regular))
                            }.padding(.leading, 9)
                            
                            Spacer()
                            
                        }
                        
                    }
                    
                    ZStack {
                        HStack {
                            Toggle(isOn: $checked3){}.frame(width: 60, height: 60).padding(.trailing, 5)
                            ZStack(alignment: .bottomTrailing) {
                                Image("p2")
                                    .resizable()
                                    .frame(width: 80, height: 100)
                                    .cornerRadius(10)
                            }
                            
                            VStack(alignment: .leading) {
                                Text("Resturant")
                                    .foregroundColor(.primary)
                                    .font(.system(size: 16, weight: .semibold)).padding(.bottom, 4)
                                Text("Go have breakfast and have coffee in your favourite resturant")
                                    .foregroundColor(.secondary)
                                    .font(.system(size: 14, weight: .regular))
                            }.padding(.leading, 9)
                            
                            Spacer()
                            
                        }
                        
                    }
                    
                }
            }
            
            Spacer()
        }
    }
}

struct StartSwipingView_Previews: PreviewProvider {
    static var previews: some View {
        StartSwipingView()
    }
}
