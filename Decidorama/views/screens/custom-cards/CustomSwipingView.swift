//
//  CustomSwipingView.swift
//  Decidorama
//
//  Created by George on 2021-04-01.
//

import SwiftUI

struct CustomSwipingView: View {
    @State var isNavigationBarHidden: Bool = true
    @State private var areYouGoingToCreateGroupView: Bool = false
    @Binding var groupValue : String
    @Binding var groupName : String
    var body: some View {
        
        
        VStack{
            HStack{
                Button(action:{
                    
                    areYouGoingToCreateGroupView = true
                    print("btn click")
                }, label:{
                    ApplicationButton(title: "Create Card", backgroundColor: .purple, textColor: .white, width: 150)
                })
                
                Button(action:{
                    print("btn click")
                }, label:{
                    ApplicationButton(title: "Delete Card", backgroundColor: .purple, textColor: .white, width: 150)
                })
            }.padding(.top, 20)
            
            // name, description, image, location, id
            Section {
                ZStack {
                    HStack {
                        
                        ZStack(alignment: .bottomTrailing) {
                            Image("p0")
                                .resizable()
                                .frame(width: 80, height: 100)
                                .cornerRadius(10)
                        }
                        
                        VStack(alignment: .leading) {
                            Text("Bowling")
                                .foregroundColor(.primary)
                                .font(.system(size: 16, weight: .semibold)).padding(.bottom, 4)
                            Text("Bowling is a really fun activity")
                                .foregroundColor(.secondary)
                                .font(.system(size: 14, weight: .regular))
                        }.padding(.leading, 9)
                        
                        Spacer()
                        
                    }
                    
                }.padding(.top, 20)
                
                
                ZStack {
                    HStack {
                        
                        ZStack(alignment: .bottomTrailing) {
                            Image("p1")
                                .resizable()
                                .frame(width: 80, height: 100)
                                .cornerRadius(10)
                        }
                        
                        VStack(alignment: .leading) {
                            Text("Hiking")
                                .foregroundColor(.primary)
                                .font(.system(size: 16, weight: .semibold)).padding(.bottom, 4)
                            Text("Walking with friends and family on a trail in a natural setting")
                                .foregroundColor(.secondary)
                                .font(.system(size: 14, weight: .regular))
                        }.padding(.leading, 9)
                        
                        Spacer()
                        
                    }
                    
                }
                
                ZStack {
                    HStack {
                        
                        ZStack(alignment: .bottomTrailing) {
                            Image("p2")
                                .resizable()
                                .frame(width: 80, height: 100)
                                .cornerRadius(10)
                        }
                        
                        VStack(alignment: .leading) {
                            Text("Resturant")
                                .foregroundColor(.primary)
                                .font(.system(size: 16, weight: .semibold)).padding(.bottom, 4)
                            Text("Go have breakfast and have coffee in your favourite resturant")
                                .foregroundColor(.secondary)
                                .font(.system(size: 14, weight: .regular))
                        }.padding(.leading, 9)
                        
                        Spacer()
                        
                    }
                    
                }
                
                Spacer()
            }.padding(.horizontal, 10)
        }
        .listRowInsets(EdgeInsets())
        .navigationBarTitle(Text("Custom Card"))
        
        
        .navigationBarHidden(isNavigationBarHidden)
        
        .onAppear {
            self.isNavigationBarHidden = false
        }
        .onDisappear {
            self.isNavigationBarHidden = false
        }
        
    }
}

struct CustomSwipingView_Previews: PreviewProvider {
    static var previews: some View {
        CustomSwipingView(groupValue: .constant("123"), groupName: .constant("123"))
    }
}
