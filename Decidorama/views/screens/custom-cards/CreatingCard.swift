//
//  CreatingCard.swift
//  Decidorama
//
//  Created by Zakim Javer on 2021-04-01.
//

import SwiftUI

struct CreatingCard: View {
    @State private var cardName: String = ""
    @State private var cardDescription: String = ""
    @State private var cardLocation: String = "Oakville"
    @State private var areYouGoingToSecondView: Bool = false
    @State var isNavigationBarHidden: Bool = true
    @Binding var groupValue : String
    @Binding var groupName : String
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    
    var body: some View {
        
        
        VStack(alignment: .leading) {
//            NavigationLink(destination: ActivitySettings(groupValue: self.$groupValue, groupName: self.$groupName)/*GroupView(itemList: itemList)*/.navigationBarBackButtonHidden(true)  .navigationBarHidden(isNavigationBarHidden)
//
//                            .onAppear {
//                                self.isNavigationBarHidden = true
//                            }
//                            .onDisappear {
//                                self.isNavigationBarHidden = false
//                            }, isActive: $areYouGoingToSecondView) { EmptyView()}
//
            List{
                HStack{
                    Spacer()
                    Image("p1")
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 300, height: 300)
                        .clipped()
                        .cornerRadius(150)
                        .padding(.bottom, 5)
                        .padding(.top, 5)
                    Spacer()
                }
//                LabelTextField(label: "CARD TITLE", mainText: $cardName, placeHolder: "Enter the cards title")
//                LabelTextField(label: "DESCRIPTION", mainText: $cardDescription, placeHolder: "Enter a short description")
                //RadioBtn(title: "Location", array: ["Oakville", "Toronto", "Hamilton"], selectedItem: $cardLocation)
                RoundedButton(btnTitle: "Create Card", action: {
                    areYouGoingToSecondView = true
                    print(" Name: ", cardName, " Desc: ", cardDescription, "/n loc: ", cardLocation)
                })
            }
            
        }
        .listRowInsets(EdgeInsets())
        .navigationBarTitle(Text("Creating Card"))
        .navigationBarItems(trailing:
                                Button(action: {
                                    //areYouGoingToSecondView = true
                                    self.mode.wrappedValue.dismiss()
                                }, label: {
                                    Text("Cancel")
                                })
        )
        
        .navigationBarHidden(isNavigationBarHidden)
        
        .onAppear {
            self.isNavigationBarHidden = true
        }
        .onDisappear {
            self.isNavigationBarHidden = false
        }
        
        
    }
}
//
//struct CreatingCard_Previews: PreviewProvider {
//    static var previews: some View {
//        CreatingCard()
//    }
//}
