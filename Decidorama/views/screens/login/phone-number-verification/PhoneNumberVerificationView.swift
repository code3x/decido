import SwiftUI
import Firebase

struct PhoneNumberVerificationView : View {
    
    @EnvironmentObject var profile: ProfileData
    
    @State var verificationNumber : String
    @State var loading = false
    
    @Binding var verified: Bool
    @Binding var firstTime: Bool
    @Binding var isVerificationPageOpen: Bool
    
    var body: some View {
        
        ZStack{
            
            BackgroundColor(topColor: Color("Purple_1"), bottomColor: Color("Purple_2"))
            
            VStack{
                
                HStack {
                    Spacer()
                    
                    Button {
                        isVerificationPageOpen = false
                    } label:{
                        Image(systemName: "xmark")
                            .foregroundColor(Color(.label))
                            .imageScale(.large)
                            .frame(width: 44, height: 44)
                    }
                }
                
                VStack{
                    Image("app_icon_white")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 150, height: 150, alignment: .center)
                        .padding(.top, 50)
                    Typography(text: "Please enter the verification code below", size: 20, weight: .semibold, color: .white)
                        .lineLimit(2)
                        .multilineTextAlignment(.center)
                        .padding( 10)
                        .padding(.leading, 30)
                        .padding(.trailing, 30)
                    
                    if loading{
                        ProgressView()
                            .progressViewStyle(CircularProgressViewStyle(tint: .white)).padding(.top, 30).scaleEffect(x: 2.0, y: 2.0, anchor: .center)
                    }
                }
                
                HStack {
                    TextField("Enter 5-digit code", text: $verificationNumber).foregroundColor(.white)
                }.customTextField().padding()
                
                Button(action: {
                    //btn action
                    print(verificationNumber)
                }) {
                    SubmitButtonContent(verified: $verified, verificationNumber: $verificationNumber, loading: $loading, user: $profile.user, firstTime: $firstTime, isVerificationPageOpen: $isVerificationPageOpen, btnText: "Verify")
                }
                
                Spacer()
            }
        }
        
    }
}

struct SubmitButtonContent : View {
    
    @Binding var verified: Bool
    @Binding var verificationNumber: String
    @Binding var loading: Bool
    
    @State var errorMsg = ""
    @State var error = false
    
    @Binding var user: User
    @Binding var firstTime: Bool
    @Binding var isVerificationPageOpen: Bool
    
    
    
    let verificationID = UserDefaults.standard.string(forKey: "authVerificationID")
    
    var btnText: String
    var body: some View {
        return Button(action: {
            print("Tapped")
            
            let credential = PhoneAuthProvider.provider().credential(
                withVerificationID: verificationID!,
                verificationCode: verificationNumber)
            
            print(credential)
            
            loading = true
            
            Auth.auth().signIn(with: credential) { (result, err) in
                
                self.loading = false
                
                if let error = err {
                    self.errorMsg = error.localizedDescription
                    //withAnimation(self.error.toggle())
                    print(errorMsg)
                    return
                }
                
                UserFunctions().getUserByPhoneNumber(phoneNumber: user.phoneNumber) { result in
                    switch result {
                    case .success(let user):
                        print(user, "-------")
                        
                        self.user = user
                        
                        if user.name == ""{
                            self.firstTime = true
                        }else{
                            let loader = NavigationControllerViewModel()
                            loader.initalLoad()
                            self.verified = true
                        }
                        
                        
                    case .failure(_):
                        //print("Error")
                        self.firstTime = true
                    }
                }
                
                isVerificationPageOpen = false
            }
            
        }, label: { ApplicationButton(title: "Submit", backgroundColor: Color("LightBlue"), textColor: .white)
        })
    }
}

#if DEBUG
struct PhoneNumberVerificationView_Previews : PreviewProvider {
    static var previews: some View {
        PhoneNumberVerificationView(verificationNumber: "123", verified: .constant(true), firstTime: .constant(false), isVerificationPageOpen: .constant(true)).environmentObject(ProfileData())
    }
}
#endif

