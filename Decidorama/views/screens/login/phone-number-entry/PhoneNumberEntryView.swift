import SwiftUI
import Firebase

struct PhoneNumberEntryView : View {
    
    @EnvironmentObject var profile: ProfileData
    @StateObject var viewModel = PhoneNumberEntryViewModel()
    
    var body: some View {
            
            ZStack{
                
                NavigationLink(destination: ApplicationTabView(),
                               isActive: $viewModel.isVerified) { EmptyView()}
                
                NavigationLink(destination: ProfileSetupView()
                               , isActive: $viewModel.firstTime) { EmptyView()}
                
                BackgroundColor(topColor: Color("Purple_1"), bottomColor: Color("Purple_2"))
                
                VStack{
                
                    VStack{
                        Image("app_icon_white")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 150, height: 150, alignment: .center)
                            .padding(.top, 50)
                        
                        
                        Typography(text: "Welcome to Decidorama!", size: 24, weight: .semibold, color: .white)
                        
                        Typography(text: "Connect with friends through the mobile app and start swiping", size: 17, color: .white)
                            .lineLimit(2)
                            .multilineTextAlignment(.center)
                            .frame(width: 260, alignment: .center)
                            .padding(.top, 10)
                        
                        if viewModel.isLoading{
                            ProgressView()
                                .progressViewStyle(CircularProgressViewStyle(tint: .white)).padding(.top, 30).scaleEffect(x: 2.0, y: 2.0, anchor: .center)
                        }
                    }
                    
                    Spacer()
                    
                    HStack {
                        Image("canada_flag").resizable().frame(width: 40, height: 20).padding(.leading, 5)
                        Text("+1 | ")
                        TextField("999-999-9999", text: $profile.user.phoneNumber).foregroundColor(.white)
                    }.customTextField().padding()
                    
                    rememberMe(rememberMe: $viewModel.rememberMe)
                    
                    HStack{
                        Button(action: {
                            viewModel.authenticatePhoneNumber(phoneNumber: profile.user.phoneNumber)
                        }) {
                            ApplicationButton(title: "Log-In", backgroundColor: Color("LightBlue"), textColor: .white)
                            
                        }
                        .sheet(isPresented: $viewModel.goingToVerificationView, content: {
                            PhoneNumberVerificationView(verificationNumber: "123123", verified: $viewModel.isVerified, firstTime: $viewModel.firstTime, isVerificationPageOpen: $viewModel.goingToVerificationView).environmentObject(profile)
                        })
                    }
                    
                    Spacer()
                }
                
            }
            
    }
}

#if DEBUG
struct PhoneNumberEntryView_Previews : PreviewProvider {
    static var previews: some View {
        PhoneNumberEntryView().environmentObject(ProfileData())
    }
}
#endif


struct rememberMe: View {
    
    @Binding var rememberMe: Bool
    
    var body: some View{
        HStack(alignment: .center){
            Label("", systemImage: rememberMe ? "checkmark.square" : "square")
                .frame(width: 50, height: 50, alignment: .center)
                .font(.system(size: 26))
                .foregroundColor(Color.white)
                .onTapGesture {
                    rememberMe.toggle()
                }
                .padding(.trailing, -15)
            
            Typography(text: "Remember Me", size: 18, color: Color.white)
            
            Spacer()
        }
        .padding(.leading, 30)
        .padding(.bottom, 10)
    }
}
