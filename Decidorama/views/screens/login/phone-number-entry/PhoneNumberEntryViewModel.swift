

import SwiftUI
import Firebase

final class PhoneNumberEntryViewModel: ObservableObject{
    
    @Published var goingToVerificationView: Bool = false
    @Published var isVerified = false // this means that its not the users first time logging in
    @Published var firstTime: Bool = false
    
    @Published var isLoading : Bool = false
    
    @Published var rememberMe : Bool = false
    
    
    
    func authenticatePhoneNumber(phoneNumber: String){
        
        //removes previous phonenumber if it was saved in user defauts
        UserDefaults.standard.set("", forKey: "phoneNumber")
        
        isLoading = true
        Auth.auth().settings?.isAppVerificationDisabledForTesting = false;
        
        PhoneAuthProvider.provider().verifyPhoneNumber("+1" + phoneNumber, uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                print("error: ", error)
                return
            }
            // Sign in using the verificationID and the code sent to the user
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
            
            //remember me
            if self.rememberMe{
                UserDefaults.standard.set(phoneNumber, forKey: "phoneNumber")
            }
            
            self.goingToVerificationView = true
            self.isLoading = false
        }
    }
}
