import SwiftUI

struct ApplicationTabView: View {
    @EnvironmentObject var profile: ProfileData
    
    var body: some View {
        TabView {
            ActivityListView()
                .navigationBarTitle("")
                .navigationBarHidden(true)
                .tabItem {
                    Image(systemName: "house")
                    Text("Home")
                }
            
            GroupSwipingView()
                .navigationBarTitle("")
                .navigationBarHidden(true)
                .tabItem {
                    Image(systemName: "rectangle.stack")
                    Text("Browse")
                }
            
            ActivityRequestsView()
                .navigationBarTitle("")
                .navigationBarHidden(true)
                .tabItem {
                    Image(systemName: "person.3.fill")
                    Text("Requests")
                }
            
            ProfileView()
                .navigationBarTitle("")
                .navigationBarHidden(true)
                .tabItem {
                    Image(systemName: "person")
                    Text("Profile")
                }
                
                
        }
        
    }
}

struct ApplicationTabView_Previews: PreviewProvider {
    static var previews: some View {
        ApplicationTabView().environmentObject(ProfileData())
    }
}
