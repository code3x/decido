import SwiftUI

struct ApplicationButton: View {
    
    var title: String
    var backgroundColor: Color
    var textColor: Color
    var width: CGFloat = 340
    var disabled: Bool = false
    
    var body: some View {
        return  Text(title)
            .frame(width: width, height: 50)
            .background(backgroundColor)
            .opacity(disabled ? 0.5 : 1)
            .font(.system(size: 20, weight: .bold,design: .default))
            .cornerRadius(10)
            .foregroundColor(textColor)
    }
}

struct ApplicationButton_Previews: PreviewProvider {
    static var previews: some View {
        
        Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
            ApplicationButton(title: "Test", backgroundColor: .green, textColor: .white)
        })
        
    }
}
