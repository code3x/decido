//
//  Typography.swift
//  Decidorama
//
//  Created by George on 2021-08-26.
//

import SwiftUI

struct Typography: View {
    
    var text: String
    var size: CGFloat = 24
    var weight: Font.Weight = .regular
    var color: Color = .black
    
    var body: some View {
        
        return Text(text)
            .font(.system(size: size, weight: weight, design: .default))
            .foregroundColor(color)
        
    }
}

struct Typography_Previews: PreviewProvider {
    static var previews: some View {
        
        Typography(text: "Hello Test")
    }
}
