import SwiftUI

extension View {
    func customTextField(lineColor: Color = .white) -> some View {
        self
            .padding(.vertical, 10)
            .overlay(Rectangle().frame(height: 2).padding(.top, 35))
            .foregroundColor(lineColor)
            .padding(10)
    }
}

struct customTextField_Previews : PreviewProvider {
    static var previews: some View {
        ZStack {
            BackgroundColor(topColor: .blue, bottomColor: .purple)
            
            HStack {
                TextField("999-999-9999", text: .constant("some text")).foregroundColor(.white)
            }.customTextField(lineColor: .black).padding()
        }
        
    }
}

struct BottomSheet<SheetContent: View>: ViewModifier {
    
    @Binding var isPresented: Bool
    
    let sheetContent: () -> SheetContent
    func body(content: Content) -> some View {
        ZStack {
            content
            if isPresented {
                VStack {
                    Spacer()
                    VStack {
                        HStack {
                            Spacer()
                            Button(action: {
                                withAnimation(.easeInOut) {
                                    self.isPresented = false
                                }
                            }) {
                                ApplicationButton(title: "X", backgroundColor: .red, textColor: .white, width: 45).padding(.trailing, 10).offset(y: 20)
                            }
                        }
                        sheetContent()
                    }
                    .padding()
                }
                .zIndex(.infinity)
                .transition(.move(edge: .bottom))
                .edgesIgnoringSafeArea(.bottom)
            }
        }
    }
}

extension View {
    func customBottomSheet<SheetContent: View>(
        isPresented: Binding<Bool>,
        sheetContent: @escaping () -> SheetContent
    ) -> some View {
        self.modifier(BottomSheet(isPresented: isPresented, sheetContent: sheetContent))
    }
}





