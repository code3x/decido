//
//  BackgroundColor.swift
//  Decidorama
//
//  Created by George on 2021-08-25.
//

import SwiftUI

struct BackgroundColor: View {
    
    var topColor: Color
    var bottomColor: Color
    
    var body: some View {
        LinearGradient(gradient: Gradient(colors: [topColor, bottomColor]),
                       startPoint: .top,
                       endPoint: .bottom
        )
        .edgesIgnoringSafeArea(.all)
    }
}

struct BackgroundColor_Previews: PreviewProvider {
    static var previews: some View {
        BackgroundColor(topColor: Color("Purple_1"), bottomColor: Color("Purple_2"))
    }
}
