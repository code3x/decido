import SwiftUI

struct ApplicationSnackbar: View {
    var text: String
    var color: Color = .purple
    var body: some View {
        HStack{
            
            Image(systemName: "bell")
                .resizable()
                .frame(width: 24.0, height: 25.0)
                .padding(.trailing, 15)
            
            Typography(text: text, size: 20, weight: .bold, color: .white)
            
        }.frame(width: 375, height: 75)
        .background(color)
        .foregroundColor(.white)
        .cornerRadius(15.0)
        .padding(.bottom, 30)
        
    }
}

struct ApplicationSnackbar_Previews: PreviewProvider {
    static var previews: some View {
        ApplicationSnackbar(text: "Alert", color: .red)
    }
}
