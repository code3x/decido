
import SwiftUI

extension View {
    func TTextFieldBlackUnderline() -> some View {
        self
            .padding(.vertical, 10)
            .overlay(Rectangle().frame(height: 2).padding(.top, 35))
            .foregroundColor(.black)
            .padding(10)
    }
}

struct TextFieldBlackUnderline_Previews : PreviewProvider {
    static var previews: some View {
        ZStack {
            BackgroundColor(topColor: .blue, bottomColor: .purple)
            
            HStack {
                TextField("999-999-9999", text: .constant("some text")).foregroundColor(.white)
            }.customTextField().padding()
        }
       
    }
}
