//
//  ProfileData.swift
//  Decidorama
//
//  Created by Zakim Javer on 2021-04-14.
//

import Foundation

class ProfileData: ObservableObject  {
    @Published var user: User
    @Published var allPreferences: [UserPreference]
    @Published var preferenceHistory: [String: Int]
    
    init(user: User = UserMockData.emptyUser, allPreferences: [UserPreference] = [UserPreference(preferenceId: "", preferenceName: "", preferenceColor: .black, selected: false)], history: [String: Int] = [:]) {
        var usr = user
        usr.bio = "Hi there! let have fun"
        usr.userStatus = "Available"
        
        self.user = usr
        self.allPreferences = allPreferences
        self.preferenceHistory = history
    }
}
