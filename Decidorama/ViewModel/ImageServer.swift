//
//  ImageServer.swift
//  Decidorama
//
//  Created by Zakim Javer on 2021-04-14.
//

import Foundation
import FirebaseStorage
import SwiftUI

struct ImageServer {
    
    //save users profile picture and user.picture
    func uploadImg(imag: UIImage, phoneNumber: String, complete: @escaping (String) -> ()){
        if let imgData = imag.jpegData(compressionQuality: 0.5){
            
            let metadata = StorageMetadata()
            metadata.contentType = "image/jpeg"
            Storage.storage().reference().child("UserProfileImages").child(phoneNumber).putData(imgData, metadata: metadata){
                (metadata, error) in
                if error != nil{
                    print("did not upload image")
                }else {
                    print("uploaded")
                    var downloadURL : String?
                    Storage.storage().reference().child("UserProfileImages").child(phoneNumber).downloadURL(completion: {(url, error) in
                        if error == nil{
                            print("no errors found")
                            downloadURL = url!.absoluteString
                            //print(downloadURL!)
                        }
                        
                        if let url = downloadURL{
                            
                            complete(url)
                        }
                    })
                }
            }
        }
    }
    
    //return an image
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit, complete: @escaping (UIImage) -> ()) {
        //print("qwqw: 1" )
        
        //contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            //print("qwqw: 2" )
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image: UIImage = UIImage(data: data)
            else { //print("qwqw: 3" )
                return }
            //print("qwqw: 4" )
            complete(image)
        }.resume()
        //print("qwqw: 5" )
    }
    
}
