//
//  ActivityData.swift
//  Decidorama
//
//  Created by Ali Muhammad on 2021-11-10.
//

import Foundation

class ActivityData: ObservableObject  {
    @Published var activity: Activity
    
    init(activity: Activity = ActivityMockData.emptyActivity) {
        var activity = activity
        activity.name = "Bowling with the guys"
        activity.description = ""
        activity.campus = "Sheridan"
        activity.location = "Brampton"
        activity.memberLimit = 10
        
        self.activity = activity

    }
}
