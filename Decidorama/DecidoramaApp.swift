import SwiftUI
import Firebase

@main
struct DecidoramaApp: App {
    
    @UIApplicationDelegateAdaptor(AppDelegate.self)
    var delegate
    
    var body: some Scene {
        WindowGroup {
            NavigationController()
            //ProfileSetupView().environmentObject(ProfileData(user: UserMockData.sampleUser))
        }
    }
    
    
    //App delegate
    class AppDelegate: NSObject, UIApplicationDelegate{
        func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
            FirebaseApp.configure()
            return true
        }
        
        func application(_ application: UIApplication,
                         didReceiveRemoteNotification notification: [AnyHashable : Any],
                         fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
            if Auth.auth().canHandleNotification(notification) {
                completionHandler(.noData)
                return
            }
        }
    }
}
