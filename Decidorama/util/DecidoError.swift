import Foundation

enum DecidoError: Error {
    case imageFailedToUpload
    case dataNotRetrievedFromServer
}
