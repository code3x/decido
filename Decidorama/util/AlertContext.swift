import SwiftUI

struct AlertItem: Identifiable {
    let id = UUID()
    let title: Text
    let message: Text
    let dismissButton: Alert.Button
}

struct AlertContext {
    static let invalidDataExample = AlertItem(title: Text("Server Error"),
                                              message: Text("The data was invalid, please try again later or contact support."),
                                              dismissButton: .default(Text("Okay")))
    
    static let imageFailedToUpload = AlertItem(title: Text("Server Error"),
                                             message: Text("The image was not uploaded to the server, please try again later or contact support."),
                                             dismissButton: .default(Text("Okay")))
}
