import UIKit
import FirebaseStorage

final class NetworkManager {
    static let shared = NetworkManager()
    private init() {}
    
    //saves image to cache to prevent re-downloading images
    private let cache = NSCache<NSString, UIImage>()
    
    func downloadImage(fromURLString urlString: String, complete: @escaping (UIImage?) -> Void) {
        let cacheKey = NSString(string: urlString)
        
        //image saved in cache
        if let image = cache.object(forKey: cacheKey){
            print("DownloadImage called from networkmanager Local: ", urlString)
            complete(image)
            return
        }
        
        //make sure url is valid
        guard let url = URL(string: urlString) else {
            complete(nil)
            return
        }
        
        //download image and save it to cache
        let task = URLSession.shared.dataTask(with: URLRequest(url: url)) { data, response, error in
            //make sure we got an image
            guard let data = data, let image = UIImage(data: data) else {
                complete(nil)
                return
            }
            
            //save image to cache
            self.cache.setObject(image, forKey: cacheKey)
            print("DownloadImage called from networkmanager: ", urlString)
            complete(image)
        }
        
        task.resume()
    }
    
    //save users profile picture and user.picture
    func uploadImg(imag: UIImage, phoneNumber: String, complete: @escaping (Result<String, DecidoError>) -> ()){
        if let imgData = imag.jpegData(compressionQuality: 0.5){
            
            let metadata = StorageMetadata()
            metadata.contentType = "image/jpeg"
            Storage.storage().reference().child("UserProfileImages").child(phoneNumber).putData(imgData, metadata: metadata){
                (metadata, error) in
                if error != nil{
                    print("did not upload image")
                    complete(.failure(.imageFailedToUpload))
                }else {
                    var downloadURL : String?
                    Storage.storage().reference().child("UserProfileImages").child(phoneNumber).downloadURL(completion: {(url, error) in
                        if error == nil{
                            downloadURL = url!.absoluteString
                        }
                        
                        if let url = downloadURL{
                            
                            complete(.success(url))
                        }
                    })
                }
            }
        }
    }
    
    //save activity images
    func uploadImg(imag: UIImage, activityID: String, complete: @escaping (Result<String, DecidoError>) -> ()){
        if let imgData = imag.jpegData(compressionQuality: 0.5){
            
            let metadata = StorageMetadata()
            metadata.contentType = "image/jpeg"
            Storage.storage().reference().child("ActivityImages").child(activityID).putData(imgData, metadata: metadata){
                (metadata, error) in
                if error != nil{
                    print("did not upload image")
                    complete(.failure(.imageFailedToUpload))
                }else {
                    var downloadURL : String?
                    Storage.storage().reference().child("ActivityImages").child(activityID).downloadURL(completion: {(url, error) in
                        if error == nil{
                            downloadURL = url!.absoluteString
                        }
                        
                        if let url = downloadURL{
                            
                            complete(.success(url))
                        }
                    })
                }
            }
        }
    }
}

