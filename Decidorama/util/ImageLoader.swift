import SwiftUI

final class ImageLoader: ObservableObject{
    
    @Published var image: Image? = nil
    
    func load(fromURLString urlString: String){
        NetworkManager.shared.downloadImage(fromURLString: urlString) { uiImage in
            guard let uiImage = uiImage else { return }
            DispatchQueue.main.async {
                self.image = Image(uiImage: uiImage)
            }
        }
    }
    
    func load(phoneNumber: String){
        UserFunctions().getUserByPhoneNumber(phoneNumber: phoneNumber) { [self] res in
            switch res{
            case .success(let user):
                load(fromURLString: user.avatar)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}


//used to return the image or a placeHolder Image
struct ImageHelper: View {
    
    var image: Image?
    
    var body: some View{
        image?.resizable() ?? Image("ImagePlaceholder").resizable()
    }
}

struct ApplicationImage: View {
    
    @StateObject var imageLoader = ImageLoader()
    @Binding var urlString: String
    
    var body: some View{
        ImageHelper(image: imageLoader.image)
            .onAppear{
                imageLoader.load(fromURLString: urlString)
            }
            .onChange(of: urlString, perform: { value in
                imageLoader.load(fromURLString: urlString)
            })
    }
    
}

struct ApplicationImageViaPhoneNumber: View {
    
    @StateObject var imageLoader = ImageLoader()
    var phoneNumber: String
    
    var body: some View{
        ImageHelper(image: imageLoader.image)
            .onAppear{
                imageLoader.load(phoneNumber: phoneNumber)
            }
            .onChange(of: phoneNumber, perform: { value in
                imageLoader.load(phoneNumber: phoneNumber)
            })
    }
    
}

struct ApplicationImageStatic: View {
    
    @StateObject var imageLoader = ImageLoader()
    var urlString: String
    
    var body: some View{
        ImageHelper(image: imageLoader.image)
            .onAppear{
                imageLoader.load(fromURLString: urlString)
            }
            .onChange(of: urlString, perform: { value in
                imageLoader.load(fromURLString: urlString)
            })
    }
    
}
