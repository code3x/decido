
import Foundation

struct Activity: Identifiable{
    var id = UUID()
    var activityId: String?
    
    var owner: String
    var image: String
    var campus: String
    var name: String
    var description: String
    var done: Bool?
    var open: Bool?
    var rating: String
    var location: String
    var memberLimit: Int
    var messages: [Message]?
    
    var date : String
    var price : String
    var preference: String
    var deleted: Bool
}

struct ActivityMockData {
    static let sampleActivity = Activity(owner: "00000000", image: "p1", campus: "LOL", name: "Hiking", description: "Event will take place at Albion Falls", done: false, open: true, rating: "3.3", location: "Oakville", memberLimit: 12, date: "", price: "", preference: "Sports", deleted: false)
    
    static let emptyActivity = Activity(owner: "", image: "", campus: "", name: "", description: "", done: false, open: true, rating: "", location: "", memberLimit: 0, date: "", price: "", preference: "Sports", deleted: false)
    
    static let mockActivities = [Activity(owner: "00000000", image: "p0", campus: "LOL", name: "Bowling", description: "One of the best events organized!", done: false, open: true, rating: "3.3", location: "Oakville", memberLimit: 12 , date: "", price: "", preference: "Sports", deleted: false),
                                 Activity(owner: "00000000", image: "p1", campus: "LOL", name: "Hiking", description: "Event will take place at Albion Falls", done: false, open: true, rating: "3.3", location: "Oakville", memberLimit: 12, date: "", price: "", preference: "Sports", deleted: false)]
    
    static let sampleActivityZJ = Activity(owner: "00000000", image: "https://firebasestorage.googleapis.com/v0/b/decidorama-3d80f.appspot.com/o/UserProfileImages%2F1234567899?alt=media&token=bcdf7769-3f0a-4267-91bb-800262ad864b", campus: "LOL", name: "Hiking", description: "Event will take place at Albion Falls", done: false, open: true, rating: "3.3", location: "Oakville", memberLimit: 12, date: "", price: "", preference: "Sports", deleted: false)
    
    static let mockActivitiesZJ = [
        Activity(owner: "00000000", image: "https://firebasestorage.googleapis.com/v0/b/decidorama-3d80f.appspot.com/o/UserProfileImages%2F1234567899?alt=media&token=bcdf7769-3f0a-4267-91bb-800262ad864b", campus: "LOL", name: "Bowling", description: "One of the best events organized!", done: false, open: true, rating: "3.3", location: "Oakville", memberLimit: 12, date: "", price: "", preference: "Sports", deleted: false),
                                 
        Activity(owner: "00000000", image: "https://firebasestorage.googleapis.com/v0/b/decidorama-3d80f.appspot.com/o/UserProfileImages%2F2898851644?alt=media&token=494a99e8-ad6d-4943-9aef-e24d4d0df0fb", campus: "LOL", name: "Hiking", description: "Event will take place at Albion Falls", done: false, open: true, rating: "3.3", location: "Oakville", memberLimit: 12, date: "", price: "", preference: "Sports", deleted: false)]
}
