import Foundation

struct User{
    var phoneNumber: String
    var avatar: String
    var bio: String
    var age: Int
    var gender: String
    var program: String
    var name: String
    var userStatus: String
    var dob: Date = Date(timeIntervalSinceNow: 0)
    
    var selectedPreferences: [UserPreference] = [UserPreference]()
}

/*user need:
 campus (dropdown, data in server)
 
 
 
 *limited to schools in Ontario
 create an array of all uni/collages with the location
 
 
*/

struct UserMockData {
    static let sampleUser = User(phoneNumber: "6477866980",
                                 avatar: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSTHGbHHmzhISDVSrrWuYwCwW4ebmqLXw7KWQ&usqp=CAU",
                                 bio: "I love hiking, playing sports and studying.",
                                 age:22,
                                 gender: "male",
                                 program: "Computers & Telecommunications",
                                 name: "George Kopti",
                                 userStatus: "Available")
    
    static let sampleUserArray = [User(phoneNumber: "1234567890",
                                       avatar: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSTHGbHHmzhISDVSrrWuYwCwW4ebmqLXw7KWQ&usqp=CAU",
                                       bio: "I love hiking, playing sports and studying.",
                                       age:22,
                                       gender: "male",
                                       program: "Computers & Telecommunications",
                                       name: "Alii",
                                       userStatus: "Available"),
    
                                  User(phoneNumber: "2898851644",
                                                               avatar: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSTHGbHHmzhISDVSrrWuYwCwW4ebmqLXw7KWQ&usqp=CAU",
                                                               bio: "I love hiking, playing sports and studying.",
                                                               age:22,
                                                               gender: "male",
                                                               program: "Computers & Telecommunications",
                                                               name: "George Kopti",
                                                               userStatus: "Available"),
                                  
                                  User(phoneNumber: "9055175557",
                                                               avatar: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSTHGbHHmzhISDVSrrWuYwCwW4ebmqLXw7KWQ&usqp=CAU",
                                                               bio: "I love hiking, playing sports and studying.",
                                                               age:22,
                                                               gender: "male",
                                                               program: "Computers & Telecommunications",
                                                               name: "Zakkk",
                                                               userStatus: "Available")]
    
    static let emptyUser = User(phoneNumber: "",
                                 avatar: "",
                                 bio: "",
                                 age:-1,
                                 gender: "",
                                 program: "",
                                 name: "",
                                 userStatus: "")
}
