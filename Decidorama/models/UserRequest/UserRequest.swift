import Foundation
import SwiftUI

struct UserRequest: Identifiable {
    var id:UUID = UUID()
    var name:String
    var bio:String
    var avatar:String

}

struct MockUserRequestData {
    
    static let sampleUserRequest = UserRequest(name: "Kristina D.", bio: "I love sports", avatar: "p0")
    
    static let sampleUserRequests =  [
        UserRequest(name: "Kristina D.", bio: "I love sports", avatar: "p0"),
        UserRequest(name: "David G.", bio: "Spectacular Dude", avatar: "p1"),
        UserRequest(name: "Robert B.", bio: "Professor @ McMaster", avatar: "p2"),
        UserRequest(name: "Ruxi A.", bio: "Professional MMA fighter", avatar: "p2"),
        UserRequest(name: "George B.", bio: "Ali's good friend ", avatar: "p0"),
        ]
}
