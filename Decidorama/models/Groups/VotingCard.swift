//
//  VotingCard.swift
//  Decidorama
//
//  Created by Zakim Javer on 2021-11-18.
//
import SwiftUI

struct VotingCard: Identifiable{
  
    var id                  : String
    var cardType            : CardType
    var title               : String
    var description         : String
    var duration            : Date                  //voting period
    var options             : [VotingCardOptions]   //The different choices available for the card
    var usersVoted          = [String]()
    var results             = [String]()
    
    
    enum CodingKeys: String, CodingKey{
        case id
        case cardType
        case title
        case description
        case duration
        case options
    }
    
    enum CardType: String{
        case multiSelect, multipleChoice
    }
}

struct VotingCardOptions: Identifiable{
    
    var id          : UUID
    var text        : String
    var selected    : Bool = false
    
}

struct VotingCardMockData {
    
    static var sampleVotingCardArray = [sampleVotingCard, sampleVotingCard1, sampleVotingCard2]
    
    
    
    static let sampleVotingCard = VotingCard(id: "01",
                                             cardType: .multiSelect,
                                             title: "What would you like to eat?",
                                             description: "These are the options available around that area, Swipe right on the places you would like to go.",
                                             duration: Date(timeIntervalSinceNow: 20),
                                             options: createOptions(options: ["Pizza-Pizza", "Domino's", "BurgerKing", "Fish-and-Chips", "Bubble tea"]))
    
    static let sampleVotingCard1 = VotingCard(id: "02",
                                              cardType: .multipleChoice,
                                              title: "Are you going?",
                                              description: "Please let us know if you're planning to come to this event.",
                                              duration: Date(timeIntervalSinceNow: 20),
                                              options: createOptions(options: ["Yes!", "No", "Not sure", "Opt1", "Op2"]))
    
    static let sampleVotingCard2 = VotingCard(id: "03",
                                              cardType: .multiSelect,
                                              title: "Are you going (Test 2)?",
                                              description: "Please let us know if you're planning to come to this event. (Test 2). These are the options available around that area, Swipe right on the places you would like to go. (Test 1)",
                                              duration: Date(timeIntervalSinceNow: 20),
                                              options: createOptions(options: ["Swimming", "Fishing", "MacDonals"]))
    
}

func createOptions(options: [String]) -> [VotingCardOptions]{
    var returnCards = [VotingCardOptions]()
    
    for op in options{
        returnCards.append(VotingCardOptions(id: UUID(), text: op))
    }
    return returnCards
}
