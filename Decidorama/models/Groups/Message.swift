//
//  MessageModel.swift
//  Decidorama
//
//  Created by Zakim Javer on 2021-10-30.
//

import SwiftUI

struct Message: Codable, Identifiable, Hashable{
  
    var id                  : String
    var messageText         : String
    var sendersPhoneNumber  : String
    var timeStamp           : Date
    
    
    enum CodingKeys: String, CodingKey{
        case id
        case messageText
        case sendersPhoneNumber
        case timeStamp
    }
}

struct MessageMockData {
    
    static var sampleMessages = [sampleMessage1, sampleMessage2, sampleMessage3, sampleMessage4, sampleMessage5, sampleMessage6, sampleMessage7, sampleMessage8]
    
    
    
    static let sampleMessage1 = Message(id: "1", messageText: "Hello", sendersPhoneNumber: "905", timeStamp: Date(timeIntervalSinceNow: 5))
    
    private static let sampleMessage2 = Message(id: "2", messageText: "Hi", sendersPhoneNumber: "6477866980", timeStamp: Date(timeIntervalSinceNow: 30))
    
    private static let sampleMessage3 = Message(id: "3", messageText: "How are you doing?", sendersPhoneNumber: "905", timeStamp: Date(timeIntervalSinceNow: 60))
    
    private static let sampleMessage4 = Message(id: "4", messageText: "Not too bad, and yourself?", sendersPhoneNumber: "6477866980", timeStamp: Date(timeIntervalSinceNow: 80))
    
    private static let sampleMessage5 = Message(id: "5", messageText: ":`(", sendersPhoneNumber: "6477866980", timeStamp: Date(timeIntervalSinceNow: 100))
    
    private static let sampleMessage6 = Message(id: "6", messageText: "The UIKit framework provides the required infrastructure for your iOS or tvOS apps. It provides the window and view architecture for implementing your interface, the event handling infrastructure for delivering Multi-Touch and other types of input to your app, and the main run loop needed to manage interactions among the user, the system, and your app.", sendersPhoneNumber: "905", timeStamp: Date(timeIntervalSinceNow: 200))
    
    private static let sampleMessage7 = Message(id: "7", messageText: "Display activity-based services to the user.", sendersPhoneNumber: "905", timeStamp: Date(timeIntervalSinceNow: 250))
    
    private static let sampleMessage8 = Message(id: "8", messageText: "You did it. Awesome bro. BUT BUT BUT, now you left out the chat list !!!  lol  Why did you make the non-Firebase version with the full UI and the Firebase version without the full UI?  Boggles the mind. Anyway, awesome job man. You're really good", sendersPhoneNumber: "6477866980", timeStamp: Date(timeIntervalSinceNow: 280))
}
