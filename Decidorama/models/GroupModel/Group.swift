//
//  GroupFirebase.swift
//  Decidorama
//
//  Created by Ali Muhammad on 2021-04-07.
//

import Foundation

class Group  {
    public var groupID: String //id
    public var groupName: String
    public var groupCreateDate: String
    public var groupDescription: String
    public var groupPictureURL: String
    public var groupType: String
    public var activities: [String] //need to be change to [Group]
    public var preferences: [String]//need to be change to [Preferences]
    
    
    init() {
        groupName = ""
        groupID = ""
        groupCreateDate = ""
        groupDescription = ""
        groupPictureURL = ""
        groupType = ""
        activities = [""]
        preferences = [""]
    }
    
    init(groupID: String, groupName: String, groupCreateDate: String, groupDescription: String, groupPictureURL: String, groupType: String, activities: [String], preferences: [String]) {
       
        self.groupName = groupName
        self.groupID = groupID
        self.groupCreateDate = groupCreateDate
        self.groupDescription = groupDescription
        self.groupPictureURL = groupPictureURL
        self.groupType = groupType
        self.activities = activities
        self.preferences = preferences
    }
}

