import Foundation
import SwiftUI

struct UserPreference: Identifiable, Hashable{
    
    var id = UUID()
    var preferenceId: String?
    var preferenceName: String
    var preferenceColor: Color
    var selected: Bool = false
    
}

struct PreferenceMockData {
    static let samplePreference = UserPreference(preferenceId: "A", preferenceName: "Sports", preferenceColor: .blue)
    
    static let samplePreferences = [UserPreference(preferenceId: "A", preferenceName: "Hiking", preferenceColor: .blue),
                                    UserPreference(preferenceId: "B", preferenceName: "Food", preferenceColor: .yellow),
                                    UserPreference(preferenceId: "C", preferenceName: "Tea", preferenceColor: .red),
                                    UserPreference(preferenceId: "D", preferenceName: "Cat lover", preferenceColor: .purple),
                                    UserPreference(preferenceId: "E", preferenceName: "Brunch", preferenceColor: .gray),
                                    UserPreference(preferenceId: "F", preferenceName: "Politics", preferenceColor: .green),
                                    UserPreference(preferenceId: "AA", preferenceName: "Shopping", preferenceColor: .blue),
                                    UserPreference(preferenceId: "AB", preferenceName: "Wine", preferenceColor: .green),
                                    UserPreference(preferenceId: "AC", preferenceName: "Walking", preferenceColor: .black),
                                    UserPreference(preferenceId: "AD", preferenceName: "Party", preferenceColor: .purple),
                                    UserPreference(preferenceId: "AE", preferenceName: "Study", preferenceColor: .green),
                                    UserPreference(preferenceId: "AF", preferenceName: "Food", preferenceColor: .yellow),
                                    UserPreference(preferenceId: "AG", preferenceName: "Travel", preferenceColor: .pink),
                                    UserPreference(preferenceId: "AH", preferenceName: "Reading", preferenceColor: .yellow),
                                    UserPreference(preferenceId: "AI", preferenceName: "Coffee", preferenceColor: .purple),
                                    UserPreference(preferenceId: "AJ", preferenceName: "DIY", preferenceColor: .gray),
                                    UserPreference(preferenceId: "AK", preferenceName: "Comedy", preferenceColor: .green),
                                    UserPreference(preferenceId: "AL", preferenceName: "Golf", preferenceColor: .black),
                                    UserPreference(preferenceId: "AM", preferenceName: "Gardening", preferenceColor: .blue),
                                    UserPreference(preferenceId: "AN", preferenceName: "Hiking", preferenceColor: .blue),
                                    UserPreference(preferenceId: "AO", preferenceName: "Writer", preferenceColor: .yellow),
                                    UserPreference(preferenceId: "AP", preferenceName: "Swimming", preferenceColor: .purple),
                                    UserPreference(preferenceId: "AQ", preferenceName: "Art", preferenceColor: .green),
                                    UserPreference(preferenceId: "AR", preferenceName: "Photography", preferenceColor: .blue),
                                    UserPreference(preferenceId: "AS", preferenceName: "Athlete", preferenceColor: .secondary),
                                    UserPreference(preferenceId: "AT", preferenceName: "Blogging", preferenceColor: .orange),
                                    UserPreference(preferenceId: "AU", preferenceName: "Tech", preferenceColor: .red),]
}
