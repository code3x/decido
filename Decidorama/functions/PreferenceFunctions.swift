import Foundation
import Firebase
import SwiftUI

class PreferenceFunctions {
    
    lazy var functions = Functions.functions()
    
    //How to call set preferences
//    PreferenceFunctions()
//        .setUserPreference(userPhoneNumber: "1234567899",
//                           preferences: [UserPreference(preferenceId: "0011",preferenceName: "lol", preferenceColor: .blue),
//                                         UserPreference(preferenceId: "0022", preferenceName: "hi", preferenceColor: .yellow)])
//
    func setUserPreference(userPhoneNumber: String, preferences: [UserPreference]) -> Void {
        
        functions.httpsCallable("setUserPreference").call(["userPhoneNumber": userPhoneNumber,
                                                           "preferenceIds": preferences.map { $0.preferenceId },
                                                           "preferenceNames":  preferences.map { $0.preferenceName }]){ result,error in
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
        }
    }
    
    func getAllPreferences(completed: @escaping (Result<[UserPreference], DecidoError>) -> Void){
        
        functions.httpsCallable("getAllPreferences").call(){ result, error in
            
            var activities: [UserPreference] = []
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
            
            guard let data = result?.data as? [String: [String:Any]] else {
                completed(.failure(.dataNotRetrievedFromServer))
                return
            }
            
            for preference in data {
                activities.append(UserPreference(preferenceId: preference.key, preferenceName: preference.value["name"] as? String ?? "", preferenceColor: self.stringToColor(colorString: preference.value["color"] as? String ?? "")))
            }
            
            completed(.success(activities))
        }
    }
    
    func getUserPreferences(userPhoneNumber: String, completed: @escaping (Result<[String], DecidoError>) -> Void){
        
        functions.httpsCallable("getUserPreferences").call(["userPhoneNumber": userPhoneNumber]){ result, error in
            
            var preferenceIds: [String] = []
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
            
            guard let data = result?.data as? [String: String] else {
                completed(.failure(.dataNotRetrievedFromServer))
                return
            }
            
            for preferenceId in data.keys {
                preferenceIds.append(preferenceId)
            }
            
            completed(.success(preferenceIds))
        }
    }
    
    func stringToColor(colorString: String) -> Color{
        switch colorString {
        case "red":
            return .red
        case "blue":
            return .blue
        case "green":
            return .green
        case "black":
            return .black
        case "white":
            return .white
        case "purple":
            return .purple
        default:
            return .purple
        }
    }
    
}
