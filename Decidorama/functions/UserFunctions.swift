import Foundation
import Firebase


class UserFunctions {
    
    lazy var functions = Functions.functions()
    let dateFormatter = DateFormatter()
    
    /*
     This is how to call the functions
     */
    //UserFunctions().getUserByPhoneNumber(phoneNumber: "2898851644") { result in
    //    switch result {
    //    case .success(let user):
    //        print(user, "-------")
    //    case .failure(_):
    //        print("Error")
    //    }
    //}
    
    func getUserByPhoneNumber(phoneNumber: String, completed: @escaping (Result<User, DecidoError>) -> Void){
        
        functions.httpsCallable("getUserByNumber").call(["phoneNumber": phoneNumber]){ result, error in
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
            
            guard let data = result?.data as? [String: Any] else {
                completed(.failure(.dataNotRetrievedFromServer))
                return
            }
            
            let avatar = data["avatar"] as? String ?? ""
            let name = data["name"] as? String ?? ""
            let bio = data["bio"] as? String ?? ""
            let userStatus = data["userStatus"] as? String ?? ""
            let age = data["age"] as? Int ?? 0
            let gender = data["gender"] as? String ?? ""
            let program = data["program"] as? String ?? ""
            
            completed(.success(User(phoneNumber: phoneNumber, avatar: avatar, bio: bio, age: age, gender: gender, program: program, name: name, userStatus: userStatus)))
        }
    }
    
    func createUser(user: User) -> Void {
        
        functions.httpsCallable("createUser").call(["phoneNumber": user.phoneNumber,
                                                    "avatar": user.avatar, "bio": user.bio,
                                                    "name": user.name, "status": user.userStatus,
                                                    "age": user.age, "program": user.program, "gender": user.gender]){_,error in
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
        }
    }
    
    func registerUserSwipe(userPhoneNumber: String, activityId: String, directionSwiped: String) -> Void {
        
        functions.httpsCallable("registerUserSwipe").call(["userPhoneNumber": userPhoneNumber,
                                                           "activityId": activityId,
                                                           "directionSwiped": directionSwiped]){ result,error in
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
        }
    }
    
    func registerUserSwipeV2(
        userPhoneNumber: String,
        activityId: String,
        directionSwiped: String,
        activityPreference: String,
        preferenceCount: Int
    ) -> Void {
        
        functions.httpsCallable("registerUserSwipeV2").call(["userPhoneNumber": userPhoneNumber,
                                                           "activityId": activityId,
                                                           "directionSwiped": directionSwiped,
                                                           "activityPreference": activityPreference,
                                                           "preferenceCount": preferenceCount ]){ result,error in
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
        }
    }
    
    func userAcceptRequest(userPhoneNumber: String, activityId: String) -> Void {
        
        functions.httpsCallable("userAcceptRequest").call(["userPhoneNumber": userPhoneNumber,
                                                         "activityId": activityId]){ result,error in
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
        }
    }
    
    func userDeclineRequest(userPhoneNumber: String, activityId: String) -> Void {
        
        functions.httpsCallable("userDeclineRequest").call(["userPhoneNumber": userPhoneNumber,
                                                         "activityId": activityId]){ result,error in
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
        }
    }
    
    
    func useUserToken(userPhoneNumber: String, activityId: String) -> Void {
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss"
        
        functions.httpsCallable("userDeclineRequest").call(["userPhoneNumber": userPhoneNumber,
                                                         "activityId": activityId,
                                                         "date": dateFormatter.string(from: Calendar.current.date(byAdding: .day, value: 1, to: Date())!)]){ result,error in
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
        }
    }
    
    func setTokenTimer(userPhoneNumber: String) -> Void {
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss"
        
        functions.httpsCallable("setTokenTimer").call(["userPhoneNumber": userPhoneNumber,
                                                         "date": dateFormatter.string(from: Calendar.current.date(byAdding: .day, value: 1, to: Date())!)]){ result,error in
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
        }
    }
    
    func getTokenTimer(userPhoneNumber: String, completed: @escaping (Result<Date, DecidoError>) -> Void){
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss"
        functions.httpsCallable("getTokenTimer").call(["userPhoneNumber": userPhoneNumber]){ result, error in
 
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
            
            guard let data = result?.data as? String else {
                completed(.failure(.dataNotRetrievedFromServer))
                return
            }
            
            completed(.success(self.dateFormatter.date(from: data) ?? Date()))
        }
    }
    
    
    func getUserRequests(userPhoneNumber: String, completed: @escaping (Result<[String], DecidoError>) -> Void){
        
        functions.httpsCallable("getUserRequests").call(["userPhoneNumber": userPhoneNumber]){ result, error in
            
            var activityIds: [String] = []
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
            
            guard let data = result?.data as? [String: String] else {
                completed(.failure(.dataNotRetrievedFromServer))
                return
            }
            
            for activityId in data.keys {
                activityIds.append(activityId)
            }
            
            completed(.success(activityIds))
        }
    }
    
    func getUserSwipeHistory(userPhoneNumber: String, completed: @escaping (Result<[String: Int], DecidoError>) -> Void){
        
        functions.httpsCallable("getUserSwipeHistory").call(["userPhoneNumber": userPhoneNumber]){ result, error in
 
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
            
            guard let data = result?.data as? [String: Int] else {
                completed(.failure(.dataNotRetrievedFromServer))
                return
            }
            completed(.success(data))
        }
    }
    
}
