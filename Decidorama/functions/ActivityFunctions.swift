import Foundation
import Firebase
import SwiftUI

/*
 needs to be implemented:
 *   getActivityMembersID\
 */

class ActivityFunctions {
    
    lazy var functions = Functions.functions()
    
    //    ActivityFunctions().getAllActivities(){ result in
    //        switch result {
    //        case .success(let activities):
    //            print(activities[0], "-------")
    //        case .failure(_):
    //            print("Error")
    //        }
    //    }
    
    func getAllActivities(filterOut: [String] = [], completed: @escaping (Result<[Activity], DecidoError>) -> Void){
        
        functions.httpsCallable("getAllActivities").call(){ result, error in
            
            var activities: [Activity] = []
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
            
            guard let data = result?.data as? [String: [String:Any]] else {
                completed(.failure(.dataNotRetrievedFromServer))
                return
            }
            
            
            
            for activity in data {
                if(!filterOut.contains(activity.key)){
                    activities.append(Activity(activityId: activity.key,
                                               owner: activity.value["owner"] as? String ?? "",
                                               image: activity.value["image"] as? String ?? "",
                                               campus: activity.value["campus"] as? String ?? "",
                                               name: activity.value["name"] as? String ?? "",
                                               description: activity.value["description"] as? String ?? "",
                                               done: activity.value["done"] as? Bool ?? nil,
                                               open: activity.value["open"] as? Bool ?? nil,
                                               rating: activity.value["ration"] as? String ?? "",
                                               location: activity.value["location"] as? String ?? "",
                                               memberLimit: activity.value["memberLimit"] as? Int ?? 0,
                                               date : activity.value["date"] as? String ?? "",
                                               price : activity.value["price"] as? String ?? "",
                                               preference: activity.value["tag"] as? String ?? "",
                                               deleted: activity.value["deleted"] as? Bool ?? false))
                }
            }
            completed(.success(activities))
        }
    }
    
    func getActivityById(activityId: String, completed: @escaping (Result<Activity, DecidoError>) -> Void){
        
        functions.httpsCallable("getActivityById").call(["activityId": activityId]){ result, error in
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
            
            guard let data = result?.data as? [String: Any] else {
                completed(.failure(.dataNotRetrievedFromServer))
                return
            }
            
            
            let activity = Activity(activityId: activityId, owner: data["owner"] as? String ?? "",
                                    image: data["image"] as? String ?? "",
                                    campus: data["campus"] as? String ?? "",
                                    name: data["name"] as? String ?? "",
                                    description: data["description"] as? String ?? "",
                                    done: data["done"] as? Bool ?? nil,
                                    open: data["open"] as? Bool ?? nil,
                                    rating: data["ration"] as? String ?? "",
                                    location: data["location"] as? String ?? "",
                                    memberLimit: data["memberLimit"] as? Int ?? 0,
                                    date : data["date"] as? String ?? "",
                                    price : data["price"] as? String ?? "",
                                    preference: data["tag"] as? String ?? "",
                                    deleted: data["deleted"] as? Bool ?? false)
            
            completed(.success(activity))
        }
    }
    
    func getActivityMembers(activityId: String, complete: @escaping (Result<[User], DecidoError>) -> Void){
        
        var members = [User]()
        getGroupMembersID(activityId: activityId) { result in
            switch result{
            case .failure(let error):
                print(error.localizedDescription)
                complete(.failure(error))
                return
            case .success(let membersPhoneNumber):
                print(membersPhoneNumber)
                for phoneNumber in membersPhoneNumber{
                    UserFunctions().getUserByPhoneNumber(phoneNumber: phoneNumber) { res in
                        switch res{
                        case .failure(let error):
                            print("Could not load user \(phoneNumber), error: ", error)
                        case .success(let member):
                            members.append(member)
                            complete(.success(members))
                        }
                    }
                }
            }
        }
        
        //let returnArray = UserMockData.sampleUserArray//[UserMockData.sampleUser, UserMockData.sampleUser, UserMockData.sampleUser]
        //complete(.success(returnArray))
    }
    
    func getUserGroups(userPhoneNumber: String, completed: @escaping (Result<[String], DecidoError>) -> Void){
        
        functions.httpsCallable("getUserGroups").call(["userPhoneNumber": userPhoneNumber]){ result, error in
            
            var groupIds: [String] = []
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
            
            guard let data = result?.data as? [String: String] else {
                completed(.failure(.dataNotRetrievedFromServer))
                return
            }
            
            for activityId in data.keys {
                groupIds.append(activityId)
            }
            
            completed(.success(groupIds))
            
        }
    }
    
    func getGroupMembersID(activityId: String, completed: @escaping (Result<[String], DecidoError>) -> Void){
        
        functions.httpsCallable("getGroupMembers").call(["activityId": activityId]){ result, error in
            
            var memberIds: [String] = []
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
            
            guard let data = result?.data as? [String: String] else {
                completed(.failure(.dataNotRetrievedFromServer))
                return
            }
            
            for memberId in data.keys {
                memberIds.append(memberId)
            }
            
            completed(.success(memberIds))
            
        }
    }
    
    func getGroupRequests(activityId: String, completed: @escaping (Result<[String], DecidoError>) -> Void){
        
        functions.httpsCallable("getGroupRequests").call(["activityId": activityId]){ result, error in
            
            var userNumber: [String] = []
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
            
            guard let data = result?.data as? [String: String] else {
                completed(.failure(.dataNotRetrievedFromServer))
                return
            }
            
            for number in data.keys {
                userNumber.append(number)
            }
            
            completed(.success(userNumber))
        }
    }
    
    func getUserSwipedActivities(userPhoneNumber: String, completed: @escaping (Result<[String], DecidoError>) -> Void){
        
        functions.httpsCallable("getUserSwipedActivities").call(["userPhoneNumber": userPhoneNumber]){ result, error in
            
            var activities: [String] = []
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
            
            guard let data = result?.data as? [String: String] else {
                completed(.failure(.dataNotRetrievedFromServer))
                return
            }
            
            for number in data.keys {
                activities.append(number)
            }
            
            completed(.success(activities))
        }
    }
    
    func createActivity(activity: Activity, userPhoneNumber: String) -> Void {
        
        functions.httpsCallable("createActivityV2").call([
            "phoneNumber": userPhoneNumber, "image": activity.image, "description": activity.description,
            "done": activity.done ?? false, "campus": activity.campus, "name": activity.name,
            "location": activity.location, "memberLimit": activity.memberLimit, "open": activity.open ?? true,
                                                            "rating": activity.rating, "activityPreference": activity.preference]){ result,error in
                
                if let error = error as NSError? {
                    if error.domain == FunctionsErrorDomain {
                        _ = FunctionsErrorCode(rawValue: error.code)
                        _ = error.localizedDescription
                        _ = error.userInfo[FunctionsErrorDetailsKey]
                    }
                }
            }
    }
    
    func sendGroupInvite(userPhoneNumber: String, activityId: String) -> Void {
        
        functions.httpsCallable("sendGroupInvite").call(["userPhoneNumber": userPhoneNumber,
                                                         "activityId": activityId]){ result,error in
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
        }
    }
    
    func adminAcceptRequest(userPhoneNumber: String, activityId: String) -> Void {
        
        functions.httpsCallable("adminAcceptRequest").call(["userPhoneNumber": userPhoneNumber,
                                                            "activityId": activityId]){ result,error in
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
        }
    }
    
    func adminDeclineRequest(userPhoneNumber: String, activityId: String) -> Void {
        
        functions.httpsCallable("adminDeclineRequest").call(["userPhoneNumber": userPhoneNumber,
                                                             "activityId": activityId]){ result,error in
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
        }
    }
    
    func adminRemovesMember(userPhoneNumber: String, activityId: String) -> Void {
        
        functions.httpsCallable("adminRemovesMember").call(["userPhoneNumber": userPhoneNumber,
                                                            "activityId": activityId]){ result,error in
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
        }
    }
    
    func sendMessage(sendersPhoneNumber: String, activityId: String, message: String) -> Void {
        
        functions.httpsCallable("sendMessage").call(["senderPhoneNumber": sendersPhoneNumber,
                                                     "message": message,
                                                     "activityId": activityId]){ result,error in
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
        }
    }
    
    
    func getActivitiesByPreference(preference: String, completed: @escaping (Result<[String], DecidoError>) -> Void){
        
        functions.httpsCallable("getActivitiesByPreference").call(["preference": preference]){ result, error in
            
            var activityIds = [String]()
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
            
            guard let data = result?.data as? [String: Any] else {
                completed(.failure(.dataNotRetrievedFromServer))
                return
            }
            
            for activityId in data.keys {
                activityIds.append(activityId)
            }
        
            completed(.success(activityIds))
        }
    }
    
    func deleteActivity(activityId: String) -> Void {
        
        functions.httpsCallable("deleteActivity").call(["activityId": activityId]){ result,error in
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
        }
    }
    
    
}

