import Foundation
import Firebase
import SwiftUI


class VotingFunctions {
    
    lazy var functions = Functions.functions()
    let dateFormatter = DateFormatter()
    
    func createVotingCard(votingCard: VotingCard, activityId: String) -> Void {
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss"
        
        functions.httpsCallable("createVotingCard").call([
            "activityId": activityId,
            "id": votingCard.id,
            "title": votingCard.title,
            "description": votingCard.description,
            "cardType": votingCard.cardType.rawValue,
            "options": votingCard.options.map { $0.text },
            "expirationTime": dateFormatter.string(from: votingCard.duration) ,
        ]){ result,error in
            
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
        }
    }
    
    func registerUserVote(votingCardId: String, activityId: String, usersPhoneNumber: String, userVotes: [String]) -> Void {
        
        functions.httpsCallable("registerUserVote").call([
            "activityId": activityId,
            "votingCardId": votingCardId,
            "userPhoneNumber": usersPhoneNumber,
            "votes": userVotes,
        ]){ result,error in
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
        }
    }
    
    func getActivityVotingCards(activityId: String, completed: @escaping (Result<[VotingCard], DecidoError>) -> Void){
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss"
        var votingCardReturn = [VotingCard]()
        
        functions.httpsCallable("getActivityVotingCards").call([
            "activityId": activityId
        ]){ result,error in
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
            
            guard let data = result?.data as? [String: [String:Any]] else {
                completed(.failure(.dataNotRetrievedFromServer))
                return
            }            
            
            for votingCard in data{
                var cardAdded = VotingCard(id: votingCard.key,
                                                   cardType: votingCard.value["cardType"] as? String ?? "" == "multiSelect" ? .multiSelect : .multipleChoice,
                                                   title: votingCard.value["title"] as? String ?? "Title",
                                                   description: votingCard.value["description"] as? String ?? "Description",
                                                   duration: self.dateFormatter.date(from: votingCard.value["expirationTime"] as? String ?? "") ?? Date(),
                                                   options: createOptions(options: votingCard.value["options"] as? [String] ?? [String]()))
                
                let votedUsers = votingCard.value["usersVoted"] as? [String: String] ?? [String: String]()
                
                //users voted
                var votedUsersArray = [String]()
                for vote in votedUsers{
                    votedUsersArray.append(vote.key)
                }
                cardAdded.usersVoted = votedUsersArray
                
                //results
                let voteResult = votingCard.value["results"] as? [String: [String]] ?? [String: [String]]()
                var votingResult = [String]()
                for result in voteResult{
                    votingResult.append(contentsOf: result.value)
                }
                cardAdded.results = votingResult
                
                //return
                votingCardReturn.append(cardAdded)
            }
            
            completed(.success(votingCardReturn))
        }
    }
    
    
    func getActivityVotingCardResult(activityId: String, votingCardId: String, completed: @escaping (Result<[String], DecidoError>) -> Void){
        
        var votingResults = [String]()
        
        functions.httpsCallable("getActivityVotingCardResult").call([
            "activityId": activityId,
            "votingCardId": votingCardId
        ]){ result,error in
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
                //used for testing, comment once backend function has been deployed
                //completed(.success(["Opt A", "Opt A", "Opt B", "Opt C", "Opt B", "Opt B"]))
            }
            
            guard let data = result?.data as? [String: [String]] else {
                completed(.failure(.dataNotRetrievedFromServer))
                return
            }
            
            for votingCard in data{
                votingResults.append(contentsOf: votingCard.value)
            }
            
            completed(.success(votingResults))
        }
    }
}
