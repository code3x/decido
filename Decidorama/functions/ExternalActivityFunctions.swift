import Foundation
import Firebase
import SwiftUI

class ExternalActivityFunctions {
    
    lazy var functions = Functions.functions()
    
    func createExternalActivity(activity: Activity) -> Void {
        
        functions.httpsCallable("createExternalActivity").call(["image": activity.image, "description": activity.description,
                                                                "done": activity.done ?? false, "campus": activity.campus, "name": activity.name,
                                                                "location": activity.location, "memberLimit": activity.memberLimit, "open": activity.open ?? true,
                                                                "rating": activity.rating, "date": activity.date, "price": activity.price]){ result,error in
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
        }
    }
    
    func getAllExternalActivities(filterOut: [String] = [], completed: @escaping (Result<[Activity], DecidoError>) -> Void){
        
        functions.httpsCallable("getAllExternalActivities").call(){ result, error in
            
            var activities: [Activity] = []
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
            
            guard let data = result?.data as? [String: [String:Any]] else {
                completed(.failure(.dataNotRetrievedFromServer))
                return
            }
            
            for activity in data {
                if(!filterOut.contains(activity.key)){
                    activities.append(Activity(activityId: activity.key,
                                               owner: activity.value["owner"] as? String ?? "",
                                               image: activity.value["image"] as? String ?? "",
                                               campus: activity.value["campus"] as? String ?? "",
                                               name: activity.value["name"] as? String ?? "",
                                               description: activity.value["description"] as? String ?? "",
                                               done: activity.value["done"] as? Bool ?? nil,
                                               open: activity.value["open"] as? Bool ?? nil,
                                               rating: activity.value["ration"] as? String ?? "",
                                               location: activity.value["location"] as? String ?? "",
                                               memberLimit: activity.value["memberLimit"] as? Int ?? 0,
                                               date : activity.value["date"] as? String ?? "",
                                               price : activity.value["price"] as? String ?? "",
                                               preference: activity.value["tag"] as? String ?? "",
                                               deleted: activity.value["deleted"] as? Bool ?? false))
                }
            }
            completed(.success(activities))
        }
    }
    
    func registerUserExternalSwipe(userPhoneNumber: String, activityId: String, directionSwiped: String) -> Void {
        
        functions.httpsCallable("registerUserExternalSwipe").call(["userPhoneNumber": userPhoneNumber,
                                                                   "activityId": activityId,
                                                                   "directionSwiped": directionSwiped]){ result,error in
            
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
        }
    }
    
}
