//
//  PreferenceFirebase.swift
//  Decidorama
//
//  Created by Ali Muhammad on 2021-04-17.
//

import Foundation
import Firebase

struct PreferenceServer {
    
    /*
    func getUserPreferences(userID: String){
        guard let url = URL(string: "https://decidorama-api.azurewebsites.net/weatherforecast") else {return}
        
        let request = URLRequest(url: url)
        print("here -- ")
        URLSession.shared.dataTask(with: request) { data, response, error in
            if let response = try? JSONDecoder().decode([String].self, from: data!) {
                DispatchQueue.main.async {
                    //set state
                }
                return
            }
        }.resume()
    }
 */
    
    /* Not user now*/
    func savePreference(preference: UserPreference){
        let location = Database.database().reference().child("Preference").child(preference.preferenceID)
        
        let preferenceData = [
            "id": preference.preferenceID,
            "name": preference.preferenceName,
            "image": preference.preferenceImage,
            "description": preference.preferenceDescription,
        ]
        
        location.updateChildValues(preferenceData)
    }
    
    func savePreferenceToUser(userID: String, preferenceID: String){
        let location = Database.database().reference().child("User").child(userID).child("Preferences")
        
        let preferenceData = [
            "preferenceID": preferenceID,
        ]
        
        location.childByAutoId().updateChildValues(preferenceData)
    }
    
    func retriveAllPreferences(complete: @escaping ([UserPreference]) -> ()){
        
        var preferences = [UserPreference]()
        
        Database.database().reference().child("Preference").observeSingleEvent(of: .value) { (snapshot) in
            
            if let data = snapshot.value as? Dictionary<String, AnyObject>{
                
                for (_, preference) in data
                {
                    var values = [String]()
                    for(value) in preference.allObjects{
                        values.append("\(value)")
                    }
                    
                    preferences.append(UserPreference(id: values[0], name: values[3], image: values[2], description: values[1]))
                }
            }
            complete(preferences)
        }
        
    }
    
    
}
