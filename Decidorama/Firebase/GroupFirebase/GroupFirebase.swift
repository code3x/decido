//
//  GroupFirebase.swift
//  Decidorama
//
//  Created by Ali Muhammad on 2021-04-17.
//

import Foundation
import Firebase

struct serverRequest{
    
    func saveGroup(group: Group){
        //location of the user
        let location = Database.database().reference().child("Group").child(UUID().uuidString)
        
        let groupData = [
            "groupName": group.groupName,
            "groupDescription": group.groupDescription,
            "groupImage": group.groupPictureURL,
        ]
        
        location.updateChildValues(groupData)
    }
    
    func retriveGroup(groupID: String, complete: @escaping (Group) -> ()){
        let group = Group()
        Database.database().reference().child("Group").child(groupID).observeSingleEvent(of: .value) { (snapshot) in
            //print(snapshot)
            if let data = snapshot.value as? Dictionary<String, AnyObject>{
                
                guard let name = data["groupName"] as? String else {
                    print("error 1")
                    complete(group)
                    return
                }
                group.groupName = name
                
                guard let groupDescription = data["groupDescription"] as? String else {
                    print("error 2")
                    complete(group)
                    return
                }
                group.groupDescription = groupDescription
                
                guard let image = data["groupImage"] as? String else {
                    print("error 3")
                    complete(group)
                    return
                }
                group.groupPictureURL = image
                complete(group)
                
                
            }else{
                print("User not found")
                print("error 0")
                complete(group)
            }
        }
    }
    
}

//
//class groupFinder: ObservableObject  {
//    @Published var groupName: String = ""
//    @Published var itemLists : [GroupRows] = []
//
//    func retriveAllGroup(complete: @escaping ([GroupRows]) -> ()){
//
//        Database.database().reference().child("Group").observeSingleEvent(of: .value) { (snapshot) in
//            let children = snapshot.children
//            while let rest = children.nextObject() as? DataSnapshot, let value = rest.value {
//                if let value = rest.value as? [String: Any] {
//
//                     let groupName = value["groupName"] as? String ?? ""
//                     let groupDescription = value["groupDescription"] as? String ?? ""
//                     let groupImage = value["groupImage"] as? String ?? ""
//
//                    self.itemLists.append(GroupRows(group_image: groupImage, group_name: groupName, group_description: groupDescription))
//             }
//
//             }
//            complete(self.itemLists)
//            }
//        }
//
//}
