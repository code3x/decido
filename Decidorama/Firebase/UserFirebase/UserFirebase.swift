//
//  UserFirebase.swift
//  Decidorama
//
//  Created by Ali Muhammad on 2021-04-17.
//

import Foundation
import Firebase


struct serverRequests{
    
    func saveUser(user: User, complete: @escaping (Bool) -> ()){
        //location of the user
        let location = Database.database().reference().child("User").child(user.phoneNumber).child("Profile")
        
        let userData = [
            "name": user.name,
            "userStatus": user.userStatus,
            "avatar": user.avatar,
            "bio": user.bio
        ]
        
        location.updateChildValues(userData) { (error, ref) in
            if error == nil{
                complete(true)
            }else{
                complete(false)
            }
        }
    }
    
    func retriveUser(phoneNumber: String, complete: @escaping (User) -> ()){
        var user = User(phoneNumber: phoneNumber, avatar: "", bio: "", age: 0, gender: "", program: "", name: "", userStatus: "")
        Database.database().reference().child("User").child(phoneNumber).child("Profile").observeSingleEvent(of: .value) { (snapshot) in
            //print(snapshot)
            if let data = snapshot.value as? Dictionary<String, AnyObject>{
                user.phoneNumber = phoneNumber
                
                guard let name = data["name"] as? String else {
                    print("retriveUser: error name")
                    complete(user)
                    return
                }
                user.name = name
                
                guard let userStatus = data["userStatus"] as? String else {
                    print("retriveUser: error status")
                    complete(user)
                    return
                }
                user.userStatus = userStatus
                
                guard let avatar = data["avatar"] as? String else {
                    print("retriveUser: error avatar")
                    complete(user)
                    return
                }
                user.avatar = avatar
                
                guard let bio = data["bio"] as? String else {
                    print("retriveUser: error Bio")
                    complete(user)
                    return
                }
                user.bio = bio
                complete(user)
                
                
            }else{
                print("User not found")
                print("error 0")
                complete(user)
            }
        }
    }
}
